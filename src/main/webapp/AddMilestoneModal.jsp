<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="/struts-tags" prefix="s" %>
<s:form method="post" enctype="multipart/form-data">
<div id="mainModal">
	<div class="modal" tabindex="-1" role="dialog" id="milestoneModal">
       <div class="modal-dialog" role="document">
         <div class="modal-content">
           <div class="modal-header">
           		<h5 class="modal-title">New Milestone</h5>
           </div>
           <div class="modal-body">
               <div class="form-group">
                   <small class="form-text text-muted">Milestone Name</small>
                   <input type="text" class="form-control unitNameField" id="idMlstnName">
               	   <p class="invalidUnitNameErr" id="id-invalidUnitNameErr"></p>               		
               </div>
               <div class="form-group">
                   <small class="form-text text-muted">Milestone Description</small>
                   <textarea class="form-control" id="idMlstnDescript" rows="3"></textarea>
               </div>
               <small class="form-text text-muted">Add Task to Milestone</small>
               <div class="form-group mlstnTaskList">
	               <s:if test="taskList!=null">
		               <s:iterator value="taskList">
			               	<div class="form-check">
		                       <input type="checkbox" class="form-check-input" id="<s:property value="getTaskID()"/>" value="<s:property value="getTaskName()"/>" name="checkTask">
		                       <label class="form-check-label mlstnTaskListLbl" for="<s:property value="getTaskID()"/>"><s:property value="getTaskName()"/></label>
	                     	</div>
		               </s:iterator>
	               </s:if>
               </div>
               <div class="row cust-modal-row"> 
                   <div class="col-md-6">
                       <small class="form-text text-muted">From Date</small>
                       <div class="input-group date">
                           <input type="text" name="nmMlstnzfrmDate" id="idMlstnFrmDate" class="form-control frmDateField unitFrmDateField" autocomplete="off" placeholder="From Date"><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                       </div>
                       <p class="invalidUnitFrmDtErr" id="id-invalidUnitFrmDtErr">* From Date Required</p>
                   </div>
                   <div class="col-md-6">
                       <small id="emailHelp" class="form-text text-muted">To Date</small>
                       <div class="input-group date">
                           <input type="text" name="nmMlstnToDate" id="idMlstnToDate" class="form-control toDateField unitToDateField" autocomplete="off" placeholder="To Date" value="<s:property value="detailingMlstnEntityObj.getDPFormattedDueTimestamp()"/>"><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                       </div>
                       <p class="invalidUnitToDtErr" id="id-invalidUnitToDtErr">* To Date Required</p>
                   </div>
               </div>
               
               <div class="row"> 
                   <div class="col-md-6">
                       <div class="dropdown form-group">
                           <small class="form-text text-muted">Priority</small>
                           <button class="btn btn-primary dropdown-toggle form-control unitPriority" type="button" id="idMlstnPriority" name="nmMlstnPriority" data-toggle="dropdown">
	                           Priority
	                           <span class="caret" ></span>
                           </button>
                           <ul class="mlstnPrtyUL unitPriorityUL dropdown-menu">
                             <li><a >Low</a></li>
                             <li><a >Medium</a></li>
                             <li><a >High</a></li>
                           </ul>
                           <p class="invalidUnitPrtyErr" id="id-invalidUnitPrtyErr">* Priority Required</p>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div class="dropdown form-group">
                           <small class="form-text text-muted">Status</small>
                           <button class="btn btn-primary dropdown-toggle form-control unitStatus" type="button" id="idMlstnStatus" name="nmMlstnStatus" data-toggle="dropdown">
	                           Status
	                           <span class="caret"></span>
                           </button>
                           <ul class="mlstnStatUL unitStatusUL dropdown-menu">
                             <li><a >Open</a></li>
                             <li><a >Development</a></li>
                             <li><a >Testing</a></li>
                             <li><a >Closed</a></li>
                           </ul>
                           <p class="invalidUnitStatErr" id="id-invalidUnitStatErr">* Status Required</p>
                       </div>
                   </div>
               </div>
           
               <small class="form-text text-muted">Attach Files</small>
               <div class="mb-3">
                   <input class="form-control fileAttachments" type="file" id="idMlstnFileAttachments" name="nmMlstnFileAttachments" multiple>
               </div>

               <div id="appendedFileListGrp" style="display: none;">
                    <div class="form-group mlstnTaskList">
                        <div class="form-check" id="appendedFileListViewer">
                            
                        </div>
                    </div>
                    <div class="form-check file-lst-clr-sec">
                        <a onclick="clearAttchmntField('idMlstnFileAttachments')">Clear All</a>
                    </div>
                </div>

           </div>
           <div class="modal-footer">
           	   <button type="button" class="btn btn-primary" onclick="addNewMilestone()">Create Milestone</button>
	           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	       </div>
         </div>
       </div>
     </div>
 </div>
</s:form>