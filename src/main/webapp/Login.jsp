<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
	 <%@include file="AppCDNReqs.html"%>
	 <script src="JSFiles/Login.js"></script>
	 <title>Login</title>
</head>
<body>
<!-- Displaying Error messages -->

<s:if test="hasActionErrors()">
	<s:iterator value="actionErrors">
		<div class="container error-container">
		     <div class="row">
		         <div class="col-md-4"></div>
		         <div class="col-md-4 error-alert-col">
		             <div class="alert alert-danger alert-dismissable">
		                 <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		                 <s:property/> 
		             </div>
		         </div>
		         <div class="col-md-4"></div>
		     </div>
	 	</div>
	</s:iterator>
</s:if>
	<!-- <form> -->
		<div class="container main-container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-login">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <a class="active" id="login-form-link">Login</a>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                	<div class="form-group">
                                        <input type="text" class="form-control" id="idLogUserName" name="loginUsername" placeholder="Username or Email" required>
                                    </div>
                                    <p class="invalid-creds-para-resp" id="idLoginUsernameErr"></p>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="idLogPassword" name="loginPassword" placeholder="Password" required>
                                    </div>
                                    <p class="invalid-creds-para-resp" id="idLoginPasswordErr"></p>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <input type="button" name="login-submit" id="login-submit" onclick="return validateLogin()" tabindex="4" class="form-control btn btn-login" value="Log In">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <!-- <div class="text-center">
                                                    <a href="https://phpoll.com/recover" tabindex="5" class="forgot-password">Forgot Password?</a>
                                                </div> -->
                                                <div class="text-center">
                                                    <p class="forgot-password">Not having an account Signup <span><a href="Signup" tabindex="5">here</a></span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- </form> -->
    </body>
</body>
</html>