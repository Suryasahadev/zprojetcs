<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Z Projects</title>
<%@include file="PreCDNIncludes.html"%>
</head>
<body>
	<!-- Error displaying for Auth page breaches -->
	<s:if test="hasActionErrors()">
		<s:iterator value="actionErrors">
			<div class="container error-container breach-error-messages">
			     <div class="row">
			         <div class="col-md-4"></div>
			         <div class="col-md-4 error-alert-col">
			             <div class="alert alert-danger alert-dismissable">
			                 <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			                 <s:property/> 
			             </div>
			         </div>
			         <div class="col-md-4"></div>
			     </div>
		 	</div>
		</s:iterator>
	</s:if>
	 
	<s:form method="post" action="addNewProject" id="homeForm" enctype="multipart/form-data">
	<!-- Replacement div for edit PP Modals--> 
	<div id="idPPModals">
		<!-- Providing Project Modal as default or if requested  -->
		<s:if test="%{requestedContent == 'CreatedProjects' || requestedContent == 'AssignedProjects' || requestedContent == 'CreatedTasks' || requestedContent == 'AssignedTasks' || requestedContent == 'CreatedMilestones' || requestedContent == 'Dashboard'}">
			<%@include file="AddProjectModal.jsp"%> 
		</s:if>
		<!-- Providing Task Modal if requested-->
		<s:if test="%{requestedContent == 'ProjectTasks'}">
			<%@include file="AddTaskModal.jsp"%> 
		</s:if>
		<!-- Providing Milestone Modal if requested-->
		<s:if test="%{requestedContent == 'ProjectMilestones'}">
			<%@include file="AddMilestoneModal.jsp"%> 
		</s:if>
	</div>  
		<div class="container-fluid home-page-main-container m-0">
            <div class="row home-page-main-row">
                <div class="col-md-12 home-page-main-col">
                	<%@include file="Sidebar.jsp"%>
                    <div class="col-md-10 home-page-body">
                        <div class="row">
                        <%@include file="Topbar.jsp"%>
                        </div>
                        <div class="row" id="home-page-body-content-nav">
                        </div>
                        <div class="row" id="home-page-body-content-elements">
                        
                        	<div class="col-md-12" id="idMain-unit-contents-div">
                        		<!-- Main Page content to be rendered here through all ajax calls -->
                        		
                        		<!-- Enabling Nav rows based on requested content -->
                        		<!-- Nav for Project Sections -->
                        		<div id="idMain-unit-navs">
	                        		<s:if test="%{requestedContent == 'ProjectTasks' || requestedContent == 'ProjectMilestones' || requestedContent == 'ProjectAttachments' || requestedContent == 'ProjectCharts' || requestedContent == 'ProjectDetails'}">
	                        			<nav class="navbar navbar-default project-nav-div">
										    <div class="container-fluid project-nav-cont">
										      <ul class="nav navbar-nav">
										        <li id="navTasks" class="navList" onclick="renderProjectContents('Tasks', <s:property value="selectedProjectID"/>)"><a>Tasks</a></li>
												<li id="navMilestones" class="navList" onclick="renderProjectContents('Milestones', <s:property value="selectedProjectID"/>)"><a >Milestones</a></li>
												<li id="navAttachments" class="navList" onclick="renderProjectContents('Attachments', <s:property value="selectedProjectID"/>)"><a >Documents</a></li>
												<li id="navCharts" class="navList" onclick="renderProjectContents('Charts', <s:property value="selectedProjectID"/>)"><a >Chart</a></li>
												<li id="navDetails" class="navList" onclick="renderProjectContents('Details', <s:property value="selectedProjectID"/>)"><a >Details</a></li>
									          </ul>
									        </div>
									     </nav>
	                        		</s:if>
	                        		
	                        		<!-- Nav for Task Sections -->
	                        		<s:if test="%{requestedContent == 'TaskAttachments' || requestedContent == 'TaskDetails'}">
	                        			<nav class="navbar navbar-default project-nav-div">
										    <div class="container-fluid project-nav-cont">
										      <ul class="nav navbar-nav">
										        <li id="navAttachments" class="navList" onclick="renderTaskContents('Attachments', <s:property value="selectedTaskID"/>)"><a >Documents</a></li>
												<li id="navDetails" class="navList" onclick="renderTaskContents('Details', <s:property value="selectedTaskID"/>)"><a >Details</a></li>
									          </ul>
									        </div>
									     </nav>
	                        		</s:if>
	                        		
	                        		<!-- Nav for Milestone Sections -->
	                        		<s:if test="%{requestedContent == 'MilestoneAttachments' || requestedContent == 'MilestoneCharts' || requestedContent == 'MilestoneDetails'}">
	                        			<nav class="navbar navbar-default project-nav-div">
										    <div class="container-fluid project-nav-cont">
										      <ul class="nav navbar-nav">
										        <li id="navAttachments" class="navList" onclick="renderMilestoneContents('Attachments', <s:property value="selectedMilestoneID"/>)"><a >Documents</a></li>
												<li id="navCharts" class="navList" onclick="renderMilestoneContents('Charts', <s:property value="selectedMilestoneID"/>)"><a >Chart</a></li>
												<li id="navDetails" class="navList" onclick="renderMilestoneContents('Details', <s:property value="selectedMilestoneID"/>)"><a >Details</a></li>
									          </ul>
									        </div>
									     </nav>
	                        		</s:if>
                        		</div>
                        		<div id="idMain-unit-data-section">
	                        		<!-- Div for User Projects listing -->
	                        		<s:if test="%{requestedContent == 'CreatedProjects' || requestedContent == 'AssignedProjects'}">
	                        			<s:if test="projectList!=null">
				                        	<s:if test="%{!projectList.isEmpty()}">
				                        		<table class="table align-middle mb-0 bg-white project-list-table dataTable" id="idproject-list-table">
					                                <thead class="bg-light">
					                                    <tr>
					                                      <th>Project Name</th>
					                                      <th>Owner</th>
					                                      <th>Start Date</th>
					                                      <th>Due Date</th>
					                                      <th>Priority</th>
					                                      <th>Status</th>
					                                    </tr>
					                                </thead>
					                                <tbody>
						                                <s:iterator value="projectList">
					                                		<tr onclick="renderProjectContents('Tasks', '<s:property value="getProjectID()"/>')">
					                                			
					                                			<td><s:property value="getProjectName()"/></td>
					                                		
					                                			<td><s:property value="getOwner()"/></td>
					                                		
					                                			<td><s:property value="getStartTimestamp()"/></td>
					                                		
					                                			<td><s:property value="getDueTimestamp()"/></td>
					                                		
					                                			<td>
					                                				<span class="badge badge-<s:property value='getPriority()'/> rounded-pill d-inline" ><s:property value="getPriority()"/></span>
					                                			</td>
					                                		
					                                			<td>
					                                				<span class="badge badge-<s:property value='getStatus()'/> rounded-pill d-inline" ><s:property value="getStatus()"/></span>
					                                			</td>
					                                		</tr>
					                                	</s:iterator>
					                               </tbody>
			                            		</table>
			                               	</s:if>
			                               	<s:else>
										 		<div class="row">
										 			<div class="col-md-4"></div>
										 			<div class="col-md-4 empty-notify-col">
										 				<h1>No Projects to Display</h1>
										 				<i class="fas fa-10x fa-project-diagram"></i>
										 			</div>
										 			<div class="col-md-4"></div>
										 		</div>
									 		</s:else>
									 	</s:if>
	                   				</s:if>
	                   				
	                        		<s:if test="%{requestedContent == 'CreatedTasks' || requestedContent == 'AssignedTasks' || requestedContent == 'ProjectTasks'}">
	                        			<!-- Div for User Tasks listing -->
									 		<s:if test="taskList!=null">
										 		<s:if test="%{!taskList.isEmpty()}">
													<table class="table align-middle mb-0 bg-white" id="idTask-list-table">
										                <thead class="bg-light">
										                  	  <tr>
													            <th>Task Name</th>
													            <th>Owner</th>
													            <th>Start Date</th>
													            <th>Due Date</th>
													            <th>Assignee</th>
													            <th>Priority</th>
													            <th>Status</th>
													            <th>Details</th>
													          </tr>
										                </thead>
										                <tbody>
															<s:iterator value="taskList">
																<tr>
																	<td><s:property value="getTaskName()"/></td>
																	
																	<td><s:property value="getOwner()"/></td>
																	
																	<td><s:property value="getStartTimestamp()"/></td>
																	
																	<td><s:property value="getDueTimeStamp()"/></td>
																	
																	<td><s:property value="getAssignee()"/></td>
																	
																	<td>
																		<span class="badge badge-<s:property value='getTaskPriority()'/> rounded-pill d-inline" ><s:property value="getTaskPriority()"/></span>
																	</td>
																	
																	<td>
																		<span class="badge badge-<s:property value='getTaskStatus()'/> rounded-pill d-inline" ><s:property value="getTaskStatus()"/></span>
																	</td>
																	
																	<td class="content-sub-details" onclick="renderTaskContents('Attachments', <s:property value="getTaskID()"/>)">
																		<i class="fas fs-lg fa-arrow-right"></i>
																	</td>
																</tr>
															</s:iterator>
										   				</tbody>
												 	</table>
										 		</s:if>
										 		<s:else>
											 		<div class="row">
											 			<div class="col-md-4"></div>
											 			<div class="col-md-4 empty-notify-col">
											 				<h2>No Tasks to Display</h2>
											 				<i class="fas fa-10x fa-list-check"></i>
											 			</div>
											 			<div class="col-md-4"></div>
											 		</div>
											 	</s:else>
										 	</s:if>
	                        		</s:if>
	                        		
	                        		<s:if test="%{requestedContent == 'CreatedMilestones' || requestedContent == 'ProjectMilestones'}">
	                        			<!-- Div for User Milestones listing -->
									 	<div id="idMlstn-unit-contents-div">
										 	<s:if test="milestoneList!=null">
										 		<s:if test="%{!milestoneList.isEmpty()}">
													<table class="table align-middle mb-0 bg-white mlstn-list-table" id="idMlstn-list-table">
											             <thead class="bg-light">
											               	  <tr>
											                    <th>Milestone Name</th>
											                    <th>Created By</th>
											                    <th>Start Date</th>
											                    <th>Due Date</th>
											                    <th>Priority</th>
											                    <th>Status</th>
											                    <th>Tasks</th>
											                    <th>Details</th>
											                  </tr>
											             </thead>
											             <tbody>
															<s:iterator value="milestoneList">
																<tr class="mlstn-tr"  id="idMlstn<s:property value="getMilestoneID()"/>">
																	<td><s:property value="getMilestoneName()"/></td>
																
																	<td><s:property value="getOwner()"/></td>
																	
																	<td><s:property value="getStartTimestamp()"/></td>
																	
																	<td><s:property value="getDueTimeStamp()"/></td>
																	
																	<td>
																		<span class="badge badge-<s:property value='getPriority()'/> rounded-pill d-inline" ><s:property value="getPriority()"/></span>
																	</td>
																	
																	<td>
																		<span class="badge badge-<s:property value='getStatus()'/> rounded-pill d-inline" ><s:property value="getStatus()"/></span>
																	</td>
																	<td class="content-sub-details" onclick="displayMilestoneSubTasks(<s:property value="getMilestoneID()"/>)" data-toggle="collapse" data-target="#mlstnSubTbl<s:property value="getMilestoneID()"/>">
																		<i class="fa-solid fa-list-check"></i>
																	</td>
																	<td class="content-sub-details" onclick="renderMilestoneContents('Attachments', <s:property value="getMilestoneID()"/>)">
																		<i class="fas fs-lg fa-arrow-right"></i>
																	</td>
																</tr>
															</s:iterator>
														</tbody>
												 	</table>
										 		</s:if>
										 		<s:else>
											 		<div class="row">
											 			<div class="col-md-4"></div>
											 			<div class="col-md-4 empty-notify-col">
											 				<h2>No Milestones to Display</h2>
											 				<i class="fas fa-10x fa-flag"></i>
											 			</div>
											 			<div class="col-md-4"></div>
											 		</div>
											 	</s:else>
										 	</s:if>
									 	</div>
	                        		</s:if>
	                        		<!-- Div for attachments listing -->
	                        		<s:if test="%{requestedContent == 'ProjectAttachments' || requestedContent == 'MilestoneAttachments' || requestedContent == 'TaskAttachments'}">
		                        		<s:if test="attachmentList!=null">
									       	<s:if test="%{!attachmentList.isEmpty()}">
												<table class="table table-borderless">
										             <thead>
										               <tr>
										                 <th scope="col">Attachments</th>
										               </tr>
										             </thead>
										             <tbody>
										             	<s:iterator value="attachmentList">
															<tr>
																<td>
																	<a href="/ZProjects/Attachments/DownloadAttachment/<s:property value="getDocumentID()"/>"><s:property value="getFileName()"/></a>
																</td>
															</tr>
														</s:iterator>
										             </tbody>
										         </table>
									         </s:if>
									         <s:else>
										 		<div class="row">
										 			<div class="col-md-4"></div>
										 			<div class="col-md-4 empty-notify-col">
										 				<h2>No Attachments to Display</h2>
										 				<i class="fas fa-10x fa-folder-open"></i>
										 			</div>
										 			<div class="col-md-4"></div>
										 		</div>
										 	</s:else>
								       	</s:if>
							       	</s:if>
							       	<!-- Div for Charts Displaying -->
							       	<s:if test="%{requestedContent == 'ProjectCharts' || requestedContent == 'MilestoneCharts'}">
	                        			<div class="col-md-12">
	                        				<div class="row">
	                        					<div class="col-md-12"><p style="color:#777; margin:10px 0px"><i class="fas fa-grip-vertical"></i> Gant for Associated Tasks</p></div>
	                        				</div>
	                        				<div class="row">
	                        					<div class="col-md-1"></div>
	                        					<div class="col-md-10">
	                        						<div class="taskChartsection" id="taskChartsection">
                        							</div>
	                        					</div>
	                        					<div class="col-md-1"></div>
	                        				</div>
	                        				<s:if test="%{requestedContent == 'ProjectCharts'}">
	                        					<div class="row">
		                        					<div class="col-md-12"><p  style="color:#777; margin:10px 0px"><i class="fas fa-grip-vertical"></i> Gant for Associated Milestones</p></div>
		                        				</div>
		                        				<div class="row">
		                        					<div class="col-md-1"></div>
		                        					<div class="col-md-10">
		                        						<div class="mlstnChartsection" id="mlstnChartsection">
	                        							</div>
		                        					</div>
		                        					<div class="col-md-1"></div>
		                        				</div>
	                        				</s:if>
	                        			</div>
                        			</s:if>
                        			<!-- Div for Charts Displaying -->
                        		 </div>
                        		 
                        		 <!-- Div for dashboard displaying -->
	                       			<s:if test="%{requestedContent == 'Dashboard'}">
		                        		<div class="row">
                        					<div class="col-md-12"><p style="color:#777; margin:10px 0px"><i class="fas fa-grip-vertical"></i> Created and Assigned Projects based on status</p></div>
                        				</div>
		                        		<div class="row">
			                        		<div class="col-md-6">
			                        			<div class="ownPrjctChartSection" id="ownPrjctChartSection">
		                        				</div>
			                        		</div>
			                        		<div class="col-md-6">
			                        			<div class="asgndPrjctChartSection" id="asgndPrjctChartSection">
		                        				</div>
			                        		</div>
		                        		</div>
		                        		<div class="row">
                        					<div class="col-md-12"><p style="color:#777; margin:10px 0px"><i class="fas fa-grip-vertical"></i> Created and Assigned Tasks based on status</p></div>
                        				</div>
		                        		<div class="row">
			                        		<div class="col-md-6">
			                        			<div class="ownTaskChartSection" id="ownTaskChartSection">
		                        				</div>
			                        		</div>
			                        		<div class="col-md-6">
			                        			<div class="asgndTaskChartSection" id="asgndTaskChartSection">
		                        				</div>
			                        		</div>
		                        		</div>
		                        		<div class="row">
                        					<div class="col-md-12"><p style="color:#777; margin:10px 0px"><i class="fas fa-grip-vertical"></i> User based Task and Project count</p></div>
                        				</div>
		                        		<div class="row">
			                        		<div class="col-md-6">
			                        			<div class="usersProjectChartSection" id="usersProjectChartSection">
		                        				</div>
			                        		</div>
			                        		<div class="col-md-6">
			                        			<div class="usersTaskChartSection" id="usersTaskChartSection">
		                        				</div>
			                        		</div>
		                        		</div>
		                        	 </s:if>
	                        	 <!-- Div for dashboard displaying -->
						 	</div>
                    	</div>
                   	</div>
                 </div>
             </div>
          </div>
	</s:form>
</body>
</html>