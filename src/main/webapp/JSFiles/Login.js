const EMPTY_USERNAME = '&#9888; Username Cannot be Empty';
const EMPTY_PASSWORD = '&#9888; Password Cannot be Empty';
const INVALID_USERNAME = '&#9888; Incorrect Username';
const INVALID_PASSWORD = '&#9888; Incorrect Password';

function validateLogin()
{
	if($('#idLogUserName').val()=='')
	{
		$('#idLoginUsernameErr').html(EMPTY_USERNAME);
		$('#idLoginUsernameErr').css("display", "block");
		$('#idLogUserName').addClass("invalid-creds-field-resp");
	}
	else if($('#idLogPassword').val()=='')
	{
		$('#idLoginPasswordErr').html(EMPTY_PASSWORD);
		$('#idLoginPasswordErr').css("display", "block");
		$('#idLogPassword').addClass("invalid-creds-field-resp");
	}
	else
	{
		$.ajax
 		({
 			url  : 'ValidateLoginUser',
 			type : 'POST',
 			data :
 				{
 					username : $('#idLogUserName').val(),
 					password : $('#idLogPassword').val()
 				},
 			success : function(response)
 			{
 				var successCode = response.successCode;
 				if(successCode!=='' && successCode=='L200')
				{
 					validationSuccess();
				}
 				else
				{
					var errorCode = response.errorCode;
					switch(errorCode)
					{
						case 'L205':
							$('#idLoginUsernameErr').html(INVALID_USERNAME);
			 				$('#idLoginUsernameErr').css("display", "block");
			 				$('#idLogUserName').addClass("invalid-creds-field-resp");
							break;
						case 'L206':
							$('#idLoginPasswordErr').html(INVALID_PASSWORD);
				 			$('#idLoginPasswordErr').css("display", "block");
				 			$('#idLogPassword').addClass("invalid-creds-field-resp");
							break;
					}
				}
 			},
 			error : function(response)
 			{
 				alert('Login Error');
 			}
 		})
	}
}
function validationSuccess()
{
	location.href = "Projects/CreatedProjects"
}