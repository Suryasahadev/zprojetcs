function hideMainContents()
{
    for (const child of document.getElementById('home-page-body-content').children) 
    {
        child.style.display = 'none';
    }
}
function displayProjects()
{
    document.getElementById('navbar-row').style.display = 'none';
    hideMainContents();
    document.getElementById('project-list-div').style.display = 'block';
}
function expandProject(projectName)
{
    hideMainContents();
    document.getElementById('navbar-row').style.display = 'block';
    document.getElementById('navTasks').classList.add('active');
    document.getElementById('task-'+projectName).style.display = 'block';
}
function setNavActive(elementID, projectID)
{
    var elements = document.getElementsByClassName('navList');
    for(var i = 0, length = elements.length; i < length; i++) 
    {
        elements[i].classList.remove('active');
    }
    document.getElementById(elementID).classList.add("active");
    
    hideMainContents();
    
    switch(elementID)
    {
        case 'navTasks':
        document.getElementById('task-'+projectID).style.display = 'table';
        break;

        case 'navMilestones':
        document.getElementById('mlstn-'+projectID).style.display = 'table';
        break;

        case 'navDocs':
        document.getElementById('docs-'+projectID).style.display = 'table';
        break;

        case 'navDetails':
        toggleProjectDetails(true)
        document.getElementById('details-'+projectID).style.display = 'block';
        break;

        case 'navCharts':
        toggleProjectDetails(true)
        document.getElementById('charts-'+projectID).style.display = 'block';
        createChart();
        break;
    }
}
function toggleProjectDetails(toggleValue) 
{
    if(toggleValue)
        $('#details-tr-project1 :input').attr('disabled', true);
    else
        $('#details-tr-project1 :input').attr('disabled', false);
}
function editProjectDetails()
{
    toggleProjectDetails(true)
}