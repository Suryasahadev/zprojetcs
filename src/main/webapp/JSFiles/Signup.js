const EMPTY_USERNAME = '&#9888; Username Cannot be Empty';
const EMPTY_USERMAIL = '&#9888; MailID Cannot be Empty';
const EMPTY_PASSWORD = '&#9888; Password Cannot be Empty';
const INVALID_USERNAME = '&#9888; Incorrect Username';
const INVALID_PASSWORD = '&#9888; Incorrect Password';
const EXISTING_USERNAME = '&#9888; Username Already Exist';
const EXISTING_MAIL_ID = '&#9888; UserMailID Already Exist';
const PASSWORD_CONSTRAINT_MISMATCH = '&#9888; Password must contain 8 - 15 characters with a mix of letters, numbers & symbols';
const PASSWORD_REPASSWORD_MISMATCH = '&#9888; Password must be same as given password';
const INCORRECT_OTP = '&#9888; Incorrect OTP';
const OTP_SENT_RESPONSE = 'OTP Sent to mail';

var mykey = "0123456789".split("");
var finalOTP = "";
var otp_inputs;

$(function()
{
    otp_inputs = document.querySelectorAll(".otp__digit");
    otp_inputs.forEach((_)=>{
    _.addEventListener("keyup", handle_next_input)
    });
});


function handle_next_input(event)
{
    let current = event.target;
    let index = parseInt(current.classList[1].split("__")[2]);
    current.value = event.key;
    
    if(event.keyCode == 8 && index > 1)
    {
        current.previousElementSibling.focus();
        finalOTP = "";
    }
    if(index < 4 && mykey.indexOf(""+event.key+"") != -1)
    {
        var next = current.nextElementSibling;
        next.focus();
    }
}

function validateSignIn()
{
	if($('#idSignUserName').val()=='')
	{
		$('#idUsernameErr').html(EMPTY_USERNAME);
		$('#idUsernameErr').css("display", "block");
		$('#idSignUserName').addClass("invalid-creds-field-resp");
	}
	else if($('#idSignUserMail').val()=='')
	{
		$('#idUsermailErr').html(EMPTY_USERMAIL);
		$('#idUsermailErr').css("display", "block");
		$('#idSignUserMail').addClass("invalid-creds-field-resp");
	}
	else if($('#idSignPassword').val()=='')
	{
		$('#passConstraintPara').html(EMPTY_PASSWORD);
		$('#passConstraintPara').css("display", "block");
		$('#idSignPassword').addClass("invalid-creds-field-resp");
	}
	else if($('#idSignRePassword').val()=='')
	{
		$('#rePassConstraintPara').html(EMPTY_PASSWORD);
		$('#rePassConstraintPara').css("display", "block");
		$('#idSignRePassword').addClass("invalid-creds-field-resp");
	}
	else
	{
		var passwordConstraint = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
		var actPassword = document.getElementById('idSignPassword');
		var reEntPassword = document.getElementById('idSignRePassword');
		if(!actPassword.value.match(passwordConstraint))
		{
			$('#passConstraintPara').html(PASSWORD_CONSTRAINT_MISMATCH);
			$('#passConstraintPara').css("display", "block");
			$('#idSignPassword').addClass("invalid-creds-field-resp");
		}
		else
		{
			debugger;
			if(actPassword.value!=reEntPassword.value)
			{
				$('#rePassConstraintPara').html(PASSWORD_REPASSWORD_MISMATCH);
				$('#rePassConstraintPara').css("display", "block");
				$('#idSignRePassword').addClass("invalid-creds-field-resp");
			}
			else
			{
				validateResponse();
			}
		}
	}
} 
function validateResponse()
{
	$.ajax
	({
		url :'ValidateSignupUser',
		type:'POST',
		data:
			{
				signUserName : $('#idSignUserName').val(),
				signUserMailID : $('#idSignUserMail').val(),
				signPassword: $('#idSignPassword').val()
			}, 
		success: function(response)
		{
			var successCode = response.successCode;
			if(successCode!=='' && successCode=='S201')
			{
				sendOTPMail();
				//Setting the mail id given 
				$('.verf-title-info').html('An OTP has been sent to '+$('#idSignUserMail').val());
				$('#otpValidationModal').modal();
			}
			else
			{
				var errorCode = response.errorCode;
				switch(errorCode)
				{
					case 'S205':
							$('#idUsernameErr').html(EXISTING_USERNAME);
							$('#idUsernameErr').css("display", "block");
							$('#idSignUserName').addClass("invalid-creds-field-resp");
						break;
					case 'S206':
							$('#idUsermailErr').html(EXISTING_MAIL_ID);
							$('#idUsermailErr').css("display", "block");
							$('#idSignUserMail').addClass("invalid-creds-field-resp");
						break;
				}
			}
		},
		error: function(response)
		{
			//calling error jsp page
		}
	});
}

function sendOTPMail()
{
	$.ajax
	({
		url: 'ValidateUserByOTP',
		type: 'POST',
		data: 
		{
			signUserName : $('#idSignUserName').val(),
			signUserMailID : $('#idSignUserMail').val(),
		},
		success: function(successResponse)
		{
			//Nothing to do here
		},
		error: function(errorResponse)
		{
			alert(errorResponse);
		}
	});
}

function resendOTP()
{
	sendOTPMail();
	
	//Showing the resend Response
	$('.otp_resp').html(OTP_SENT_RESPONSE);
	$('.otp_result').css({"display": "block", "color": "#118a44"});
	$('.otp__digit').css("border", "1px solid #337ab7");
}

function validateOTP()
{
	//appending all values
	for(let {value} of otp_inputs)
    {
        finalOTP += value;
    }
    
    $.ajax
	({
		url: 'VerifySignupOTP',
		type: 'POST',
		data: 
		{
			signUserName : $('#idSignUserName').val(),
			signUserMailID : $('#idSignUserMail').val(),
			signPassword: $('#idSignPassword').val(),
			insertedOTP: parseInt(finalOTP)
		},
		success: function(successResponse)
		{
			var successCode = successResponse.successCode;
			if(successCode!=='' && successCode=='S300')
			{
				$('#otpValidationModal').modal('toggle');
				
				//Sign in Success Action here
				setTimeout(function()
			    {
					showSuccessAndProceed('Signed in Sucesfully');
				}, 1000);
				
			}
			else
			{
				var errorCode = successResponse.errorCode;
				switch(errorCode)
				{
					case 'S350':
						$('.otp_resp').html(INCORRECT_OTP);
						$('.otp_result').css({"display": "block", "color": "#dc4c64"});
						$('.otp__digit').css("border", "1px solid #dc4c64");
					break;
				}
			}
		},
		error: function(errorResponse)
		{
			alert(errorResponse);
		}
	});
}

//For Creating Success message divs
function showSuccessAndProceed(message)
{
	var div = document.createElement("div");
	var element = "<div class=\"container successcontainer breach-error-messages\" style=\"top:0px;\">\n" +
                "\t\t\t     <div class=\"row\">\n" +
                "\t\t\t         <div class=\"col-md-4\"></div>\n" +
                "\t\t\t         <div class=\"col-md-4 error-alert-col\">\n" +
                "\t\t\t             <div class=\"alert alert-success alert-dismissable\">\n" +
                "\t\t\t                 <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">×</a>\n" +
                "\t\t\t                 "+message+" \n" +
                "\t\t\t             </div>\n" +
                "\t\t\t         </div>\n" +
                "\t\t\t         <div class=\"col-md-4\"></div>\n" +
                "\t\t\t     </div>\n" +
                "\t\t \t</div>"
	div.innerHTML = element;
	document.body.appendChild(div);
	$('.successcontainer').css('display', 'block');
     
    setTimeout(function()
    {
		$('.successcontainer').slideToggle( "slow" );  
	}, 1000); 
	
	setTimeout(function()
    {
		$('.successcontainer').remove(); 
		validationSuccess(); 
	}, 2000);
}

function validationSuccess()
{
	//calling home Action Class
	location.href = "Projects/CreatedProjects";
}