/**
 * 
 */
 
 //Global Variables
 var storedFiles = [];
 var storedFileNames = [];
 var userArr = [];

//Reloading on PopState
window.onpopstate = function(event) 
{
  	window.location.reload();
};

//For Creating Success message divs
function createSuccessMessage(message)
{
	var div = document.createElement("div");
	var element = "<div class=\"container successcontainer breach-error-messages\" style=\"top:0px;\">\n" +
                "\t\t\t     <div class=\"row\">\n" +
                "\t\t\t         <div class=\"col-md-4\"></div>\n" +
                "\t\t\t         <div class=\"col-md-4 error-alert-col\">\n" +
                "\t\t\t             <div class=\"alert alert-success alert-dismissable\">\n" +
                "\t\t\t                 <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">×</a>\n" +
                "\t\t\t                 "+message+" \n" +
                "\t\t\t             </div>\n" +
                "\t\t\t         </div>\n" +
                "\t\t\t         <div class=\"col-md-4\"></div>\n" +
                "\t\t\t     </div>\n" +
                "\t\t \t</div>"
	div.innerHTML = element;
	document.body.appendChild(div);
	$('.successcontainer').css('display', 'block');
     
    setTimeout(function()
    {
		$('.successcontainer').slideToggle( "slow" );  
	}, 1000); 
	
	setTimeout(function()
    {
		$('.successcontainer').remove();  
	}, 2000);
	      
}

//Onload function to set nav activs and building modal DD's
$(function()
{
	debugger
	var currentURL = window.location.href;
	var reqToken = currentURL.split("/").pop();
	switch(reqToken)
	{
		case 'Tasks':
		{
			setNavActive('navTasks');
			buildModalDDs('Task', false);
			break;
		}
		case 'Milestones':
		{
			setNavActive('navMilestones');
			buildModalDDs('Milestone', false);
			break;
		}
		case 'Attachments':
		{
			setNavActive('navAttachments');
			buildModalDDs('', true);
			break;
		}
		case 'Charts':
		{
			setNavActive('navCharts');
			//Requesting chart data through ajax call
			requestChartDatas(currentURL);
			buildModalDDs('', true);
			break;
		}
		case 'Dashboard':
		{
			actvateSidebarModule('idSidebar-module-Dashboard');
			requestDashboardCharts();
			break;
		}
		case 'CreatedProjects':
		{
			actvateSidebarModule('idSidebar-module-CrtPrjcts');
			break;
		}
		case 'AssignedProjects':
		{
			actvateSidebarModule('idSidebar-module-AsgndPrjcts');
			break;
		}
		case 'CreatedTasks':
		{
			actvateSidebarModule('idSidebar-module-CrtTasks');
			break;
		}
		case 'AssignedTasks':
		{
			actvateSidebarModule('idSidebar-module-AsgndTasks');
			break;
		}
		case 'CreatedMilestones':
		{
			actvateSidebarModule('idSidebar-module-CrtMlstns');
			break;
		}
	}
});

//function to activate sidebar modules
function actvateSidebarModule(moduleID)
{
	$('.module-active').removeClass("module-active");
	$('#'+moduleID).addClass('module-active');
}

//function to build Modal DD's
function buildModalDDs(modalName, vanishMode)
{
	var ul = document.getElementById('idContentAdditionDD');
	ul.innerHTML = "";
	
	//returning after vanishing
	if(vanishMode)
	 	return; 
	 	
	if(modalName=='Task')
	{
		var prjctLi = document.createElement("li");
		prjctLi.innerHTML = '<p onclick="initSubModalValues(\''+modalName+'\')">Create '+modalName+'</p>';
		ul.appendChild(prjctLi);
	}
	else if(modalName=='Milestone')
	{
		var prjctLi = document.createElement("li");
		prjctLi.innerHTML = '<p onclick="initSubModalValues(\''+modalName+'\')">Create '+modalName+'</p>';
		ul.appendChild(prjctLi);
	}
	else
	{
		var prjctLi = document.createElement("li");
		prjctLi.innerHTML = '<p onclick="openUnitModal(\''+modalName+'\')">Create '+modalName+'</p>';
		ul.appendChild(prjctLi);
	}	
}


//Call to render new milestone and task modal list 
function initSubModalValues(unitName)
{
	//Getting cur Project ID through current URL
	var curProjectID = getCurProjectID();
		
	if(unitName=='Task')
	{
		$.ajax
		({
			type:'POST',
			url:'/ZProjects/Tasks/'+curProjectID+'/InitNewTaskModal',
			success: function(successResponse)
			{
				$('#idPPModals').html($(successResponse).find('#mainModal').html());

				openUnitModal(unitName)
			},
			error: function(errorResponse)
			{
				console.log(errorResponse);
			}
		});
	}
	else if(unitName=='Milestone')
	{
		$.ajax
		({
			type:'POST',
			url:'/ZProjects/Milestones/'+curProjectID+'/InitNewMilestoneModal',
			success: function(successResponse)
			{
				$('#idPPModals').html($(successResponse).find('#mainModal').html());

				openUnitModal(unitName)
			},
			error: function(errorResponse)
			{
				console.log(errorResponse);
			}
		});
	}
}

//Settting Nav Active for Selected nav
function setNavActive(navId)
{
	$('#'+navId).addClass('active');
}

//For Using User names under Tokenfield DD
$(function()
{
	$.ajax({
		url: '/ZProjects/Users/GetUserDetails',
		type: 'GET',
		dataType: 'JSON', 
		success: function(successResponse)
		{
			var userJSNArrObj = successResponse.userArr;
			//Itertaing and Adding all usernames in Array
			if(userJSNArrObj.length>0)
			{
				for(var curUserIndex=0; curUserIndex<userJSNArrObj.length; curUserIndex++)
				{
					userArr.push(userJSNArrObj[curUserIndex]);
				}
			}
		},
		error: function(errorResponse)
		{
			alert(errorResponse);
		} 
	});
});

 // !-- For File Handling of Appended Files
	
	//function to handle the file select listenere
	function appdendSelectedFiles(elem) 
	{
		//to check that even single file is selected or not
		if(!elem.target.files) 
				return;      
	
		//Enabling File List Group 
		document.getElementById('appendedFileListGrp').style.display = "block";
	
		//get the array of file object in files variable
		var files = elem.target.files;
		var filesArr = Array.prototype.slice.call(files);
	
		filesArr.forEach(function(curFile) 
		{
			//add new selected files into the array list
			storedFiles.push(curFile);
			//Its Names
			storedFileNames.push(curFile.name);
			//print new selected files into the given division
			document.getElementById("appendedFileListViewer").innerHTML += "<div class='filename'> <span> " + curFile.name + "</span></div>";
		});
	
		//store the array of file in our element this is send to other page by form submit
		//$("input[name=replyfiles]").val(storedFiles);
	}
	
	function clearStoredFiles()
	{
		storedFiles = [];
	}
	function clearAttchmntField(fieldID)
	{
		clearStoredFiles();
		document.getElementById("appendedFileListViewer").innerHTML = "";
		document.getElementById(fieldID).value='';
	
		//Disanbling File List Group 
		document.getElementById('appendedFileListGrp').style.display = "none";
	}
// For File Handling of Appended Files --!

//Function to init date field operations
function initDateFieldOps()
{
	// !-- For Filtering Date Operations
	$(".frmDateField").datepicker({
        todayHighlight: true,
        autoclose: true,
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('.toDateField').datepicker('setStartDate', minDate);
    });

    $(".toDateField").datepicker({
		todayHighlight: true,
	    autoclose: true
	})
    .on('changeDate', function (selected) {
        var maxDate = new Date(selected.date.valueOf());
        $('.frmDateField').datepicker('setEndDate', maxDate);
    });
	// For Filtering Date Operations --!
}

//Function to init Priority and Status field operations
function initUnitPSOps()
{
	$(".unitPriorityUL li a").click(function()
	{
	    var selText = $(this).text();
	    $(this).parents('.dropdown').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
	});
	$(".unitStatusUL li a").click(function()
	{
	    var selText = $(this).text();
	    $(this).parents('.dropdown').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
	});
}

//For Updating File Appending Function
function initFileAttachmentOps()
{
	document.querySelector('.fileAttachments').addEventListener('change', appdendSelectedFiles, false);
}

//Initializing Tokens using UserArray for ProjectMembers
function initProjectTokensArray()
{
	$('#idPrjctMembers').tokenfield
    ({
		autocomplete: 
        {
            source: userArr,
            delay: 100
        },
        showAutocompleteOnFocus: true
    })
    .on('tokenfield:createtoken', function (event) //For Avoiding Duplicate Values
    {
	    var existingTokens = $(this).tokenfield('getTokens');
	    $.each(existingTokens, function(index, token) 
	    {
	        if (token.value === event.attrs.value)
	        {
	            event.preventDefault();
	            
			    //Enabling Error
		        $('#id-invalidUnitMemberErr').css('display', 'block');
		        $('#id-invalidUnitMemberErr').html('Duplicate Members cannot be Added');
	        
		        //Highlighting required field
		        $('.unitMemberField').css('border-color', '#dc3545');
	    	}
	    });
	});
}

//Initializing Tokens using UserArray for TaskMembers
function initTaskTokensArray()
{
	$('#idTaskAssignee').tokenfield
    ({
		autocomplete: 
        {
            source: userArr,
            delay: 100
        },
        showAutocompleteOnFocus: true
    })
    .on('tokenfield:createtoken', function (event) 
    {
		if($(this).tokenfield('getTokens').length>=1)
		{
			event.preventDefault();
			
			//Enabling Error
	        $('#id-invalidUnitMemberErr').css('display', 'block');
	        $('#id-invalidUnitMemberErr').html('Maximum 1 Assignee can be added');
        
	        //Highlighting required field
	        $('.unitMemberField').css('border-color', '#dc3545');
		}
	});
}
	
//Function to get priority as int value
function getPriorityIntValue(txtValue)
{
	var priority = (txtValue.includes('Low'))?1:(txtValue.includes('Medium'))?2:3;
    return priority;
}

//Function to get Status as int value
function getStatusIntValue(txtValue)
{
	var status = (txtValue.includes('Open'))?1:(txtValue.includes('Development'))?2:(txtValue.includes('Testing'))?3:4;
    return status;
}

//Global function for replacing popup modals
function replaceModals(responseContent)
{
	$('#idPPModals').html($(responseContent).find('#idPPModals').html());
} 

//Function to triger modal opening and doing pre-processing of modal reqs
function openUnitModal(unitName)
{
	//Pre-Processing (initing the Reqs)
	initDateFieldOps();
	initUnitPSOps();
	initFileAttachmentOps();
	clearStoredFiles();
	 
	switch(unitName)
	{
		case 'Project':
		{
			initProjectTokensArray();
			$('#projectModal').modal();
			break;
		}
		case 'Task':
		{
			initTaskTokensArray();
			$('#taskModal').modal();
			break;
		}
		case 'Milestone':
		{
			$('#milestoneModal').modal();
			break;
		}
	}
}



//Validating Modal Fields 
function validateModalInputs(unitName)
{
    if(!$('.unitNameField').val())
    {
        //Enabling Error
        $('#id-invalidUnitNameErr').html('* '+unitName+' name should not be empty');
        $('#id-invalidUnitNameErr').css('display', 'block');
        
        //Highlighting required field
        $('.unitNameField').css({'border-color': '#dc3545', 'margin-bottom': '5px'});
        return false;
    }
    
    if(!$('.unitFrmDateField').val())
    {
        //Enabling Error
        $('#id-invalidUnitFrmDtErr').css('display', 'block');
        
        //Highlighting required field
        $('.unitFrmDateField').css('border-color', '#dc3545');
        return false;
    }
    
    if(!$('.unitToDateField').val())
    {
        //Enabling Error
        $('#id-invalidUnitToDtErr').css('display', 'block');
        
        //Highlighting required field
        $('.unitToDateField').css('border-color', '#dc3545');
        return false;
    }
    
    if($('.unitPriority').text().includes('Priority'))
    {
        //Enabling Error
        $('#id-invalidUnitPrtyErr').css('display', 'block');
        return false;
    }
    if($('.unitStatus').text().includes('Status'))
    {
        //Enabling Error
        $('#id-invalidUnitStatErr').css('display', 'block');
        return false;
    }

    return true;
}

//To make request for content rendering of user picked contents
function requestUserContent(url)
{
	$.ajax
	({
		url: url,
		type: 'GET',
		success : function(successResponse)
		{
			$('#idMain-unit-contents-div').html(jQuery(successResponse).find('#idMain-unit-contents-div').html());
			
			//Replacing Modals
			replaceModals(successResponse);
			
			//Pushing URL to History 
			window.history.pushState(null, '', url);
			
		},
		error : function(errorResponse)
		{
			alert(errorResponse);
		}
	});
}

//To filter user picked content from sidebar 
function renderUserContents(moduleName)
{
	switch(moduleName)
	{
		case 'CreatedProjects' :
		{
			requestUserContent('/ZProjects/Projects/CreatedProjects');
			buildModalDDs('Project', false);
			actvateSidebarModule('idSidebar-module-CrtPrjcts');
			break;
		}
		case 'AssignedProjects' :
		{
			requestUserContent('/ZProjects/Projects/AssignedProjects');
			actvateSidebarModule('idSidebar-module-AsgndPrjcts');
			buildModalDDs('Project', false);
			break;
		}
		case 'CreatedTasks' :
		{
			requestUserContent('/ZProjects/Tasks/CreatedTasks');
			actvateSidebarModule('idSidebar-module-CrtTasks');
			buildModalDDs('Project', false);
			break;
		}
		case 'AssignedTasks' :
		{
			requestUserContent('/ZProjects/Tasks/AssignedTasks');
			actvateSidebarModule('idSidebar-module-AsgndTasks');
			buildModalDDs('Project', false);
			break;
		}
		case 'CreatedMilestones' :
		{
			requestUserContent('/ZProjects/Milestones/CreatedMilestones');
			actvateSidebarModule('idSidebar-module-CrtMlstns');
			buildModalDDs('Project', false);
			break;
		}
		case 'Dashboard' :
		{
			requestDashboardCharts();
			actvateSidebarModule('idSidebar-module-Dashboard');
			buildModalDDs('Project', false);
			break;
		}
	}
}

//For requesting project contents 
function requestProjectContents(url)
{
	$.ajax
	({
		url: url,
		type: 'GET',
		success : function(successResponse)
		{
			$('#idMain-unit-contents-div').html(jQuery(successResponse).find('#idMain-unit-contents-div').html());
			
			//Replacing Modals
			replaceModals(successResponse);
			
			//Setting nav active using url
			var moduleNavSuffix = url.split("/").pop();
			setNavActive('nav'+moduleNavSuffix);
			
			
			//Pushing URL to History 
			window.history.pushState(null, '', url);
		},
		error : function(errorResponse)
		{
			alert(errorResponse);
		}
	});
}

//For requesting unit detailing contents
function requestUnitDetails(unitUrl, unitName)
{
	$.ajax
	({
		type:'GET',
		url: unitUrl,
		success: function(successResponse)
		{
			//replacing modals externally without calling replace modal function
			$('#idPPModals').html($(successResponse).find('#mainModal').html());

			openUnitModal(unitName)
			
			buildModalDDs('', true);
			
			//removing active for other nav elements
			$('.navList').removeClass("active");
			
			//setting details nav only as active
			setNavActive('navDetails');
		},
		error: function(errorResponse)
		{
			console.log(errorResponse);
		}
	});
}

function requestChartDatas(unitUrl)
{
	modifiedURL = unitUrl.replace('Charts', 'ChartDetails')
	$.ajax
	({
		url: modifiedURL,
		type: 'GET',
		dataType: 'JSON',
		success: function(successResposnse)
		{
			//Constructing the chart twice for Projects and once for Milestone (Unit based Rendering of charts)
			if(unitUrl.includes('/Projects'))
			{
				constructChart(successResposnse.TaskData.Data, successResposnse.TaskData.ColorData, taskChartsection, "Task", "Task Gant", "Gant for Associated Tasks");
				constructChart(successResposnse.MilestoneData.Data, successResposnse.MilestoneData.ColorData, mlstnChartsection, "Milestone", "Milestone Gant", "Gant for Associated Milestones");
			}
			else if(unitUrl.includes('/Milestones'))
			{
				constructChart(successResposnse.Data, successResposnse.ColorData, taskChartsection, "Task", "Task Gant", "Gant for Associated Tasks");
			}
				
			var moduleNavSuffix = unitUrl.split("/").pop();
			
			//removing active for other nav elements
			$('.navList').removeClass("active");
			//Setting nav active using url
			setNavActive('nav'+moduleNavSuffix);
			
			//Pushing URL to History 
			window.history.pushState(null, '', unitUrl);
		},
		error: function(errorResponse)
		{
			console.log(errorResponse);
		}
	});
}

function requestDashboardCharts()
{
	$.ajax
	({
		url: '/ZProjects/Dashboard',
		type: 'GET',
		success: function(successResponse)
		{
			$('#idMain-unit-contents-div').html(jQuery(successResponse).find('#idMain-unit-contents-div').html());
			
			$.ajax
			({
				url: '/ZProjects/DashboardDetails',
				type: 'GET',
				dataType: 'JSON',
				success: function(successResposnse)
				{
					constructDashboardCharts(successResposnse);
					
					//Pushing URL to History 
					window.history.pushState(null, '', '/ZProjects/Dashboard');
				},
				error: function(errorResponse)
				{
					console.log(errorResponse);
				}
			});
		},
		error: function(errorResponse)
		{
			console.log(errorResponse);
		}
	});
}

//For requesting Chart contents 
function requestChartSection(unitUrl)
{
	$.ajax
	({
		url: unitUrl,
		type: 'GET',
		success: function(successResponse)
		{
			$('#idMain-unit-contents-div').html(jQuery(successResponse).find('#idMain-unit-contents-div').html());
			
			requestChartDatas(unitUrl);
		},
		error: function(errorResponse)
		{
			console.log(errorResponse);
		}
	});
}
//To filter project contents requested
function renderProjectContents(contentName, projectID)
{
	switch(contentName)
	{
		case 'Tasks' :
		{
			requestProjectContents('/ZProjects/Projects/'+projectID+'/Tasks');
			buildModalDDs('Task', false);
			break;
		}
		case 'Milestones' :
		{
			requestProjectContents('/ZProjects/Projects/'+projectID+'/Milestones');
			buildModalDDs('Milestone', false);
			break;
		}
		case 'Attachments' :
		{
			requestProjectContents('/ZProjects/Projects/'+projectID+'/Attachments');
			buildModalDDs('', true);
			break;
		}
		case 'Charts' :
		{
			requestChartSection('/ZProjects/Projects/'+projectID+'/Charts');
			buildModalDDs('', true);
			break;
		}
		case 'Details' :
		{
			requestUnitDetails('/ZProjects/Projects/'+projectID+'/Details', 'Project')
			break;
		}
	}
} 

function requestTaskContents(url)
{
	$.ajax
	({
		url: url,
		type: 'GET',
		success : function(successResponse)
		{
			$('#idMain-unit-contents-div').html(jQuery(successResponse).find('#idMain-unit-contents-div').html());
			
			//Replacing Modals
			replaceModals(successResponse);
			
			//Setting nav active using url
			var moduleNavSuffix = url.split("/").pop();
			setNavActive('nav'+moduleNavSuffix);
			
			
			//Pushing URL to History 
			window.history.pushState(null, '', url);
		},
		error : function(errorResponse)
		{
			alert(errorResponse);
		}
	});
}
//To filter Task contents requested
function renderTaskContents(contentName, taskID)
{
	switch(contentName)
	{
		case 'Attachments' :
		{
			requestTaskContents('/ZProjects/Tasks/'+taskID+'/Attachments');
			buildModalDDs('', true);
			break;
		}
		case 'Details' :
		{
			requestUnitDetails('/ZProjects/Tasks/'+taskID+'/Details', 'Task')
			buildModalDDs('', true);
			break;
		}
	}
}

function requestMilestoneContents(url)
{
	$.ajax
	({
		url: url,
		type: 'GET',
		success : function(successResponse)
		{
			$('#idMain-unit-contents-div').html(jQuery(successResponse).find('#idMain-unit-contents-div').html());
			
			//Replacing Modals
			replaceModals(successResponse);
			
			//Setting nav active using url
			var moduleNavSuffix = url.split("/").pop();
			setNavActive('nav'+moduleNavSuffix);
			
			//Pushing URL to History 
			window.history.pushState(null, '', url);
		},
		error : function(errorResponse)
		{
			alert(errorResponse);
		}
	});
}
//To filter Milestone contents requested
function renderMilestoneContents(contentName, milestoneID)
{
	switch(contentName)
	{
		case 'Attachments' :
		{
			requestMilestoneContents('/ZProjects/Milestones/'+milestoneID+'/Attachments');
			buildModalDDs('', true);
			break;
		}
		case 'Charts' :
		{
			requestChartSection('/ZProjects/Milestones/'+milestoneID+'/Charts');
			buildModalDDs('', true);
			break;
		}
		case 'Details' :
		{
			requestUnitDetails('/ZProjects/Milestones/'+milestoneID+'/Details', 'Milestone')
			buildModalDDs('', true);
			break;
		}
	}
} 

//To Add new Contents
function addNewProject()
{
	if(validateModalInputs('Project'))
	{
		var projectNewFromDateArray = $('#idPrjctFrmDate').val().split('/');
		var projectNewFromDateStr = projectNewFromDateArray[2]+'-'+projectNewFromDateArray[0]+'-'+projectNewFromDateArray[1];
		var projectNewToDateArray = $('#idPrjctToDate').val().split('/');
		var projectNewToDateStr = projectNewToDateArray[2]+'-'+projectNewToDateArray[0]+'-'+projectNewToDateArray[1];
		var frmData = new FormData();
		
		//Adding Multiple Files and Their Names
		for(var curFileIndex=0; curFileIndex<storedFiles.length; curFileIndex++)
		{
			var curFile = storedFiles[curFileIndex];
			frmData.append('crtNewProjectFiles', curFile);
			var curFileName = storedFileNames[curFileIndex];
			frmData.append('crtNewProjectFilesName', curFileName);
		}
		
		frmData.append('crtNewProjectName', $('#idPrjctName').val());
		frmData.append('crtNewProjectDescription', $('#idPrjctDescript').val());
		frmData.append('crtNewProjectOwner', $('#idPrjctOwner').val());
		frmData.append('crtNewProjectMembers', $('#idPrjctMembers').val());
		frmData.append('crtNewProjectFrmDate', projectNewFromDateStr);
		frmData.append('crtNewProjectToDate', projectNewToDateStr);
		frmData.append('crtNewProjectPriority', getPriorityIntValue($('.unitPriority').text()));
		frmData.append('crtNewProjectStatus', getStatusIntValue($('.unitStatus').text()));
		$.ajax
		({	
			url:'/ZProjects/Projects/AddNewProject',
			type:'POST',
			processData: false,
			contentType: false,
			traditional:true,
			data:
				frmData,
			success: function(successResponse)
			{
				//Closing the Modal
				$('#projectModal').modal('toggle');
				
				//Ajax call for rendering all projects
				renderUserContents('CreatedProjects');
				
				//Project creation success message
				createSuccessMessage('Project Created Succesfully');
				
			},
			error: function(errorResponse)
			{
				alert(errorResponse);
			}
					
		 });
	}
}

//To Add new Contents
function editProjectDetails(projectID)
{
	if(validateModalInputs('Project'))
	{
		var projectNewFromDateArray = $('#idPrjctFrmDate').val().split('/');
		var projectNewFromDateStr = projectNewFromDateArray[2]+'-'+projectNewFromDateArray[0]+'-'+projectNewFromDateArray[1];
		var projectNewToDateArray = $('#idPrjctToDate').val().split('/');
		var projectNewToDateStr = projectNewToDateArray[2]+'-'+projectNewToDateArray[0]+'-'+projectNewToDateArray[1];
		var frmData = new FormData();
		
		//Adding Multiple Files and Their Names
		for(var curFileIndex=0; curFileIndex<storedFiles.length; curFileIndex++)
		{
			var curFile = storedFiles[curFileIndex];
			frmData.append('crtNewProjectFiles', curFile);
			var curFileName = storedFileNames[curFileIndex];
			frmData.append('crtNewProjectFilesName', curFileName);
		}
		
		frmData.append('crtNewProjectName', $('#idPrjctName').val());
		frmData.append('crtNewProjectDescription', $('#idPrjctDescript').val());
		frmData.append('crtNewProjectOwner', $('#idPrjctOwner').val());
		frmData.append('crtNewProjectMembers', $('#idPrjctMembers').val());
		frmData.append('crtNewProjectFrmDate', projectNewFromDateStr);
		frmData.append('crtNewProjectToDate', projectNewToDateStr);
		frmData.append('crtNewProjectPriority', getPriorityIntValue($('.unitPriority').text()));
		frmData.append('crtNewProjectStatus', getStatusIntValue($('.unitStatus').text()));
		$.ajax
		({	
			url:'/ZProjects/Projects/'+projectID+'/EditProject',
			type:'POST',
			processData: false,
			contentType: false,
			traditional:true,
			data:
				frmData,
			success: function(successResponse)
			{
				//Closing the Modal
				$('#projectModal').modal('toggle');
				
				//Ajax call for rendering all projects
				renderUserContents('CreatedProjects');
				
				//Project creation success message
				createSuccessMessage('Project Edited Succesfully');
			},
			error: function(errorResponse)
			{
				alert(errorResponse);
			}
					
		 });
	}
}

function getCurProjectID()
{
	var curURL = window.location.href;
	var splittedURL = curURL.split("/");
	splittedURL.pop();
	return splittedURL.pop();
}

//Adding New Task 
function addNewTask()
{
	if(validateModalInputs('Task'))
	{
		//Getting cur Project ID through current URL
		var curProjectID = getCurProjectID();
		var choosenMlstn = $('input[name="chosenMlstn"]:checked').val();
		
		//Throwing Error in Struts XML so Declaring it as 0 if nothing is selected
		if(choosenMlstn==undefined)
		{
			choosenMlstn=0;
		}
		
		var taskNewFrmDateArray = $('#idTaskFrmDate').val().split('/');
		var idTaskFrmDateStr = taskNewFrmDateArray[2]+'-'+taskNewFrmDateArray[0]+'-'+taskNewFrmDateArray[1];
		var taskNewToDateArray = $('#idTaskToDate').val().split('/');
		var taskNewToDateStr = taskNewToDateArray[2]+'-'+taskNewToDateArray[0]+'-'+taskNewToDateArray[1];
		var frmData = new FormData();
		//Adding Multiple Files and Their Names
		for(var curFileIndex=0; curFileIndex<storedFiles.length; curFileIndex++)
		{
			var curFile = storedFiles[curFileIndex];
			frmData.append('crtNewTaskFiles', curFile);
			var curFileName = storedFileNames[curFileIndex];
			frmData.append('crtNewTaskFilesName', curFileName);
		}
		frmData.append('crtNewTaskMlstnID', choosenMlstn);
		frmData.append('crtNewTaskName', $('#idTaskName').val());
		frmData.append('crtNewTaskDescription', $('#idTaskDescript').val());
		frmData.append('crtNewTaskOwner', $('#idTaskOwner').val());
		frmData.append('crtNewTaskAssignee', $('#idTaskAssignee').val());
		frmData.append('crtNewTaskFrmDate', idTaskFrmDateStr);
		frmData.append('crtNewTaskToDate', taskNewToDateStr);
		frmData.append('crtNewTaskPriority', getPriorityIntValue($('.unitPriority').text()));
		frmData.append('crtNewTaskStatus', getStatusIntValue($('.unitStatus').text()));
		
		$.ajax
		({
			type:'POST',
			url: '/ZProjects/Tasks/'+curProjectID+'/AddNewTask',
			processData: false,
			contentType: false,
			traditional:true,
			data:
				frmData,
			success: function(successResponse)
			{
				$('#taskModal').modal('toggle');
				
				//Ajax call for rendering all projects
				renderProjectContents('Tasks', curProjectID);
				
				//Project creation success message
				createSuccessMessage('Task Created Succesfully');
			},
			error: function(errorResponse)
			{
				console.log(errorResponse);
			}
		});
	}
}

function editTaskDetails(taskID)
{
	if(validateModalInputs('Task'))
	{
		//Getting cur Project ID through current URL
		
		var choosenMlstn = $('input[name="chosenMlstn"]:checked').val();
		
		//Throwing Error in Struts XML so Declaring it as 0 if nothing is selected
		if(choosenMlstn==undefined)
		{
			choosenMlstn=0;
		}
		
		var taskNewFrmDateArray = $('#idTaskFrmDate').val().split('/');
		var idTaskFrmDateStr = taskNewFrmDateArray[2]+'-'+taskNewFrmDateArray[0]+'-'+taskNewFrmDateArray[1];
		var taskNewToDateArray = $('#idTaskToDate').val().split('/');
		var taskNewToDateStr = taskNewToDateArray[2]+'-'+taskNewToDateArray[0]+'-'+taskNewToDateArray[1];
		var frmData = new FormData();
		//Adding Multiple Files and Their Names
		for(var curFileIndex=0; curFileIndex<storedFiles.length; curFileIndex++)
		{
			var curFile = storedFiles[curFileIndex];
			frmData.append('crtNewTaskFiles', curFile);
			var curFileName = storedFileNames[curFileIndex];
			frmData.append('crtNewTaskFilesName', curFileName);
		}
		frmData.append('crtNewTaskMlstnID', choosenMlstn);
		frmData.append('crtNewTaskName', $('#idTaskName').val());
		frmData.append('crtNewTaskDescription', $('#idTaskDescript').val());
		frmData.append('crtNewTaskOwner', $('#idTaskOwner').val());
		frmData.append('crtNewTaskAssignee', $('#idTaskAssignee').val());
		frmData.append('crtNewTaskFrmDate', idTaskFrmDateStr);
		frmData.append('crtNewTaskToDate', taskNewToDateStr);
		frmData.append('crtNewTaskPriority', getPriorityIntValue($('.unitPriority').text()));
		frmData.append('crtNewTaskStatus', getStatusIntValue($('.unitStatus').text()));
		
		$.ajax
		({
			type:'POST',
			url: '/ZProjects/Tasks/'+taskID+'/EditTask',
			processData: false,
			contentType: false,
			traditional:true,
			data:
				frmData,
			dataType: 'JSON',
			success: function(successResponse)
			{
				$('#taskModal').modal('toggle');
				
				var assctdProjectID = successResponse.AssociatedProjectID;
				//Ajax call for rendering all projects
				renderProjectContents('Tasks', assctdProjectID);
				
				//Project creation success message
				createSuccessMessage('Task Edited Succesfully');
			},
			error: function(errorResponse)
			{
				console.log(errorResponse);
			}
		});
	}
}
 
//Adding New Milestone
function addNewMilestone()
{
	if(validateModalInputs('Milestone'))
	{
		//Getting cur Project ID through current URL
		var curProjectID = getCurProjectID();
		
		var checkedTasksID = $('input[name="checkTask"]:checked').map(function() {
							    return $(this).attr('id');
							  }).get();
							  
		var mlstnNewFrmDateArray = $('#idMlstnFrmDate').val().split('/');
		var mlstnFrmDateStr = mlstnNewFrmDateArray[2]+'-'+mlstnNewFrmDateArray[0]+'-'+mlstnNewFrmDateArray[1];
		var mlstnNewToDateArray = $('#idMlstnToDate').val().split('/');
		var mlstnNewToDateStr = mlstnNewToDateArray[2]+'-'+mlstnNewToDateArray[0]+'-'+mlstnNewToDateArray[1];
		var frmData = new FormData();
		//Adding Multiple Files and Their Names
		for(var curFileIndex=0; curFileIndex<storedFiles.length; curFileIndex++)
		{
			var curFile = storedFiles[curFileIndex];
			frmData.append('crtNewMlstnFiles', curFile);
			var curFileName = storedFileNames[curFileIndex];
			frmData.append('crtNewMlstnFilesName', curFileName);
		}
		frmData.append('crtNewMlstnName', $('#idMlstnName').val());
		frmData.append('crtNewMlstnDescription', $('#idMlstnDescript').val());
		frmData.append('crtNewMlstnOwner', $('#idMlstnOwner').val());
		frmData.append('crtNewMlstnTasksID', checkedTasksID);
		frmData.append('crtNewMlstnFrmDate', mlstnFrmDateStr);
		frmData.append('crtNewMlstnToDate', mlstnNewToDateStr);
		frmData.append('crtNewMlstnPriority', getPriorityIntValue($('.unitPriority').text()));
		frmData.append('crtNewMlstnStatus', getStatusIntValue($('.unitStatus').text()));
		
		$.ajax
		({
			type:'POST',
			url: '/ZProjects/Milestones/'+curProjectID+'/AddNewMilestone',
			processData: false,
			contentType: false,
			traditional:true,
			data:
				frmData,
			success: function(successResponse)
			{
				$('#milestoneModal').modal('toggle');
				
				//Ajax call for rendering all projects
				renderProjectContents('Milestones', curProjectID);
				
				//Project creation success message
				createSuccessMessage('Milestone Created Succesfully');
			},
			error: function(errorResponse)
			{
				alert(errorResponse);
			}
		});
	}
}

//Adding New Milestone
function editMilestoneDetails(milestoneID)
{
	if(validateModalInputs('Milestone'))
	{
		var checkedTasksID = $('input[name="checkTask"]:checked').map(function() {
							    return $(this).attr('id');
							  }).get();
							  
		var mlstnNewFrmDateArray = $('#idMlstnFrmDate').val().split('/');
		var mlstnFrmDateStr = mlstnNewFrmDateArray[2]+'-'+mlstnNewFrmDateArray[0]+'-'+mlstnNewFrmDateArray[1];
		var mlstnNewToDateArray = $('#idMlstnToDate').val().split('/');
		var mlstnNewToDateStr = mlstnNewToDateArray[2]+'-'+mlstnNewToDateArray[0]+'-'+mlstnNewToDateArray[1];
		var frmData = new FormData();
		//Adding Multiple Files and Their Names
		for(var curFileIndex=0; curFileIndex<storedFiles.length; curFileIndex++)
		{
			var curFile = storedFiles[curFileIndex];
			frmData.append('crtNewMlstnFiles', curFile);
			var curFileName = storedFileNames[curFileIndex];
			frmData.append('crtNewMlstnFilesName', curFileName);
		}
		frmData.append('crtNewMlstnName', $('#idMlstnName').val());
		frmData.append('crtNewMlstnDescription', $('#idMlstnDescript').val());
		frmData.append('crtNewMlstnTasksID', checkedTasksID);
		frmData.append('crtNewMlstnFrmDate', mlstnFrmDateStr);
		frmData.append('crtNewMlstnToDate', mlstnNewToDateStr);
		frmData.append('crtNewMlstnPriority', getPriorityIntValue($('.unitPriority').text()));
		frmData.append('crtNewMlstnStatus', getStatusIntValue($('.unitStatus').text()));
		
		$.ajax
		({
			type:'POST',
			url: '/ZProjects/Milestones/'+milestoneID+'/EditMilestone',
			processData: false,
			contentType: false,
			traditional:true,
			data:
				frmData,
			success: function(successResponse)
			{
				$('#milestoneModal').modal('toggle');
				
				var assctdProjectID = successResponse.AssociatedProjectID;
				//Ajax call for rendering all projects
				renderProjectContents('Milestones', assctdProjectID);
				
				//Project creation success message
				createSuccessMessage('Milestone Edited Succesfully');
			},
			error: function(errorResponse)
			{
				alert(errorResponse);
			}
		});
	}	
}
 
//JQuery Event for handling table expansion
function displayMilestoneSubTasks(mlstnID) 
{
	if($('#mlstnSubTbl'+mlstnID).length==0)
	{
		var constructedTable = "\t<tr id=\"mlstnSubTbl"+mlstnID+"\" class=\"mlstnSubTaskCls table row-child collapse in\" aria-expanded=\"true\">\n" +
                    "\t\t<td>"+
                    "\t\t<table class=\"table align-middle mb-0 bg-white subTable-list-table\" id=\"idSubTable-list-table\">\n" +
                    "\t\t                <thead class=\"bg-light\">\n" +
                    "\t\t                  <tr>\n" +
                    "\t\t\t\t\t            <th>Task Name</th>\n" +
                    "\t\t\t\t\t            <th>Owner</th>\n" +
                    "\t\t\t\t\t            <th>Start Date</th>\n" +
                    "\t\t\t\t\t            <th>Due Date</th>\n" +
                    "\t\t\t\t\t            <th>Assignee</th>\n" +
                    "\t\t\t\t\t            <th>Priority</th>\n" +
                    "\t\t\t\t\t            <th>Status</th>\n" +
                    "\t\t\t\t\t            </tr>\n" +
                    "\t\t                </thead>\n" +
                    "\t\t                <tbody>\n";
		//Ajax call for getting table data
		$.ajax
		({
			url: '/ZProjects/Milestones/MilestoneTasks/'+mlstnID,	
			type: 'GET',
			dataType: 'JSON', 
			async : true,
			success: function(successResponse)
			{
				var jsonObj = successResponse.TaskArray;
				if(jsonObj.length>0)
				{
					var constrRows = "";
					for(var curIndex=0; curIndex<jsonObj.length; curIndex++)
					{
						var curObj = jsonObj[curIndex];
						var curRow = 
						"\t\t                <tr draggable=\"true\">\n" +
	                    "\t\t                <td>"+curObj.TaskName+"</td>\n" +
	                    "\t\t                <td>"+curObj.Owner+"</td>\n" +
	                    "\t\t                <td>"+curObj.StartDate+"</td>\n" +
	                    "\t\t                <td>"+curObj.DueDate+"</td>\n" +
	                    "\t\t                <td>"+curObj.Assignee+"</td>\n" +
	                    "\t\t                <td>\n" +
	                    "\t\t                <span class='badge badge-"+curObj.Priority+" rounded-pill d-inline' >"+curObj.Priority+"</span>\n" +
	                    "\t\t                </td>\n" +
	                    "\t\t                <td>\n" +
	                    "\t\t                <span class='badge badge-"+curObj.Status+" rounded-pill d-inline' >"+curObj.Status+"</span>\n" +
	                    "\t\t                </td>\n" +
	                    "\t\t                </tr>\n";
						constrRows += curRow; 
					}
					constructedTable += constrRows;
					constructedTable += "\t\t   \t\t\t\t</tbody>\n" +
	                    "\t\t\t\t \t</table>"+
	                    "\t\t</td>"+
	                    "\t</tr>";
					$(constructedTable).insertAfter($('#idMlstn'+mlstnID).closest('tr'));
				}
				else
				{
					var emptyTable = "\t<tr id=\"mlstnSubTbl"+mlstnID+"\" class=\"mlstnSubTaskCls table row-child collapse in\" aria-expanded=\"true\">\n" +
                    "\t\t<td>"+
                    "\t\t<table class=\"table align-middle mb-0 bg-white subTable-list-table\" id=\"idSubTable-list-table\">\n" +
                    "\t\t                <tbody>\n"+
                    "\t\t                <tr draggable=\"true\">\n" +
                    "\t\t                <td style=\"color:grey;\"> No Associated tasks </td>\n" +
                    "\t\t                </tr>\n";
                    "\t\t   \t\t\t\t</tbody>\n" +
                    "\t\t\t\t \t</table>"+
                    "\t\t</td>"+
                    "\t</tr>";
                    
                    $(emptyTable).insertAfter($('#idMlstn'+mlstnID).closest('tr'));
				}
			}, 
			error: function(errorResponse)
			{
				console.log(errorResponse);
			}
		});
	}
} 

//Function to construct chart
function constructChart(data, colorData, sectionName, unitName,  titleName, descriptionName)
{
	var chartDataObj = 
	{
        "seriesdata": 
        {
            "chartdata": 
            [
                {
                    "type": "gantt",
                    "data": 
                    [
                        
                    ]
                }
            ]
        },
        "metadata": 
        {
            "axes": 
            {
                "x": [ 0 ],
                "y": 
                [ 
                    [ 1 ] 
                ],
                "tooltip": 
                [
                    "<span style='font-size:18px;font-weight:bold'>{{val(0)}}</span>",
                    1,
                    2,
                    3
                ],
                "label": 
                [
                    3
                ]
            },
            "columns": 
            [
                {
                    "dataindex": 0,
                    "columnname": "Task",
                    "datatype": "ordinal"
                },
                {
                    "dataindex": 1,
                    "columnname": "Duration",
                    "datatype": "time",
                    "time": 
                    {
                        "subfunction": "DATE",
                        "inputformat": "YYYY mm dd",
                        "format": 
                        {
                            "specifier": "MMM dd"
                        }
                    },
                    "numeric": 
                    {
                        "format": 
                        {
                            "type": "SIprefix"
                        }
                    }
                },
                {
                    "dataindex": 2,
                    "datatype": "ordinal",
                    "columnname": "Owner"
                },
                {
                    "dataindex": 3,
                    "columnname": "Assignee",
                    "datatype": "ordinal"
                }
            ]
        },
        "chart": 
        {
            "axes": 
            {
                "rotated": true,
                "xaxis": 
                {
                    "reversed": true,
                    "label": 
                    {
                        "show": true,
                        "text": unitName+" Name"
                    },
                    "axisline": 
                    {
                        "show": false
                    }
                },
                "yaxis": 
                [
                    {
                        "orient": "top",
                        "label": 
                        {
                            "text": unitName+" Duration"
                        },
                        "grid": {},
                        "axisline":
                        {
                            "show": true
                        }
                    }
                ]
            },
            "plot": 
            {
                "plotoptions": 
                {
                    "gantt": 
                    {
                        "multiColoring": true,
                        "maxBandWidth": 30,
                        "fillOpacity": 0.3,
                        "border": 
                        {
                            "show": true,
                            "size": 1,
                            "style": "solid",
                            "radius": 0,
                            "color": null
                        },
                        "levelMarker": 
                        {
                            "enabled": true,
                            "color": null,
                            "fillOpacity": 0.8,
                            "bandWidth": 20
                        },
                        "datalabels": 
                        {
                            "fontStyle": "italic"
                        }
                    }
                },
                "renderer": {},
                "border": 
                {
                    "show": true
                }
            },
            "marginBottom": "20",
            "marginRight": 20
        },
        "legend": 
        {
            "colors": 
            [
                
            ]
        },
        "tooltip": 
        {
            "backgroundColor": "white",
            "opacity": 1,
            "layout": "horizontal",
            "fontColor": "rgba(0,0,0,0.7)",
            "shadow": "2px 2px 2px rgba(0,0,0,0.3)",
            "borderRadius": "20",
            "borderWidth": "10 2 2 2"
        },
        "canvas": 
        {
            "title": 
            {
                "text": titleName,
                "hAlign": "left"
            },
            "subtitle": 
            {
				"text": descriptionName,
                "hAlign": "left"
            }
        }
    }
    
    chartDataObj.seriesdata.chartdata[0].data = data;
    chartDataObj.legend.colors = colorData;
    var chartObj = new $ZC.charts(sectionName, chartDataObj);
}

//Function to construct Dashboard charts
function constructDashboardCharts(responseObj)
{
	let barChartData = 
	{
		"canvas": 
		{
			"theme": "flatui",
			"title": 
			{
				"text": ""
			},
			"subtitle": 
	        {
	            "text": ""
	        }
		},
		"seriesdata": 
		{
			"chartdata": 
			[
				{
					"type": "bar",
					"data": 
					[
						
					]
				}
			]
		},
		"metadata": 
		{
			"axes": 
			{
				"x": 
				[
					0
				],
				"y": 
				[
					[
						1
					]
				],
				"tooltip": 
				[
					0,
					1
				]
			},
			"columns": 
			[
				{
					"dataindex": 0,
					"columnname": "Status",
					"datatype": "ordinal"
				},
				{
					"dataindex": 1,
					"columnname": "",
					"datatype": "numeric"
				}
			]
		},
		"chart": 
		{
			"axes": 
			{
				"xaxis": 
				{
					"label": 
					{
						"text": "Status"
					},
					"ticklabel": 
					{
						"rotation": "auto"
					},
					"axisline": 
					{
						"color": "transparent"
					}
				},
				"yaxis": 
				[
					{
						"label": 
						{
							"text": ""
						}
					}
				]
			},
			"plot": 
			{
				"renderer": 
				{
					"mode": "SVG"
				}
			}
		},
		"legend": 
	    {
			"colors":
			[
				 '#004c6d'
			]
		},
	}
	
	let pieChartData = 
	{
		"legend": 
	    {
			"layout": "vertical",
			"colors":
			[
				 '#5866C2',
				 '#FDAA29',
				 '#EF4F85',
				 '#7DCF41'
			]
		},
		"chart": 
	    {
			"plot": 
	        {
				"plotoptions": 
	            {
					"pie": 
	                {
						"gradients": 
	                    {
							"type": "linear",
							"options":
	                        {
								"linear": 
	                            {
									"x1": "0",
									"x2": "0",
									"y1": "0",
									"y2": "100",
									"gradientUnits": "objectBoundingBox",
									"colorGamma": 
	                                [
										0.55,
										0
									]
								}
							}
						}
					}
				}
			}
		},
		"metadata": 
	    {
			"axes": 
	        {
				"x": [0],
				"y": [ [ 1 ] ],
				"tooltip": 
	            [
					0,
					1,
					"Percentage : {{per(1)}}"
				]
			},
			"columns": 
	        [
				{
					"dataindex": 0,
					"columnname": "Status",
					"datatype": "ordinal"
				},
				{
					"dataindex": 1,
					"columnname": "",
					"datatype": "numeric"
				}
			]
		},
		"seriesdata": 
	    {
			"chartdata": 
	        [
				{
					"type": "pie",
					"data": 
	                [
						
					]
				}
			]
		},
		"canvas": 
	    {
			"title": 
	        {
				"text": ""
			},
	        "subtitle": 
	        {
	            "text": ""
	        }
		}
	}
	
	var clstrdUserChartData = 
    {
		"seriesdata": 
		{
			"chartdata": 
			[
				{
					"type": "bar",
					"seriesname": "Created",
					"data": 
					[
						[
							
						]
					]
				},
				{
					"type": "bar",
					"seriesname": "Assigned",
					"data": 
					[
						[
	                        
						]
					]
				}
			]
		},
		"metadata": 
		{
			"axes": 
			{
				"x": 
				[
					0
				],
				"y": 
				[
					[
						1
					]
				],
				"clr": 
				[
					2
				],
				"tooltip": 
				[
					0,
					1,
					2
				]
			},
			"columns": 
	        [
				{
					"dataindex": 0,
					"columnname": "User",
					"datatype": "ordinal"
				},
				{
					"dataindex": 1,
					"columnname": "",
					"datatype": "numeric"
				},
	            {
					"columnname": ""
				}
			]
		},
		"legend": 
		{
			"layout": "vertical",
			"colors": 
			[
				"#003f5c",
				"#346888"
			]
		},
		"chart": 
		{
			"axes": 
			{
				"xaxis": 
				{
					"categories": 
	                [
						
					],
					"label": 
	                {
						"text": "Users"
					},
					"ticklabel": 
	                {
						"alignMode": "rotate"
					}
				},
				"yaxis": 
				[
					{
						"label": 
	                    {
							"text": ""
						}
					}
				]
			},
			"plot": 
			{
				"plotoptions": 
				{
					"bar": 
					{
						"interPadding": 0.07,
						"gradients": 
						{
							"type": "linear",
							"options": 
							{
								"linear": 
								{
									"y2": 100,
									"colorGamma": 
									[
										0.4,
										0
									]
								}
							}
						}
					}
				}
			}
		},
		"canvas": 
		{
			"title": 
			{
				"text": ""
			},
			"subtitle": 
			{
				"text": ""
			}
		}
	}

	let ownPrjctChartData = jQuery.extend(true, {}, pieChartData);
	ownPrjctChartData.seriesdata.chartdata[0].data = responseObj.OwnProjectData;
	ownPrjctChartData.metadata.columns[1].columnname = "No of Projects";
    ownPrjctChartData.canvas.title.text = "Created Projects";
    ownPrjctChartData.canvas.subtitle.text = "Pie Visualization of Created Projects";
    var ownPrjctDataObj = new $ZC.charts(ownPrjctChartSection, ownPrjctChartData);
    
    let asgndPrjctChartData = jQuery.extend(true, {}, barChartData);
    asgndPrjctChartData.seriesdata.chartdata[0].data = responseObj.AsgndProjectData;
    asgndPrjctChartData.metadata.columns[1].columnname = "No of Projects";
    asgndPrjctChartData.canvas.title.text = "Assigned Projects";
    asgndPrjctChartData.canvas.subtitle.text = "Bar Visualization of Assigned Projects";
    asgndPrjctChartData.chart.text = "Assigned Projects";
    var asgndPrjctDataObj = new $ZC.charts(asgndPrjctChartSection, asgndPrjctChartData);
    
    let ownTaskChartData = jQuery.extend(true, {}, pieChartData);
    ownTaskChartData.seriesdata.chartdata[0].data = responseObj.OwnTaskData;
    ownTaskChartData.metadata.columns[1].columnname = "No of Tasks";
    ownTaskChartData.canvas.title.text = "Created Tasks";
    ownTaskChartData.canvas.subtitle.text = "Pie Visualization of Created Tasks";
    var ownTaskDataObj = new $ZC.charts(ownTaskChartSection, ownTaskChartData);
    
    let asgndTaskChartData = jQuery.extend(true, {}, barChartData);
    asgndTaskChartData.seriesdata.chartdata[0].data = responseObj.AsgndTaskData;
    asgndTaskChartData.metadata.columns[1].columnname = "No of Tasks";
    asgndTaskChartData.canvas.title.text = "Assigned Tasks";
    asgndTaskChartData.canvas.subtitle.text = "Bar Visualization of Assigned Tasks";
    asgndTaskChartData.chart.text = "Assigned Tasks";
    var asgndTaskDataObj = new $ZC.charts(asgndTaskChartSection, asgndTaskChartData);
    
    let clstrdProjectChartData = jQuery.extend(true, {}, clstrdUserChartData);
    clstrdProjectChartData.seriesdata.chartdata[0].data = responseObj.UserCreatedProjectData;
    clstrdProjectChartData.seriesdata.chartdata[1].data = responseObj.UserAssignedProjectData;
    clstrdProjectChartData.metadata.columns[1].columnname = "Projects";
    clstrdProjectChartData.metadata.columns[2].columnname = "Project Base";
    //clstrdProjectChartData.chart.axes.xaxis.categories = responseObj.UserList;
    clstrdProjectChartData.chart.axes.yaxis[0].label.text = "Projects";
    clstrdProjectChartData.canvas.title.text = "Users Projects";
    clstrdProjectChartData.canvas.subtitle.text = "Clustered Bar Visualization of Users Created and Assigned Projects";
    var clstrdProjectDataObj = new $ZC.charts(usersProjectChartSection, clstrdProjectChartData);
    
    let clstrdTaskChartData = jQuery.extend(true, {}, clstrdUserChartData);
    clstrdTaskChartData.seriesdata.chartdata[0].data = responseObj.UserCreatedTaskData;
    clstrdTaskChartData.seriesdata.chartdata[1].data = responseObj.UserAssignedTaskData;
    clstrdTaskChartData.metadata.columns[1].columnname = "Tasks";
    clstrdTaskChartData.metadata.columns[2].columnname = "Tasks Base";
    //clstrdTaskChartData.chart.axes.xaxis.categories = responseObj.UserList;
    clstrdTaskChartData.chart.axes.yaxis[0].label.text = "Tasks";
    clstrdTaskChartData.canvas.title.text = "Users Tasks";
    clstrdTaskChartData.canvas.subtitle.text = "Clustered Bar Visualization of Users Created and Assigned Tasks";
    var clstrdTaskDataObj = new $ZC.charts(usersTaskChartSection, clstrdTaskChartData);
    
}
//For Loggin out the User
function logoutUser()
{
	location.href = "/ZProjects/Logout";
}