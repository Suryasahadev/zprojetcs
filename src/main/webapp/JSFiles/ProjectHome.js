var crtNewProjectPriority = 0;
var crtNewProjectStatus = 0;

function setProjectPriority(priority)
{
	crtNewProjectPriority = priority;
}
function setProjectStatus(status)
{
	crtNewProjectStatus = status;
}

var crtNewTaskPriority = 0;
var crtNewTaskStatus = 0;

function setTaskPriority(priority)
{
	crtNewTaskPriority = priority;
}
function setTaskStatus(status)
{
	crtNewTaskStatus = status;
}

var crtNewMlstnPriority = 0;
var crtNewMlstnStatus = 0;

function setMlstnPriority(priority)
{
	debugger
	crtNewMlstnPriority = priority;
}
function setMlstnStatus(status)
{
	crtNewMlstnStatus = status;
}

//Validating Modal Fields 

function validateModalInputs(unitName)
{
    if(!$('.unitNameField').val())
    {
        //Enabling Error
        $('#id-invalidUnitNameErr').html('* '+unitName+' name should not be empty');
        $('#id-invalidUnitNameErr').css('display', 'block');
        
        //Highlighting required field
        $('.unitNameField').css({'border-color': '#dc3545', 'margin-bottom': '5px'});
        return false;
    }
    
    if(!$('.unitFrmDateField').val())
    {
        //Enabling Error
        $('#id-invalidUnitFrmDtErr').css('display', 'block');
        
        //Highlighting required field
        $('.unitFrmDateField').css('border-color', '#dc3545');
        return false;
    }
    
    if(!$('.unitToDateField').val())
    {
        //Enabling Error
        $('#id-invalidUnitToDtErr').css('display', 'block');
        
        //Highlighting required field
        $('.unitToDateField').css('border-color', '#dc3545');
        return false;
    }
    
    if($('.unitPriority').text().includes('Priority'))
    {
        //Enabling Error
        $('#id-invalidUnitPrtyErr').css('display', 'block');
        return false;
    }
    if($('.unitStatus').text().includes('Status'))
    {
        //Enabling Error
        $('#id-invalidUnitStatErr').css('display', 'block');
        return false;
    }

    return true;
}

//Initializing Tokens using UserArray for ProjectMembers
function initiateProjectTokensArray()
{
	$('#idPrjctMembers').tokenfield
    ({
		autocomplete: 
        {
            source: userArr,
            delay: 100
        },
        showAutocompleteOnFocus: true
    })
    .on('tokenfield:createtoken', function (event) //For Avoiding Duplicate Values
    {
	    var existingTokens = $(this).tokenfield('getTokens');
	    $.each(existingTokens, function(index, token) 
	    {
	        if (token.value === event.attrs.value)
	        {
	            event.preventDefault();
	            
			    //Enabling Error
		        $('#id-invalidUnitMemberErr').css('display', 'block');
		        $('#id-invalidUnitMemberErr').html('Duplicate Members cannot be Added');
	        
		        //Highlighting required field
		        $('.unitMemberField').css('border-color', '#dc3545');
	    	}
	    });
	});
}

//Initializing Tokens using UserArray for TaskMembers
function initiateTaskTokensArray()
{
	$('#idTaskAssignee').tokenfield
    ({
		autocomplete: 
        {
            source: userArr,
            delay: 100
        },
        showAutocompleteOnFocus: true
    })
    .on('tokenfield:createtoken', function (event) 
    {
		if($(this).tokenfield('getTokens').length>=1)
		{
			event.preventDefault();
			
			//Enabling Error
	        $('#id-invalidUnitMemberErr').css('display', 'block');
	        $('#id-invalidUnitMemberErr').html('Maximum 1 Assignee can be added');
        
	        //Highlighting required field
	        $('.unitMemberField').css('border-color', '#dc3545');
		}
	});
}

//For Using User names under Tokenfield DD
var userArr = [];

$(function()
{
	$.ajax({
		url: '/TMS_Web/Home/GetUserDetails',
		type: 'GET',
		dataType: 'JSON', 
		success: function(successResponse)
		{
			var userJSNArrObj = successResponse.userArr;
			//Itertaing and Adding all usernames in Array
			if(userJSNArrObj.length>0)
			{
				for(var curUserIndex=0; curUserIndex<userJSNArrObj.length; curUserIndex++)
				{
					userArr.push(userJSNArrObj[curUserIndex]);
				}
			}
			
			initiateProjectTokensArray();
		},
		error: function(errorResponse)
		{
			alert(errorResponse);
		} 
	});
});
// !-- For File Handling of Appended Files
	var storedFiles = [];
	var storedFileNames = [];
	//function to handle the file select listenere
	function appdendSelectedFiles(elem) 
	{
		//to check that even single file is selected or not
		if(!elem.target.files) 
				return;      
	
		//Enabling File List Group 
		document.getElementById('appendedFileListGrp').style.display = "block";
	
		//get the array of file object in files variable
		var files = elem.target.files;
		var filesArr = Array.prototype.slice.call(files);
	
		filesArr.forEach(function(curFile) 
		{
			//add new selected files into the array list
			storedFiles.push(curFile);
			//Its Names
			storedFileNames.push(curFile.name);
			//print new selected files into the given division
			document.getElementById("appendedFileListViewer").innerHTML += "<div class='filename'> <span> " + curFile.name + "</span></div>";
		});
	
		//store the array of file in our element this is send to other page by form submit
		//$("input[name=replyfiles]").val(storedFiles);
	}
	
	function clearStoredFiles()
	{
		storedFiles = [];
	}
	function clearAttchmntField(fieldID)
	{
		clearStoredFiles();
		document.getElementById("appendedFileListViewer").innerHTML = "";
		document.getElementById(fieldID).value='';
	
		//Disanbling File List Group 
		document.getElementById('appendedFileListGrp').style.display = "none";
	}
// For File Handling of Appended Files --!


/*$(document).ajaxComplete(function(ev, jqXHR, settings) {
      var stateObj = 
      { 
		url: settings.url, 
      	innerhtml: document.body.innerHTML 
      };
      window.history.pushState(stateObj, settings.url, settings.url);
   });*/

   window.onpopstate = function (event) {
      var currentState = window.history.state;
      document.body.innerHTML = currentState.innerhtml;
   };
   
function constrctSelectTag(maxRowCount, selectedIndex)
{
	selectedIndex+=1;
	var selectElementTag = '';
	var constrSlctElmntPrfx = 
						'<p>Records : '+
						'<select class="form-control input-sm" onchange=\"displayAllProjects(value)\">';
	var constrSlctElmntApendent= '';
	var constrSlctElmntSufx = 					  
						'</select>'+
						'</p>';
	var maxPotRowCount = ((maxRowCount/30)+1)*30;
	
	for(var curMin = 1, curMax = 30;curMax<maxPotRowCount; curMin = curMax+1, curMax = curMax+30)
	{
		if(curMin==selectedIndex)
			constrSlctElmntApendent+='<option value='+(curMin-1)+' selected>'+curMin+'-'+curMax+'</option>';
		else
			constrSlctElmntApendent+='<option value='+(curMin-1)+'>'+curMin+'-'+curMax+'</option>';
	}
	
	selectElementTag = constrSlctElmntPrfx+constrSlctElmntApendent+constrSlctElmntSufx;
	return selectElementTag;
}

function constrctDataTable(tableID, maxRowCount, selectedIndex)
{
	$('#'+tableID).dataTable({
	    paging: false,
	    "order": []
	});
	$('#'+tableID).DataTable();
	$('.dataTables_length').addClass('bs-select');
	
	//Constructing Wrappers
    var wrapperElmntCntnts = $('.dataTables_wrapper .row');
    var col1 = wrapperElmntCntnts.children()[0];
    col1.innerHTML = constrctSelectTag(maxRowCount, +selectedIndex);	
}
$(document).ready(function() 
{
	constrctDataTable('idproject-list-table', $('#tot-prjct-rec-count').val(), 0);
});

//Display Home using Ajax
function displayAllProjects(initIndex)
{
	//window.history.replaceState(null, '', '/TMS_Web/Home');
	$.ajax
	({
		type:'POST',
		url:'/TMS_Web/Home/All/Projects',
		data: {
			initRecordIndex : initIndex,
		},
		success: function(successResponse)
		{
			$('#home-page-body-content-nav').html('');
			$('#home-page-body-content-elements').html(jQuery(successResponse).find('#home-page-body-content-elements').html());
			
			$('#idPPModals').html(jQuery(successResponse).find('#idPPModals').html());
			
			//Updating the URL
		    window.history.replaceState(null, '', '/TMS_Web/Home/All/Projects');
		    
		    //Adding Milestone and Task option in Dropdown
			var ul = document.getElementById('idContentAdditionDD');
			ul.innerHTML = "";
			var prjctLi = document.createElement("li");
			prjctLi.innerHTML = "<p data-toggle='modal' data-target='#addProjectModal'>Create Project</p>";
		    ul.appendChild(prjctLi);
		    
		    //Configuring Datatable
		    constrctDataTable('idproject-list-table', $('#tot-prjct-rec-count').val(), initIndex);
		    
		    initiateProjectTokensArray();
		},
		error: function(errorResponse)
		{
			alert(errorResponse);
		}
	});
}
//Ajax call to get Current User Contents
function displayUserContents(contentName, curUserName)
{
	switch(contentName)
	{
		//Ajax call to get Current User Projects
		case 'Projects':
		{
			$.ajax
			({
				url: '/TMS_Web/Home/'+curUserName+'/Projects',
				type: "POST",
				success: function(successResponse)
				{
					$('#home-page-body-content-nav').html('');
					$('#home-page-body-content-elements').html(jQuery(successResponse).find('#id-user-content-projects').html());
					
					//Updating the URL
				    window.history.replaceState(null, '', "/TMS_Web/Home/"+curUserName+"/Projects");
				},
				error: function(errorResponse)
				{
					alert(errorResponse);
				}
			});
			break;
		}
		//Ajax call to get Current User Milestones
		case 'Milestones':
		{
			$.ajax
			({
				url: '/TMS_Web/Home/'+curUserName+'/Milestones',
				type: "POST",
				success: function(successResponse)
				{
					$('#home-page-body-content-nav').html('');
					$('#home-page-body-content-elements').html(jQuery(successResponse).find('#id-user-content-mlstns').html());
					
					//Updating the URL
				    window.history.replaceState(null, '', "/TMS_Web/Home/"+curUserName+"/Milestones");
				},
				error: function(errorResponse)
				{
					alert(errorResponse);
				}
			});
			break;
		}
		//Ajax call to get Current User Tasks
		case 'Tasks':
		{
			$.ajax
			({
				url: '/TMS_Web/Home/'+curUserName+'/Tasks',
				type: "POST",
				success: function(successResponse)
				{
					$('#home-page-body-content-nav').html('');
					$('#home-page-body-content-elements').html(jQuery(successResponse).find('#id-user-content-tasks').html());
					
					//Updating the URL
				    window.history.replaceState(null, '', "/TMS_Web/Home/"+curUserName+"/Tasks");
				},
				error: function(errorResponse)
				{
					alert(errorResponse);
				}
			});
			break;
		}
	}
	
}
//Ajax Call to Backend to render the project Tasks
function renderProjectContents(contentName, projectName, projectID)
{
	var actualPrjctName = projectName;
	projectName = projectName.replaceAll(" ", "_S_");
	
	switch(contentName)
	{
		//Ajax call to get All Projects Tasks
		case 'Tasks':
		{
			$.ajax
			({
				type:'POST',
				url:'/TMS_Web/Home/Projects/'+projectName+'/'+projectID+'/Tasks',
				success: function(successResponse)
				{
					$('#home-page-body-content-nav').html(jQuery(successResponse).find('#home-page-body-content-nav').html());
					$('#home-page-body-content-elements').html(jQuery(successResponse).find('#home-page-body-content-elements-prjct-tasks').html());
					
					$('#idPPModals').html(jQuery(successResponse).find('#idPPModals').html());
					
					//Adding Task option in Dropdown
					$('#idContentAdditionDD').empty();
					var ul = document.getElementById('idContentAdditionDD');
					var taskLi = document.createElement("li");
					taskLi.innerHTML = "<p onclick=\"renderModals('Task', "+projectID+")\">Create Task</p>";
				    ul.appendChild(taskLi);
					
					  var stateObj = 
				      { 
				      	innerhtml: document.body.innerHTML 
				      };
      
					//Updating the URL
				    window.history.pushState(stateObj, '', "/TMS_Web/Home/Projects/"+actualPrjctName+"/Tasks");
				    
				    //Setting Nav
				    setNavActive('navTasks');
				},
				error: function(errorResponse)
				{
					console.log(errorResponse);
				}
			});
			break;
		}
		//Ajax call to get All Projects Milestones
		case 'Milestones':
		{
			$.ajax
			({
				type:'POST',
				url:'/TMS_Web/Home/Projects/'+projectName+'/'+projectID+'/Milestones',
				success: function(successResponse)
				{
					$('#home-page-body-content-nav').html(jQuery(successResponse).find('#home-page-body-content-nav').html());
					$('#home-page-body-content-elements').html(jQuery(successResponse).find('#home-page-body-content-elements-prjct-mlstns').html());
					
					$('#idPPModals').html(jQuery(successResponse).find('#idPPModals').html());
					
					//Adding Milestone option in Dropdown
					$('#idContentAdditionDD').empty();
					var ul = document.getElementById('idContentAdditionDD');
					var mlstnLi = document.createElement("li");
				    mlstnLi.innerHTML = "<p onclick=\"renderModals('Milestone', "+projectID+")\">Create Milestone</p>";
				    ul.appendChild(mlstnLi);
				    
				      var stateObj = 
				      { 
				      	innerhtml: document.body.innerHTML 
				      };
				      
					//Updating the URL
				    window.history.pushState(stateObj, '', "/TMS_Web/Home/Projects/"+actualPrjctName+"/Milestones");	
				    
				    //Setting Nav
				    setNavActive('navMilestones');
				},
				error: function(errorResponse)
				{
					console.log(errorResponse);
				}
			});
			break;
		}
		//Ajax call to get All Projects Docs
		case 'Documents':
		{
			$.ajax
			({
				type:'POST',
				url:'/TMS_Web/Home/Projects/'+projectName+'/'+projectID+'/Documents',
				success: function(successResponse)
				{
					$('#home-page-body-content-nav').html(jQuery(successResponse).find('#home-page-body-content-nav').html());
					$('#home-page-body-content-elements').html(jQuery(successResponse).find('#home-page-body-content-elements-prjct-docs').html());
					
					$('#idContentAdditionDD').empty();
					//Updating the URL
				    window.history.replaceState(null, '', "/TMS_Web/Home/Projects/"+actualPrjctName+"/Documents");	
				    //Setting Nav
				    setNavActive('navDocs');
				},
				error: function(errorResponse)
				{
					console.log(errorResponse);
				}
			});
			break;
		}
		//Ajax call to get All Projects Charts
		case 'Charts':
		{
			setNavActive('navCharts');
			$('#idContentAdditionDD').empty();
			break;
		}
		//Ajax call to get All Projects Details
		case 'Details':
		{
			$.ajax
			({
				type:'POST',
				url:'/TMS_Web/Home/Projects/'+projectName+'/'+projectID+'/Details',
				success: function(successResponse)
				{
					$('#home-page-body-content-nav').html(jQuery(successResponse).find('#home-page-body-content-nav').html());
					$('#home-page-body-content-elements').html(jQuery(successResponse).find('#mainModal').html());
					
					//Updating the URL
				    window.history.replaceState(null, '', "/TMS_Web/Home/Projects/"+actualPrjctName+"/Details");	
				    //Setting Nav
				    setNavActive('navDetails');
					$('#idContentAdditionDD').empty();
					
					$('#addProjectModal').modal();
					
					initiateProjectTokensArray();
				},
				error: function(errorResponse)
				{
					console.log(errorResponse);
				}
			});
			
			break;
		}
	}
}
//Rendering Milestone Modal
function renderModals(unitContent, projectID)
{
	switch(unitContent)
	{
		case 'Task':
		{
			$.ajax
			({
				type:'POST',
				url:'/TMS_Web/Home/Projects/'+projectID+'/RenderTaskModal',
				success: function(successResponse)
				{
					//Clearing Previously Appended Files from StoredFiles Variable
					clearStoredFiles();

					$('#idPPModals').html(jQuery(successResponse).find('#mainModal').html());
					$('#addTaskModal').modal();
					
					//For initiating User Liat into Token List for Tasks
					initiateTaskTokensArray();
				},
				error: function(errorResponse)
				{
					console.log(errorResponse);
				}
			});
			break;
		}
		case 'Milestone':
		{
			$.ajax
			({
				type:'POST',
				url:'/TMS_Web/Home/Projects/'+projectID+'/RenderMlstnModal',
				success: function(successResponse)
				{
					//Clearing Previously Appended Files from StoredFiles Variable
					clearStoredFiles();
					
					$('#idPPModals').html(jQuery(successResponse).find('#mainModal').html());
					$('#addMilestoneModal').modal();
				},
				error: function(errorResponse)
				{
					console.log(errorResponse);
				}
			});
			break;
		}
	}
}
function renderTaskContents(moduleName, projectName, taskName, taskID)
{
	var projectID = $('#idCurrentProjectID').val();
	switch(moduleName)
	{
		//Ajax call to get All Attachments of tasks
		case 'Documents':
		{
			$.ajax
			({
				type:'POST',
				url:'/TMS_Web/Home/TaskDetailing/'+projectID+'/'+taskID+'/Documents',
				data:
				{
					selectedProject :  projectName,
					selectedTask : taskName
				},
				success: function(successResponse)
				{
					$('#home-page-body-content-nav').html(jQuery(successResponse).find('#home-page-body-content-task-sub-nav').html());
					$('#home-page-body-content-elements').html(jQuery(successResponse).find('#home-page-body-content-elements-prjct-docs').html());
					
					$('#idContentAdditionDD').empty();
					//Updating the URL
				    window.history.replaceState(null, '', "/TMS_Web/Home/"+projectName+"/"+taskName+"/Documents");	
				    //Setting Nav
				    setNavActive('navDocs');
				},
				error: function(errorResponse)
				{
					console.log(errorResponse);
				}
			});
			break;	
		}
		//Ajax call to get All Details of tasks
		case 'Details':
		{
			$.ajax
			({
				type:'POST',
				url:'/TMS_Web/Home/Tasks/'+projectID+'/'+taskID+'/Details',
				success: function(successResponse)
				{
					$('#home-page-body-content-nav').html(jQuery(successResponse).find('#home-page-body-content-task-sub-nav').html());
					$('#idPPModals').html(jQuery(successResponse).find('#mainModal').html());
					
					$('#addTaskModal').modal();
					
					$('#idContentAdditionDD').empty();
					//Updating the URL
				    window.history.replaceState(null, '', "/TMS_Web/Home/"+projectName+"/"+taskName+"/Details");	
				    //Setting Nav
				    setNavActive('navDetails');
				    
				    //For initiating User Liat into Token List for Tasks
					initiateTaskTokensArray();
				},
				error: function(errorResponse)
				{
					console.log(errorResponse);
				}
			});
			break;	
		}
	}
}
function renderMlstnContents(moduleName, projectName, milestoneName, mlstnID)
{
	var projectID = $('#idCurrentProjectID').val();
	switch(moduleName)
	{
		//Ajax call to get All Attachments of Milestones
		case 'Documents':
		{
			$.ajax
			({
				type:'POST',
				data:
				{
					selectedProject :  projectName,
					selectedMilestone : milestoneName
				},
				url:'/TMS_Web/Home/MlstnDetailing/'+projectID+'/'+mlstnID+'/Documents',
				success: function(successResponse)
				{
					$('#home-page-body-content-nav').html(jQuery(successResponse).find('#home-page-body-content-mlstn-sub-nav').html());
					$('#home-page-body-content-elements').html(jQuery(successResponse).find('#home-page-body-content-elements-prjct-docs').html());
					
					$('#idContentAdditionDD').empty();
					//Updating the URL
				    window.history.replaceState(null, '', "/TMS_Web/Home/"+projectName+"/"+milestoneName+"/Documents");	
				    //Setting Nav
				    setNavActive('navDocs');
				},
				error: function(errorResponse)
				{
					console.log(errorResponse);
				}
			});
			break;	
		}
		//Ajax call to get All Details of Milestones
		case 'Details':
		{
			$.ajax
			({
				type:'POST',
				url:'/TMS_Web/Home/Milestones/'+projectID+'/'+mlstnID+'/Details',
				success: function(successResponse)
				{
					$('#home-page-body-content-nav').html(jQuery(successResponse).find('#home-page-body-content-mlstn-sub-nav').html());
					$('#idPPModals').html(jQuery(successResponse).find('#mainModal').html());
					
					$('#addMilestoneModal').modal();
					
					$('#idContentAdditionDD').empty();
					//Updating the URL
				    window.history.replaceState(null, '', "/TMS_Web/Home/"+projectName+"/"+milestoneName+"/Details");	
				    //Setting Nav
				    setNavActive('navDetails');
				},
				error: function(errorResponse)
				{
					console.log(errorResponse);
				}
			});
			break;	
		}
	}
}

//Settting Nav Active for Selected nav
function setNavActive(navId)
{
	$('#'+navId).addClass('active');
}
//AJAX JSON new JS implementaions
function addNewProject()
{
	if(validateModalInputs('Project'))
	{
		var projectNewFromDateArray = $('#idPrjctFrmDate').val().split('/');
		var projectNewFromDateStr = projectNewFromDateArray[2]+'-'+projectNewFromDateArray[0]+'-'+projectNewFromDateArray[1];
		var projectNewToDateArray = $('#idPrjctToDate').val().split('/');
		var projectNewToDateStr = projectNewToDateArray[2]+'-'+projectNewToDateArray[0]+'-'+projectNewToDateArray[1];
		var frmData = new FormData();
		//Adding Multiple Files and Their Names
		for(var curFileIndex=0; curFileIndex<storedFiles.length; curFileIndex++)
		{
			var curFile = storedFiles[curFileIndex];
			frmData.append('crtNewProjectFiles', curFile);
			var curFileName = storedFileNames[curFileIndex];
			frmData.append('crtNewProjectFilesName', curFileName);
		}
		frmData.append('crtNewProjectName', $('#idPrjctName').val());
		frmData.append('crtNewProjectDescription', $('#idPrjctDescript').val());
		frmData.append('crtNewProjectOwner', $('#idPrjctOwner').val());
		frmData.append('crtNewProjectMembers', $('#idPrjctMembers').val());
		frmData.append('crtNewProjectFrmDate', projectNewFromDateStr);
		frmData.append('crtNewProjectToDate', projectNewToDateStr);
		frmData.append('crtNewProjectPriority', crtNewProjectPriority);
		frmData.append('crtNewProjectStatus', crtNewProjectStatus);
		frmData.append('crtNewProjectTags', $('#idPrjctTag').val());
		$.ajax
		({	
			//New Ajax call for rendering all projects
			
			url:'/TMS_Web/addNewProject',
			type:'POST',
			processData: false,
			contentType: false,
			traditional:true,
			data:
				frmData,
			success: function(successResponse)
			{
				$('#home-page-body-content-elements').html(jQuery(successResponse).find('#home-page-body-content-elements').html());
				
				constrctDataTable('idproject-list-table', $('#tot-prjct-rec-count').val(), 0);
				
				$('#addProjectModal').modal('toggle');
			},
			error: function(errorResponse)
			{
				alert(errorResponse);
			}
					
		 });
	}
}
function editProject(curProjectID)
{
	if(validateModalInputs('Project'))
	{
		//Updating Priority and Status if User haven't Updated
		var selPriorty = $('#idPrjctPriority').text();
		var prjctPriority = (selPriorty.includes('Low'))?1:(selPriorty.includes('Medium'))?2:3;
	    setProjectPriority(prjctPriority);
	    
	    var selStatus = $('#idPrjctStatus').text();
		var prjctStatus = (selStatus.includes('Open'))?1:(selStatus.includes('Development'))?2:(selStatus.includes('Testing'))?3:4;
	    setProjectStatus(prjctStatus);
		
		var projectNewFromDateArray = $('#idPrjctFrmDate').val().split('/');
		var projectNewFromDateStr = projectNewFromDateArray[2]+'-'+projectNewFromDateArray[0]+'-'+projectNewFromDateArray[1];
		var projectNewToDateArray = $('#idPrjctToDate').val().split('/');
		var projectNewToDateStr = projectNewToDateArray[2]+'-'+projectNewToDateArray[0]+'-'+projectNewToDateArray[1];
		var frmData = new FormData();
		//Adding Multiple Files and Their Names
		for(var curFileIndex=0; curFileIndex<storedFiles.length; curFileIndex++)
		{
			var curFile = storedFiles[curFileIndex];
			frmData.append('crtNewProjectFiles', curFile);
			var curFileName = storedFileNames[curFileIndex];
			frmData.append('crtNewProjectFilesName', curFileName);
		}
		frmData.append('crtNewProjectName', $('#idPrjctName').val());
		frmData.append('crtNewProjectDescription', $('#idPrjctDescript').val());
		frmData.append('crtNewProjectOwner', $('#idPrjctOwner').val());
		frmData.append('crtNewProjectMembers', $('#idPrjctMembers').val());
		frmData.append('crtNewProjectFrmDate', projectNewFromDateStr);
		frmData.append('crtNewProjectToDate', projectNewToDateStr);
		frmData.append('crtNewProjectPriority', crtNewProjectPriority);
		frmData.append('crtNewProjectStatus', crtNewProjectStatus);
		frmData.append('crtNewProjectTags', $('#idPrjctTag').val());
		$.ajax
		({	
			//New Ajax call for rendering all projects
			
			url:'/TMS_Web/Home/Projects/'+curProjectID+'/EditProject',
			type:'POST',
			processData: false,
			contentType: false,
			traditional:true,
			data:
				frmData,
			success: function(successResponse)
			{
				displayAllProjects(0);
				$('#addProjectModal').modal('toggle');
			},
			error: function(errorResponse)
			{
				alert(errorResponse);
			}
					
		 });
	 }
}
//Edit Milestone and Saving Changes
function editMilestone(curMilestoneID)
{
	if(validateModalInputs('Milestone'))
	{
		//Updating Priority and Status if User haven't Updated
		var selPriorty = $('#idMlstnPriority').text();
		var mlstnPriority = (selPriorty.includes('Low'))?1:(selPriorty.includes('Medium'))?2:3;
	    setMlstnPriority(mlstnPriority);
	    
	    var selStatus = $('#idMlstnStatus').text();
		var mlstnStatus = (selStatus.includes('Open'))?1:(selStatus.includes('Development'))?2:(selStatus.includes('Testing '))?3:4;
	    setMlstnStatus(mlstnStatus);
	    
	    
		var curProjectName = $('#idCurrentProjectName').val();
		
		var curProjectID = $('#idCurrentProjectID').val();
		
		var checkedTasksID = $('input[name="checkTask"]:checked').map(function() {
							    return $(this).attr('id');
							  }).get();
							  
		var mlstnNewFrmDateArray = $('#idMlstnFrmDate').val().split('/');
		var mlstnFrmDateStr = mlstnNewFrmDateArray[2]+'-'+mlstnNewFrmDateArray[0]+'-'+mlstnNewFrmDateArray[1];
		var mlstnNewToDateArray = $('#idMlstnToDate').val().split('/');
		var mlstnNewToDateStr = mlstnNewToDateArray[2]+'-'+mlstnNewToDateArray[0]+'-'+mlstnNewToDateArray[1];
		var frmData = new FormData();
		//Adding Multiple Files and Their Names
		for(var curFileIndex=0; curFileIndex<storedFiles.length; curFileIndex++)
		{
			var curFile = storedFiles[curFileIndex];
			frmData.append('crtNewMlstnFiles', curFile);
			var curFileName = storedFileNames[curFileIndex];
			frmData.append('crtNewMlstnFilesName', curFileName);
		}
		frmData.append('crtNewMlstnName', $('#idMlstnName').val());
		frmData.append('crtNewMlstnProjectName', $('#idCurrentProjectName').val());
		frmData.append('crtNewMlstnProjectID', $('#idCurrentProjectID').val());
		frmData.append('crtNewMlstnDescription', $('#idMlstnDescript').val());
		frmData.append('crtNewMlstnOwner', $('#idMlstnOwner').val());
		frmData.append('crtNewMlstnTasksID', checkedTasksID);
		frmData.append('crtNewMlstnFrmDate', mlstnFrmDateStr);
		frmData.append('crtNewMlstnToDate', mlstnNewToDateStr);
		frmData.append('crtNewMlstnPriority', crtNewMlstnPriority);
		frmData.append('crtNewMlstnStatus', crtNewMlstnStatus);
		frmData.append('crtNewMlstnTags', $('#idMlstnTag').val());
		
		$.ajax
		({	
			url:'/TMS_Web/Home/Projects/'+curProjectID+'/'+curMilestoneID+'/EditMilestone',
			type:'POST',
			processData: false,
			contentType: false,
			traditional:true,
			data:
				frmData,
			success: function(successResponse)
			{
				renderProjectContents('Milestones', curProjectName , curProjectID);
				$('#addMilestoneModal').modal('toggle');
			},
			error: function(errorResponse)
			{
				alert(errorResponse);
			}
					
		 });
	}
}

//Edit Task and Saving Changes
function editTask(curTaskID)
{
	if(validateModalInputs('Task'))
	{
		//Updating Priority and Status if User haven't Updated
		var selPriorty = $('#idTaskPriority').text();
		var taskPriority = (selPriorty.includes('Low'))?1:(selPriorty.includes('Medium'))?2:3;
	    setTaskPriority(taskPriority);
	    
	    var selStatus = $('#idTaskStatus').text();
		var taskStatus = (selStatus.includes('Open'))?1:(selStatus.includes('Development'))?2:(selStatus.includes('Testing '))?3:4;
	    setTaskStatus(taskStatus);
	    
	    
		var curProjectName = $('#idCurrentProjectName').val();
		
		var curProjectID = $('#idCurrentProjectID').val();
		
		var choosenMlstn = $('input[name="chosenMlstn"]:checked').attr('id');
				
		//Throwing Error in Struts XML so Declaring it as 0 if nothing is selected
		if(choosenMlstn==undefined)
		{
			choosenMlstn=0;
		}
					  
		var taskNewFrmDateArray = $('#idTaskFrmDate').val().split('/');
		var idTaskFrmDateStr = taskNewFrmDateArray[2]+'-'+taskNewFrmDateArray[0]+'-'+taskNewFrmDateArray[1];
		var taskNewToDateArray = $('#idTaskToDate').val().split('/');
		var taskNewToDateStr = taskNewToDateArray[2]+'-'+taskNewToDateArray[0]+'-'+taskNewToDateArray[1];
		var frmData = new FormData();
		//Adding Multiple Files and Their Names
		for(var curFileIndex=0; curFileIndex<storedFiles.length; curFileIndex++)
		{
			var curFile = storedFiles[curFileIndex];
			frmData.append('crtNewTaskFiles', curFile);
			var curFileName = storedFileNames[curFileIndex];
			frmData.append('crtNewTaskFilesName', curFileName);
		}
		frmData.append('crtNewTaskProjectName', $('#idCurrentProjectName').val());
		frmData.append('crtNewTaskProjectID', $('#idCurrentProjectID').val());
		frmData.append('crtNewTaskMlstnID', choosenMlstn);
		frmData.append('crtNewTaskName', $('#idTaskName').val());
		frmData.append('crtNewTaskDescription', $('#idTaskDescript').val());
		frmData.append('crtNewTaskOwner', $('#idTaskOwner').val());
		frmData.append('crtNewTaskAssignee', $('#idTaskAssignee').val());
		frmData.append('crtNewTaskFrmDate', idTaskFrmDateStr);
		frmData.append('crtNewTaskToDate', taskNewToDateStr);
		frmData.append('crtNewTaskPriority', crtNewTaskPriority);
		frmData.append('crtNewTaskStatus', crtNewTaskStatus);
		frmData.append('crtNewTaskTags', $('#idTaskTag').val());
		
		$.ajax
		({	
			url:'/TMS_Web/Home/Projects/'+curProjectID+'/'+curTaskID+'/EditTask',
			type:'POST',
			processData: false,
			contentType: false,
			traditional:true,
			data:
				frmData,
			success: function(successResponse)
			{
				renderProjectContents('Tasks', curProjectName , curProjectID);
				$('#addTaskModal').modal('toggle');
			},
			error: function(errorResponse)
			{
				alert(errorResponse);
			}	
		 });
	 }
}
//Adding New Task 
function addNewTask()
{
	if(validateModalInputs('Task'))
	{
		var curProjectName = $('#idCurrentProjectName').val();
		curProjectName = curProjectName.replaceAll(" ", "_S_");
		
		var curProjectID = $('#idCurrentProjectID').val();
		var choosenMlstn = $('input[name="chosenMlstn"]:checked').val();
		
		//Throwing Error in Struts XML so Declaring it as 0 if nothing is selected
		if(choosenMlstn==undefined)
		{
			choosenMlstn=0;
		}
		
		var taskNewFrmDateArray = $('#idTaskFrmDate').val().split('/');
		var idTaskFrmDateStr = taskNewFrmDateArray[2]+'-'+taskNewFrmDateArray[0]+'-'+taskNewFrmDateArray[1];
		var taskNewToDateArray = $('#idTaskToDate').val().split('/');
		var taskNewToDateStr = taskNewToDateArray[2]+'-'+taskNewToDateArray[0]+'-'+taskNewToDateArray[1];
		var frmData = new FormData();
		//Adding Multiple Files and Their Names
		for(var curFileIndex=0; curFileIndex<storedFiles.length; curFileIndex++)
		{
			var curFile = storedFiles[curFileIndex];
			frmData.append('crtNewTaskFiles', curFile);
			var curFileName = storedFileNames[curFileIndex];
			frmData.append('crtNewTaskFilesName', curFileName);
		}
		frmData.append('crtNewTaskProjectName', $('#idCurrentProjectName').val());
		frmData.append('crtNewTaskProjectID', $('#idCurrentProjectID').val());
		frmData.append('crtNewTaskMlstnID', choosenMlstn);
		frmData.append('crtNewTaskName', $('#idTaskName').val());
		frmData.append('crtNewTaskDescription', $('#idTaskDescript').val());
		frmData.append('crtNewTaskOwner', $('#idTaskOwner').val());
		frmData.append('crtNewTaskAssignee', $('#idTaskAssignee').val());
		frmData.append('crtNewTaskFrmDate', idTaskFrmDateStr);
		frmData.append('crtNewTaskToDate', taskNewToDateStr);
		frmData.append('crtNewTaskPriority', crtNewTaskPriority);
		frmData.append('crtNewTaskStatus', crtNewTaskStatus);
		frmData.append('crtNewTaskTags', $('#idTaskTag').val());
		
		$.ajax
		({
			type:'POST',
			url: '/TMS_Web/Home/Projects/'+curProjectName+'/'+curProjectID+'/addNewTask',
			processData: false,
			contentType: false,
			traditional:true,
			data:
				frmData,
			success: function(successResponse)
			{
				$('#home-page-body-content-elements').html(jQuery(successResponse).find('#home-page-body-content-elements-prjct-tasks').html());
				$('#addTaskModal').modal('toggle');
			},
			error: function(errorResponse)
			{
				console.log(errorResponse);
			}
		});
	}
}

//Adding New Milestone
function addNewMilestone()
{
	if(validateModalInputs('Milestone'))
	{
		var curProjectName = $('#idCurrentProjectName').val();
		curProjectName = curProjectName.replaceAll(" ", "_S_");
		
		var curProjectID = $('#idCurrentProjectID').val();
		
		var checkedTasksID = $('input[name="checkTask"]:checked').map(function() {
							    return $(this).attr('id');
							  }).get();
							  
		var mlstnNewFrmDateArray = $('#idMlstnFrmDate').val().split('/');
		var mlstnFrmDateStr = mlstnNewFrmDateArray[2]+'-'+mlstnNewFrmDateArray[0]+'-'+mlstnNewFrmDateArray[1];
		var mlstnNewToDateArray = $('#idMlstnToDate').val().split('/');
		var mlstnNewToDateStr = mlstnNewToDateArray[2]+'-'+mlstnNewToDateArray[0]+'-'+mlstnNewToDateArray[1];
		var frmData = new FormData();
		//Adding Multiple Files and Their Names
		for(var curFileIndex=0; curFileIndex<storedFiles.length; curFileIndex++)
		{
			var curFile = storedFiles[curFileIndex];
			frmData.append('crtNewMlstnFiles', curFile);
			var curFileName = storedFileNames[curFileIndex];
			frmData.append('crtNewMlstnFilesName', curFileName);
		}
		frmData.append('crtNewMlstnName', $('#idMlstnName').val());
		frmData.append('crtNewMlstnProjectName', $('#idCurrentProjectName').val());
		frmData.append('crtNewMlstnProjectID', $('#idCurrentProjectID').val());
		frmData.append('crtNewMlstnDescription', $('#idMlstnDescript').val());
		frmData.append('crtNewMlstnOwner', $('#idMlstnOwner').val());
		frmData.append('crtNewMlstnTasksID', checkedTasksID);
		frmData.append('crtNewMlstnFrmDate', mlstnFrmDateStr);
		frmData.append('crtNewMlstnToDate', mlstnNewToDateStr);
		frmData.append('crtNewMlstnPriority', crtNewMlstnPriority);
		frmData.append('crtNewMlstnStatus', crtNewMlstnStatus);
		frmData.append('crtNewMlstnTags', $('#idMlstnTag').val());
		
		$.ajax
		({
			type:'POST',
			url: '/TMS_Web/Home/Projects/'+curProjectName+'/'+curProjectID+'/addNewMilestone',
			processData: false,
			contentType: false,
			traditional:true,
			data:
				frmData,
			success: function(successResponse)
			{
				$('#home-page-body-content-elements').html(jQuery(successResponse).find('#home-page-body-content-elements-prjct-mlstns').html());
				$('#addMilestoneModal').modal('toggle');
			},
			error: function(errorResponse)
			{
				alert(errorResponse);
			}
		});
	}
}

//JQuery Event for handling table expansion
function displayMilestoneSubTasks(mlstnID) 
{
	if($('#mlstnSubTbl'+mlstnID).length==0)
	{
		var constructedTable = "\t<tr id=\"mlstnSubTbl"+mlstnID+"\" class=\"mlstnSubTaskCls table row-child collapse in\" aria-expanded=\"true\">\n" +
                    "\t\t<td>"+
                    "\t\t<table class=\"table align-middle mb-0 bg-white subTable-list-table\" id=\"idSubTable-list-table\">\n" +
                    "\t\t                <thead class=\"bg-light\">\n" +
                    "\t\t                  <tr>\n" +
                    "\t\t\t\t\t            <th>Task Name</th>\n" +
                    "\t\t\t\t\t            <th>Owner</th>\n" +
                    "\t\t\t\t\t            <th>Start Date</th>\n" +
                    "\t\t\t\t\t            <th>Due Date</th>\n" +
                    "\t\t\t\t\t            <th>Assignee</th>\n" +
                    "\t\t\t\t\t            <th>Priority</th>\n" +
                    "\t\t\t\t\t            <th>Status</th>\n" +
                    "\t\t\t\t\t            </tr>\n" +
                    "\t\t                </thead>\n" +
                    "\t\t                <tbody>\n";
		//Ajax call for getting table data
		$.ajax
		({
			url: '/TMS_Web/Home/MilestoneTasks/'+mlstnID,	
			type: 'POST',
			dataType: 'JSON', 
			async : true,
			success: function(successResponse)
			{
				var jsonObj = successResponse.TaskArray;
				if(jsonObj.length>0)
				{
					var constrRows = "";
					for(var curIndex=0; curIndex<jsonObj.length; curIndex++)
					{
						var curObj = jsonObj[curIndex];
						var curRow = 
						"\t\t                <tr draggable=\"true\">\n" +
	                    "\t\t                <td>"+curObj.TaskName+"</td>\n" +
	                    "\t\t                <td>"+curObj.Owner+"</td>\n" +
	                    "\t\t                <td>"+curObj.StartDate+"</td>\n" +
	                    "\t\t                <td>"+curObj.DueDate+"</td>\n" +
	                    "\t\t                <td>"+curObj.Assignee+"</td>\n" +
	                    "\t\t                <td>\n" +
	                    "\t\t                <span class='badge badge-"+curObj.Priority+" rounded-pill d-inline' >"+curObj.Priority+"</span>\n" +
	                    "\t\t                </td>\n" +
	                    "\t\t                <td>\n" +
	                    "\t\t                <span class='badge badge-"+curObj.Status+" rounded-pill d-inline' >"+curObj.Status+"</span>\n" +
	                    "\t\t                </td>\n" +
	                    "\t\t                </tr>\n";
						constrRows += curRow; 
					}
					constructedTable += constrRows;
					constructedTable += "\t\t   \t\t\t\t</tbody>\n" +
	                    "\t\t\t\t \t</table>"+
	                    "\t\t</td>"+
	                    "\t</tr>";
					$(constructedTable).insertAfter($('#idMlstn'+mlstnID).closest('tr'));
					}
			}, 
			error: function(errorResponse)
			{
				console.log(errorResponse);
			}
		});
	}
}