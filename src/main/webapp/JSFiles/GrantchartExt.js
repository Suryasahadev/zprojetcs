function createChart() 
{
const days = document.querySelectorAll(".chart-values li");
const tasks = document.querySelectorAll(".chart-bars li");
const daysArray = [...days];

    tasks.forEach(el => 
    {
        const duration = el.dataset.duration.split("-");
        const startDay = duration[0];
        const endDay = duration[1];
        let left = 0, width = 0;

        const filteredStartArray = daysArray.filter(day => day.textContent == startDay);
        left = filteredStartArray[0].offsetLeft;

        const filteredEndArray = daysArray.filter(day => day.textContent == endDay);
        width = filteredEndArray[0].offsetLeft + filteredEndArray[0].offsetWidth - left;
        
        // apply css
        el.style.left = `${left}px`;
        el.style.width = `${width}px`;
        el.style.backgroundColor = el.dataset.color;
        el.style.opacity = 1;
    });
}
window.addEventListener("resize", createChart);