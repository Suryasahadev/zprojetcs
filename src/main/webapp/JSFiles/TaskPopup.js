// !-- For Filtering Date Operations
	$(".frmDateField").datepicker({
        todayHighlight: true,
        autoclose: true,
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('.toDateField').datepicker('setStartDate', minDate);
    });

    $(".toDateField").datepicker({
		todayHighlight: true,
	    autoclose: true
	})
    .on('changeDate', function (selected) {
        var maxDate = new Date(selected.date.valueOf());
        $('.frmDateField').datepicker('setEndDate', maxDate);
    });
// For Filtering Date Operations --!

$(".taskPrtyUL li a").click(function()
{
    var selText = $(this).text();
    var taskPriority = (selText=='Low')?1:(selText=='Medium')?2:3;
    setTaskPriority(taskPriority);
    $(this).parents('.dropdown').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
});
$(".taskStatUL li a").click(function()
{
    var selText = $(this).text();
    var taskStatus = (selText=='Open')?1:(selText=='Development')?2:(selText=='Testing')?3:4;
    setTaskStatus(taskStatus);
    $(this).parents('.dropdown').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
});

//For Updating File Appending Function
$(function()
{
	document.querySelector('#idTaskFileAttachments').addEventListener('change', appdendSelectedFiles, false);
});