// !-- For Filtering Date Operations
	$(".frmDateField").datepicker({
        todayHighlight: true,
        autoclose: true,
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('.toDateField').datepicker('setStartDate', minDate);
    });

    $(".toDateField").datepicker({
		todayHighlight: true,
	    autoclose: true
	})
    .on('changeDate', function (selected) {
        var maxDate = new Date(selected.date.valueOf());
        $('.frmDateField').datepicker('setEndDate', maxDate);
    });
// For Filtering Date Operations --!
$(document).ready(function()
{
    $('.cust-form-token').tokenfield({
        showAutocompleteOnFocus: true
    });
});
$(".mlstnPrtyUL li a").click(function()
{
    var selText = $(this).text();
    var prjctPriority = (selText=='Low')?1:(selText=='Medium')?2:3;
    setMlstnPriority(prjctPriority);
    $(this).parents('.dropdown').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
});
$(".mlstnStatUL li a").click(function()
{
    var selText = $(this).text();
    var prjctStatus = (selText=='Open')?1:(selText=='Development')?2:(selText=='Testing')?3:4;
    setMlstnStatus(prjctStatus);
    $(this).parents('.dropdown').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
});

//For Updating File Appending Function
$(function()
{
	document.querySelector('#idMlstnFileAttachments').addEventListener('change', appdendSelectedFiles, false);
});