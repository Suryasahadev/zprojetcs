<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="/struts-tags" prefix="s" %>
<!-- Project Popup Begins -->
<s:form method="post" enctype="multipart/form-data">
<div id="mainModal">
    <div class="modal" tabindex="-1" role="dialog" id="projectModal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
	           	<h5 class="modal-title">Edit Project</h5>
        	</div>
        <div class="modal-body">
            <div class="form-group">
                <small class="form-text text-muted">Project Name</small>
                <input type="text" class="form-control unitNameField" id="idPrjctName" value="<s:property value="detailingPrjctEntityObj.getProjectName()"/>">
            	<p class="invalidUnitNameErr" id="id-invalidUnitNameErr"></p>
            </div>
            <div class="form-group">
                <small class="form-text text-muted">Description</small>
                <textarea class="form-control" id="idPrjctDescript" rows="3"><s:property value="detailingPrjctEntityObj.getProjectDescription()"/></textarea>
            </div>
            <div class="form-group">
                <small class="form-text text-muted">Add Members</small>
                <input type="text" name="nmPrjctMembers" id="idPrjctMembers" class="form-control cust-form-token unitMemberField" value="<s:property value="detailingPrjctEntityObj.getStringifiedMembers()"/>"/>
            	<p class="invalidUnitMemberErr" id="id-invalidUnitMemberErr"></p>
            </div>
            
            <div class="row cust-modal-row"> 
                <div class="col-md-6">
                    <small class="form-text text-muted">From Date</small>
                    <div class="input-group date">
                        <input type="text" name="nmPrjctFrmDate" id="idPrjctFrmDate" class="form-control frmDateField unitFrmDateField" autocomplete="off" placeholder="From Date" value="<s:property value="detailingPrjctEntityObj.getDPFormattedStartTimestamp()"/>"><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <p class="invalidUnitFrmDtErr" id="id-invalidUnitFrmDtErr">* From Date Required</p>
                </div>
                <div class="col-md-6">
                    <small id="emailHelp" class="form-text text-muted">To Date</small>
                    <div class="input-group date">
                        <input type="text" name="nmPrjctToDate" id="idPrjctToDate" class="form-control toDateField unitToDateField" autocomplete="off" placeholder="To Date" value="<s:property value="detailingPrjctEntityObj.getDPFormattedDueTimestamp()"/>"><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <p class="invalidUnitToDtErr" id="id-invalidUnitToDtErr">* To Date Required</p>
                </div>
            </div>
            
            <div class="row"> 
                <div class="col-md-6">
                    <div class="dropdown form-group">
                        <small class="form-text text-muted">Priority</small>
                        <button class="btn btn-primary dropdown-toggle form-control unitPriority" type="button" id="idPrjctPriority" name="nmPrjctPriority" data-toggle="dropdown">
                         <s:if test="detailingPrjctEntityObj!=null">
                         	<s:property value="detailingPrjctEntityObj.getPriority()"/>
                         </s:if>
                         <s:else>
                         	Priority
                         </s:else>
                         <span class="caret"></span>
                        </button>
                        <ul class="prjctPrtyUL unitPriorityUL dropdown-menu">
                          <li><a >Low</a></li>
                          <li><a >Medium</a></li>
                          <li><a >High</a></li>
                        </ul>
                        <p class="invalidUnitPrtyErr" id="id-invalidUnitPrtyErr">* Priority Required</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dropdown form-group">
                        <small class="form-text text-muted">Status</small>
                        <button class="btn btn-primary dropdown-toggle form-control unitStatus" type="button" id="idPrjctStatus" name="nmPrjctStatus" data-toggle="dropdown">
	                        <s:if test="detailingPrjctEntityObj!=null">
	                        	<s:property value="detailingPrjctEntityObj.getStatus()"/>
	                        </s:if>
	                        <s:else>
	                        	Status
	                        </s:else>
                        	<span class="caret"></span>
                        </button>
                        <ul class="prjctStatUL unitStatusUL dropdown-menu">
                          <li><a >Open</a></li>
                          <li><a >Development</a></li>
                          <li><a >Testing</a></li>
                          <li><a >Closed</a></li>
                        </ul>
                        <p class="invalidUnitStatErr" id="id-invalidUnitStatErr">* Status Required</p>
                    </div>
                </div>
            </div>

            <small class="form-text text-muted">Attach Files</small>
            <div class="mb-3">
                <s:file class="form-control fileAttachments" id="idPrjctFileAttachments" name="crtNewProjectFiles" multiple="multiple"></s:file>
            </div>

            <div id="appendedFileListGrp" style="display: none;">
                <div class="form-group mlstnTaskList">
                    <div class="form-check" id="appendedFileListViewer">
                        
                    </div>
                </div>
                <div class="form-check file-lst-clr-sec">
                    <a onclick="clearAttchmntField('idPrjctFileAttachments')">Clear All</a>
                </div>
            </div>
        </div>
        <div class="modal-footer">
           <button type="button" class="btn btn-primary" onclick="editProjectDetails(<s:property value="detailingPrjctEntityObj.getProjectID()"/>)">Save changes</button>
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  </div>
</s:form>
<!-- Project Popup Ends -->
