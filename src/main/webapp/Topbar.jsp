<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="/struts-tags" prefix="s"%>
<div class="col-md-12 home-nav-col">
    <nav class="navbar navbar-fixed home-nav-navbar">
        <div class="container-fluid home-nav-cont">
            <ul class="nav navbar-nav home-nav-cont-ul">
                <li class="home-nav-cont-li">
                    <strong><s:property value="#session.Username"></s:property></strong>
                </li>
            </ul>
            <button type="button" class="btn btn-primary user-btn" onclick="logoutUser()">
            	<i class="fas fa-sign-out-alt"></i>
                <%-- <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16"> 
                    <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                </svg> --%>
            </button>
        </div>
        <!--Cannot include inside previous div as the alignment changes-->
        <div class="container-fluid home-nav-cont dropdown">
            <button class="btn btn-primary rounded-circle dropdown-toggle" id="idContentAdditionButton" type="button" data-toggle="dropdown"><i class="fa-solid fa-plus"></i></button>
            <ul class="dropdown-menu" id="idContentAdditionDD">
                <li><p onclick="openUnitModal('Project')">Create Project</p></li>
            </ul>
        </div>
    </nav>
</div>