<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@include file="AppCDNReqs.html"%>
<title>Sign up</title>
<script src="JSFiles/Signup.js"></script>
</head>
	<body>
		<div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-login">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <a class="active" id="register-form-link">Signup</a>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                	<form>
	                                    <div class="form-group">
	                                        <input type="text" id="idSignUserName" name="signUserName" placeholder="Username" class="form-control" required/>
	                                    </div>
	                                    <p class="invalid-creds-para-resp" id="idUsernameErr"></p>
	      								<div class="form-group">
	                                        <input type="email" id="idSignUserMail" name="signMailID" placeholder="MailID" class="form-control" required/>
	                                    </div>
	                                    <p class="invalid-creds-para-resp" id="idUsermailErr"></p>
	                                    <div class="form-group">
	                                        <input type="password" id="idSignPassword" id="signPassword" name="signPassword" placeholder="Enter Password" class="form-control" required/>
	                                    </div>
	                                    <p class="invalid-creds-para-resp" id="passConstraintPara"></p>
	                                    <div class="form-group">
	                                        <input type="password" id="idSignRePassword" name="signRePassword" placeholder="Re-Enter Password" class="form-control" required/>
	                                    </div>
	                                    <p class="invalid-creds-para-resp" id="rePassConstraintPara"></p>
	                                    <div class="form-group">
	                                        <div class="row">
	                                            <div class="col-sm-6 col-sm-offset-3">
	                                                <input type="button" onclick="validateSignIn()" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Signup Now">
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="text-center">
	                                        <p class="forgot-password">Already having an account Login <span><a href="Login" tabindex="5">here</a></span></p>
	                                    </div>
                                    </form>
                                </div>
                            </div>
                            
                            <!-- OTP Validation Modal -->
                            <div class="modal fade otp-modal" id="otpValidationModal" tabindex="-1" role="dialog" aria-hidden="true">
				                <div class="modal-dialog modal-dialog-centered" role="document">
					                <div class="modal-content">
					                    <div class="modal-body">
				                            <div class="verf-title">
				                              <h3>OTP VERIFICATION</h3>
				                              <p class="verf-title-info"></p>
				                              <p class="verf-title-msg">Please enter OTP to verify</p>
				                            </div>
				                            <div class="otp-input-fields">
				                              <input type="number" class="otp__digit otp__field__1">
				                              <input type="number" class="otp__digit otp__field__2">
				                              <input type="number" class="otp__digit otp__field__3">
				                              <input type="number" class="otp__digit otp__field__4">
				                            </div>
				                            
				                            <div class="otp_result"><p class="otp_resp"></p></div>
				                            <div class="modal-footer">
				                                <a onclick="resendOTP()">Resend otp</a>
				                                <button class="btn btn-primary" onclick="validateOTP()">Verify</button>
				                            </div>
					                    </div>
					                </div>
				                </div>
				            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</body>
</html>