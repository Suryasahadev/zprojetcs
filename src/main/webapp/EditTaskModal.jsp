<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="/struts-tags" prefix="s" %>
<s:form method="post" enctype="multipart/form-data">
	<div id="mainModal">
	     <div class="modal" tabindex="-1" role="dialog" id="taskModal">
	       <div class="modal-dialog" role="document">
	         <div class="modal-content">
	           <div class="modal-header">
		           <h5 class="modal-title">Edit Task</h5>
	           </div>
	           <div class="modal-body">
	               <div class="form-group">
	                   <small class="form-text text-muted">Task Name</small>
	                   <input type="text" class="form-control unitNameField" id="idTaskName" value="<s:property value="detailingTaskEntityObj.getTaskName()"/>">
	               	   <p class="invalidUnitNameErr" id="id-invalidUnitNameErr"></p>	
	               </div>
	               <div class="form-group">
	                   <small class="form-text text-muted">Task Description</small>
	                   <textarea class="form-control" id="idTaskDescript" rows="3"><s:property value="detailingTaskEntityObj.getTaskDescription()"/></textarea>
	               </div>
	               <div class="form-group">
	                   <small class="form-text text-muted">Assign to</small>
	                   <input type="text" name="nmTaskAssignee" id="idTaskAssignee" class="form-control cust-form-token unitMemberField" value="<s:property value="detailingTaskEntityObj.getAssignee()"/>"/>
	               	   <p class="invalidUnitMemberErr" id="id-invalidUnitMemberErr"></p>
	               </div>
	               <div class="form-group">
	               		 <small class="form-text text-muted">Select Milestone</small>
		               	 <div class="form-group mlstnTaskList">
		               	 	<s:if test="taskMlstnMap!=null">
			               		<s:iterator value="taskMlstnMap">
			               			<s:set name="isChecked" value="%{value}"/>
			               			<s:if test="isChecked">
			               				<div class="form-check">
					                       <input type="radio" class="form-check-input" id="<s:property value="%{key.getMilestoneID()}"/>" value="<s:property value="%{key.getMilestoneID()}"/>" name="chosenMlstn" checked>
					                       <label class="form-check-label mlstnTaskListLbl" for="<s:property value="%{key.getMilestoneID()}"/>">
					                       		<s:property value="%{key.getMilestoneName()}"/>
					                       </label>
				                     	</div>
			               			</s:if>
			               			<s:else>
			               				<div class="form-check">
					                       <input type="radio" class="form-check-input" id="<s:property value="%{key.getMilestoneID()}"/>" value="<s:property value="%{key.getMilestoneID()}"/>" name="chosenMlstn">
					                       <label class="form-check-label mlstnTaskListLbl" for="<s:property value="%{key.getMilestoneID()}"/>">
					                       		<s:property value="%{key.getMilestoneName()}"/>
					                       </label>
										</div>
			               			</s:else>
			               		</s:iterator>
			               </s:if>
	                    </div>
	               </div>
	               
	               <div class="row cust-modal-row"> 
	                   <div class="col-md-6">
	                       <small class="form-text text-muted">From Date</small>
	                       <div class="input-group date">
	                           <input type="text" name="nmTaskFrmDate" id="idTaskFrmDate" class="form-control frmDateField unitFrmDateField" autocomplete="off" placeholder="From Date" value="<s:property value="detailingTaskEntityObj.getDPFormattedStartTimestamp()"/>"><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	                       </div>
	                       <p class="invalidUnitFrmDtErr" id="id-invalidUnitFrmDtErr">* From Date Required</p>
	                   </div>
	                   <div class="col-md-6">
	                       <small id="emailHelp" class="form-text text-muted">To Date</small>
	                       <div class="input-group date">
	                           <input type="text" name="nmTaskToDate" id="idTaskToDate" class="form-control toDateField unitToDateField" autocomplete="off" placeholder="To Date" value="<s:property value="detailingTaskEntityObj.getDPFormattedDueTimestamp()"/>"><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	                       </div>
	                       <p class="invalidUnitToDtErr" id="id-invalidUnitToDtErr">* To Date Required</p>
	                   </div>
	               </div>
	               
	               <div class="row"> 
	                   <div class="col-md-6">
	                       <div class="dropdown form-group">
	                           <small class="form-text text-muted">Priority</small>
	                           <button class="btn btn-primary dropdown-toggle form-control unitPriority" type="button" id="idTaskPriority" name="nmTaskPriority" data-toggle="dropdown">
		                           	 <s:if test="detailingTaskEntityObj!=null">
			                         	<s:property value="detailingTaskEntityObj.getTaskPriority()"/>
			                         </s:if>
			                         <s:else>
			                         	Priority
			                         </s:else>
	                           		 <span class="caret" ></span>
	                           </button>
	                           <ul class="taskPrtyUL unitPriorityUL dropdown-menu">
	                             <li><a >Low</a></li>
	                             <li><a >Medium</a></li>
	                             <li><a >High</a></li>
	                           </ul>
	                           <p class="invalidUnitPrtyErr" id="id-invalidUnitPrtyErr">* Priority Required</p>
	                       </div>
	                   </div>
	                   <div class="col-md-6">
	                       <div class="dropdown form-group">
	                           <small class="form-text text-muted">Status</small>
	                           <button class="btn btn-primary dropdown-toggle form-control unitStatus" type="button" id="idTaskStatus" name="nmTaskStatus" data-toggle="dropdown">
		                           <s:if test="detailingTaskEntityObj!=null">
			                        	<s:property value="detailingTaskEntityObj.getTaskStatus()"/>
			                        </s:if>
			                        <s:else>
			                        	Status
			                        </s:else>
		                           <span class="caret"></span>
	                           </button>
	                           <ul class="taskStatUL unitStatusUL dropdown-menu">
	                             <li><a >Open</a></li>
	                             <li><a >Development</a></li>
	                             <li><a >Testing</a></li>
	                             <li><a >Closed</a></li>
	                           </ul>
	                           <p class="invalidUnitStatErr" id="id-invalidUnitStatErr">* Status Required</p>
	                       </div>
	                   </div>
	               </div>
		               
	               <small class="form-text text-muted">Attach Files</small>
	               <div class="mb-3">
	                   <input class="form-control fileAttachments" type="file" id="idTaskFileAttachments" name="nmTaskFileAttachments" multiple>
	               </div>

				   <div id="appendedFileListGrp" style="display: none;">
						<div class="form-group mlstnTaskList">
							<div class="form-check" id="appendedFileListViewer">
								
							</div>
						</div>
						<div class="form-check file-lst-clr-sec">
							<a onclick="clearAttchmntField('idTaskFileAttachments')">Clear All</a>
						</div>
					</div>

	           </div>
	           <div class="modal-footer">
		           <button type="button" class="btn btn-primary" onclick="editTaskDetails(<s:property value="detailingTaskEntityObj.getTaskID()"/>)">Save changes</button>
		           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		       </div>
	         </div>
	       </div>
	     </div>
 	</div>
</s:form>