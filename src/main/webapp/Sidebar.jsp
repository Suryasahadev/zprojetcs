<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="/struts-tags" prefix="s"%>
<div class="col-md-2 home-page-dup-sidebar"></div>
<div class="col-md-2 home-page-sidebar">
    <div class="home-page-sidebar-header">
        <p class="h3">
            Z Projects
        </p>
    </div>
    <div class="home-page-sidebar-contents-div" id="sidebar">
        <ul class="list-unstyled components mb-5 sidebar-main-ul">
        	<li>
                <p href="#projectSec" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle sidebar-main-module-p"><i class="fas fa-project-diagram"></i> Projects</p>
                <ul class="list-unstyled collapse in sidebar-sub-module-ul" id="projectSec" aria-expanded="true">
                    <li>
		            	<p class="home-page-sidebar-content sidebar-sub-module-p module-active" onclick="renderUserContents('CreatedProjects')" id="idSidebar-module-CrtPrjcts"><i class="far fa-plus-square"></i>&nbsp; Created Projects</p>
		            </li>
		            <li>
		            	<p class="home-page-sidebar-content sidebar-sub-module-p" onclick="renderUserContents('AssignedProjects')" id="idSidebar-module-AsgndPrjcts"><i class="fas fa-tags"></i>&nbsp; Tagged Projects</p>
		            </li>
                </ul>
            </li>
            
            <li>
                <p href="#taskSec" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle sidebar-main-module-p"><i class="fa-solid fa-list-check"></i> Tasks</p>
                <ul class="list-unstyled collapse in sidebar-sub-module-ul" id="taskSec" aria-expanded="true">
                    <li>
		            	<p class="home-page-sidebar-content sidebar-sub-module-p" onclick="renderUserContents('CreatedTasks')" id="idSidebar-module-CrtTasks"><i class="far fa-plus-square"></i>&nbsp; Created Tasks</p>
		            </li>
		            <li>
		            	<p class="home-page-sidebar-content sidebar-sub-module-p" onclick="renderUserContents('AssignedTasks')" id="idSidebar-module-AsgndTasks"><i class="fas fa-tags"></i>&nbsp; Tagged Tasks</p>
		            </li>
                </ul>
            </li>
            
            
            <li>
            	<p class="home-page-sidebar-content sidebar-main-module-p" onclick="renderUserContents('CreatedMilestones')" id="idSidebar-module-CrtMlstns"><i class="far fa-flag"></i> Created Milestones</p>
            </li>
            <li>
            	<p class="home-page-sidebar-content sidebar-main-module-p" onclick="renderUserContents('Dashboard')" id="idSidebar-module-Dashboard"><i class="fas fa-chart-pie"></i> Dashboard</p>
            </li>
       	</ul>
    </div>
</div>