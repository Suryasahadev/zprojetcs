package org.zprojects.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;

import org.apache.naming.factory.MailSessionFactory;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.zprojects.connector.DBManager;
import org.zprojects.container.GlobalNotations;
import org.zprojects.container.SessionContainer;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ChartAction extends ActionSupport implements SessionContainer, GlobalNotations
{
	//For setting user requested content
	private String requestedContent = "";
	
	private int requestedUnit = 0;
	private int selectedUnitID = 0;
	private int selectedProjectID=0;
	private int selectedMilestoneID=0;
	
	
	public String getRequestedContent() {
		return requestedContent;
	}
	public void setRequestedContent(String requestedContent) {
		this.requestedContent = requestedContent;
	}
	public int getRequestedUnit() {
		return requestedUnit;
	}
	public void setRequestedUnit(int requestedUnit) {
		this.requestedUnit = requestedUnit;
	}
	public int getSelectedUnitID() {
		return selectedUnitID;
	}
	public void setSelectedUnitID(int selectedUnitID) {
		this.selectedUnitID = selectedUnitID;
	}
	public int getSelectedProjectID() {
		return selectedProjectID;
	}

	public void setSelectedProjectID(int selectedProjectID) {
		this.selectedProjectID = selectedProjectID;
	}
	
	public int getSelectedMilestoneID() {
		return selectedMilestoneID;
	}

	public void setSelectedMilestoneID(int selectedMilestoneID) {
		this.selectedMilestoneID = selectedMilestoneID;
	}
	
	public String enableChartSection()
	{
		//Enabling the chart section by setting the value
		if(requestedUnit==1)
			requestedContent = PROJECT_CHARTS;
		else if(requestedUnit==2)
			requestedContent = MILESTONE_CHARTS;
		else if(requestedUnit==4)
			requestedContent = USER_DASHBOARD;
		
		return SUCCESS;
	}
	
	@SuppressWarnings({ "unchecked", "serial" })
	public JSONObject getGantArrayContainer(String query, String constraintName, int constraintID)
	{
		JSONObject dataContainer = new JSONObject();
		JSONArray dataArray = new JSONArray();
		JSONArray colorCodeArray = new JSONArray();
		try(Connection dbConnectionObj = DBManager.getDBConnectorInst())
		{
			try(PreparedStatement taskListPrepObj = dbConnectionObj.prepareStatement(query);)
			{
				taskListPrepObj.setInt(1, constraintID);
				ResultSet taskListSet = taskListPrepObj.executeQuery();
				while(taskListSet.next())
				{
					/*
					 constructing a JSONArrayObject in the format of 
										[
					                     "Design",
					                     [
					                         '1463596200000',
					                         '1464028200000'
					                     ],
					                     'Srinivas',
					                     "Augustine"
					                 	]
					 */
					
					//Constructing the data array based on requested content (Tasks or Milestones)
					dataArray
					.add(
							new JSONArray() 
							{
			                    {
			                        add(taskListSet.getString(""+constraintName+"_Name"));
			                        add(new JSONArray() {
			                            {
			                                add(taskListSet.getString("Start_Millis"));
			                                add(taskListSet.getString("Due_Millis"));
			                            }
			                        });
			                        add(taskListSet.getString(""+constraintName+"Owner"));
			                        if(constraintName.equals("Task"))
			                        	add(taskListSet.getString("TaskAssignee"));
			                        else
			                        	add("");
			                    }
			                }
					);
					
					colorCodeArray.add(STATUS_COLOR_CODDING.get(taskListSet.getInt("Status")));
				}
				
				dataContainer.put("Data", dataArray);
				dataContainer.put("ColorData", colorCodeArray);
			}
		}
		catch(Exception connectExcep)
		{
			connectExcep.printStackTrace();
		}
		
		return dataContainer;
	}
	
	public JSONObject getTasksGant()
	{
		String getTaskQuery = "SELECT task.*, owner.Username AS TaskOwner, assignee.Username AS TaskAssignee, UNIX_TIMESTAMP(task.Start_Timestamp)* 1000 AS Start_Millis, UNIX_TIMESTAMP(task.Due_Timestamp)* 1000 AS Due_Millis FROM Task task LEFT JOIN Users owner ON task.Owner=owner.User_ID LEFT JOIN Users assignee ON task.Assignee=assignee.User_ID where task."+RENDER_CONTENT_ID_MAPPING.get(requestedUnit)+"=? ORDER BY Creation_Timestamp DESC;";
		return getGantArrayContainer(getTaskQuery, "Task", selectedUnitID);
	}
	public JSONObject getMilestoneGant()
	{
		String getTaskQuery = "SELECT milestone.*, owner.Username AS MilestoneOwner, UNIX_TIMESTAMP(milestone.Start_Timestamp)* 1000 AS Start_Millis, UNIX_TIMESTAMP(milestone.Due_Timestamp)* 1000 AS Due_Millis FROM Milestones milestone JOIN Users owner ON milestone.Owner=owner.User_ID where milestone."+RENDER_CONTENT_ID_MAPPING.get(requestedUnit)+"=? ORDER BY Creation_Timestamp DESC;";
		return getGantArrayContainer(getTaskQuery, "Milestone", selectedUnitID);
	}
	
	private void writeJSONString(JSONObject jsonObj)
	{
		//Writing it to Printwriter
		HttpServletResponse reponseObj = ServletActionContext.getResponse();
		reponseObj.setContentType("application/json");		
		try(PrintWriter writerObj = reponseObj.getWriter())
		{
			writerObj.write(jsonObj.toJSONString());
		}
		catch(IOException ioExcep)
		{
			ioExcep.printStackTrace();
		}
	}
	public void getProjectGant()
	{
		JSONObject taskObject = getTasksGant();
		JSONObject milestoneObject = getMilestoneGant();
		JSONObject combObj = new JSONObject();
		combObj.put("TaskData", taskObject);
		combObj.put("MilestoneData", milestoneObject);
		
		writeJSONString(combObj);
	}
	
	public void getMilestoneTaskGant()
	{
		writeJSONString(getTasksGant());
	}
	
	@SuppressWarnings({ "unchecked", "serial"})
	private JSONArray constructPieDataArray(ResultSet setObj)
	{
		JSONArray constrctArray = new JSONArray();
		
		try
		{
			setObj.next();
			constrctArray.add(
					new JSONArray()
					{
						{
							 add("Open");	
							 add(setObj.getInt("Open"));
						}
					});
			constrctArray.add(
					new JSONArray()
					{
						{
							 add("Development");	
							 add(setObj.getInt("Development"));
						}
					});
			constrctArray.add(
					new JSONArray()
					{
						{
							 add("Testing");	
							 add(setObj.getInt("Testing"));
						}
					});
			constrctArray.add(
					new JSONArray()
					{
						{
							 add("Closed");	
							 add(setObj.getInt("Closed"));
						}
					});
		}
		catch(SQLException resltExcep)
		{
			resltExcep.printStackTrace();
		}
		
		return constrctArray;
	}
	
	@SuppressWarnings({ "unchecked", "serial" })
	private JSONArray constructBarDataArray(ResultSet setObj)
	{
		JSONArray constrctArray = new JSONArray();
		
		try
		{
			while(setObj.next())
			{
				constrctArray.add(
						new JSONArray()
						{
							{
								 add(setObj.getString("Username"));	
								 add(setObj.getInt("Count"));
							}
						});
			}
		}
		catch(SQLException rsSetExcep)
		{
			rsSetExcep.printStackTrace();
		}
		
		return constrctArray;
	}
	
	public void getDashboardDatas()
	{
		JSONObject dashboardDataObj = new JSONObject();
		
		String countSubQuery = "COUNT(IF(Status=1, 1, NULL)) AS Open, COUNT(IF(Status=2, 1, NULL)) AS Development, COUNT(IF(Status=3, 1, NULL)) AS Testing, COUNT(IF(Status=4, 1, NULL)) AS Closed";
		try(Connection dbConnection = DBManager.getDBConnectorInst();
				PreparedStatement ownProjectDefPrepObj = dbConnection.prepareStatement("SELECT "+countSubQuery+" FROM Projects WHERE Owner=?;");
				PreparedStatement asgndProjectDefPrepObj = dbConnection.prepareStatement("SELECT "+countSubQuery+" FROM Projects WHERE Project_ID IN (SELECT Project_ID FROM Project_Map WHERE User_ID=?);");
				PreparedStatement ownTaskDefPrepObj = dbConnection.prepareStatement("SELECT "+countSubQuery+" FROM Task WHERE Owner=?;");
				PreparedStatement asgnedTaskDefPrepObj = dbConnection.prepareStatement("SELECT "+countSubQuery+" FROM Task WHERE Assignee=?;");
				
				PreparedStatement usrListPrepObj = dbConnection.prepareStatement("SELECT Username FROM Users;");
				PreparedStatement usrCreatedProjectPrepObj = dbConnection.prepareStatement("SELECT Username, COUNT(*) AS Count FROM Projects JOIN Users ON Projects.Owner=Users.User_ID GROUP BY Username;");
				PreparedStatement usrAssignedProjectPrepObj = dbConnection.prepareStatement("SELECT Username, COUNT(*) AS Count FROM Project_Map JOIN Users ON Project_Map.User_ID=Users.User_ID GROUP BY Username;");
				PreparedStatement usrCreatedTaskPrepObj = dbConnection.prepareStatement("SELECT Username, COUNT(*) AS Count FROM Task JOIN Users ON Task.Owner=Users.User_ID GROUP BY Username;");
				PreparedStatement usrAssignedTaskPrepObj = dbConnection.prepareStatement("SELECT Username, COUNT(*) AS Count FROM Task JOIN Users ON Task.Assignee=Users.User_ID GROUP BY Username;");
						)
		
		
		{
			ownProjectDefPrepObj.setInt(1, (int)getSession().get(SK_LOGGED_USERID));
			asgndProjectDefPrepObj.setInt(1, (int)getSession().get(SK_LOGGED_USERID));
			ownTaskDefPrepObj.setInt(1, (int)getSession().get(SK_LOGGED_USERID));
			asgnedTaskDefPrepObj.setInt(1, (int)getSession().get(SK_LOGGED_USERID));
			
			dashboardDataObj.put("OwnProjectData", constructPieDataArray(ownProjectDefPrepObj.executeQuery()));
			dashboardDataObj.put("AsgndProjectData", constructPieDataArray(asgndProjectDefPrepObj.executeQuery()));
			dashboardDataObj.put("OwnTaskData", constructPieDataArray(ownTaskDefPrepObj.executeQuery()));
			dashboardDataObj.put("AsgndTaskData", constructPieDataArray(asgnedTaskDefPrepObj.executeQuery()));
			
			
			//Adding all Username for categories List
			ResultSet userSet = usrListPrepObj.executeQuery();
			JSONArray userArray = new JSONArray();
			while(userSet.next())
			{
				userArray.add(userSet.getString("Username"));
			}
			dashboardDataObj.put("UserList", userArray);
			
			dashboardDataObj.put("UserCreatedProjectData", constructBarDataArray(usrCreatedProjectPrepObj.executeQuery()));
			dashboardDataObj.put("UserAssignedProjectData", constructBarDataArray(usrAssignedProjectPrepObj.executeQuery()));
			dashboardDataObj.put("UserCreatedTaskData", constructBarDataArray(usrCreatedTaskPrepObj.executeQuery()));
			dashboardDataObj.put("UserAssignedTaskData", constructBarDataArray(usrAssignedTaskPrepObj.executeQuery()));
			
			writeJSONString(dashboardDataObj);
		}
		catch(SQLException connectionExcep)
		{
			connectionExcep.printStackTrace();
		}
	}
}
