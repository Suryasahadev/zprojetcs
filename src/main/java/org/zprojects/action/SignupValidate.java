package org.zprojects.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.zprojects.connector.DBManager;
import org.zprojects.connector.MailGenerator;
import org.zprojects.container.GlobalNotations;
import org.zprojects.container.SessionContainer;

import com.opensymphony.xwork2.ActionSupport;

public class SignupValidate extends ActionSupport implements SessionContainer, GlobalNotations
{
	private String signUserName = "";
	private String signUserMailID = "";
	private String signPassword = "";
	private int insertedOTP = 0;
	private String errorCode = "";
	private String successCode = "";
	private String root="actionErrors";
	private int statusCode=400;
	
	public String getSignUserName() {
		return signUserName;
	}
	public void setSignUserName(String signUserName) {
		this.signUserName = signUserName;
	}
	public String getSignUserMailID() {
		return signUserMailID;
	}
	public void setSignUserMailID(String signUserMailID) {
		this.signUserMailID = signUserMailID;
	}
	public String getSignPassword() {
		return signPassword;
	}
	public void setSignPassword(String signPassword) {
		this.signPassword = signPassword;
	}
	public int getInsertedOTP() {
		return insertedOTP;
	}
	public void setInsertedOTP(int insertedOTP) {
		this.insertedOTP = insertedOTP;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getSuccessCode() {
		return successCode;
	}
	public void setSuccessCode(String successCode) {
		this.successCode = successCode;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String validateSignedUser()
	{
		if(isUserNameExist())
		{
			setErrorCode(EC_USER_NAME_EXISTS);
			return ERROR;
		}
		else if(isUserMailIDExist())
		{
			setErrorCode(EC_USER_MAIL_EXISTS);
			return ERROR;
		}
		else
		{
			setSuccessCode(SC_SIGN_USER_VALIDATION_SUCCESS);
			return SUCCESS;
		}
	}
	
	public String sendValidationdOTPMail()
	{
		MailGenerator.sendOTPMail(signUserName, signUserMailID);
		return SUCCESS;
	}
	
	public String verifySignUpOTP()
	{
		if(insertedOTP!=0 && insertedOTP==(int)getSession().get(CURRENTLY_GENERATED_OTP))
		{
			setSuccessCode(SC_OTP_VALIDATION_SUCCESS);
			
			//Adding User Details in DB
			int addUserID = addUserDetails();
			getSession().put(SK_LOGGED_USERNAME, getSignUserName());
			getSession().put(SK_LOGGED_USERID, addUserID);
			getSession().put(SK_USER_LOGGED_IN, true);
			
			return SUCCESS;
		}
		else
		{
			setErrorCode(EC_INCORRECT_OTP);
			return ERROR;
		}
	}
	private boolean isUserNameExist()
	{
		try(Connection userCheckConnectionObj = DBManager.getDBConnectorInst();
			PreparedStatement usernameStateObj = userCheckConnectionObj.prepareStatement("SELECT IF ( EXISTS(SELECT Username FROM Users WHERE Username=?), 1, 0) AS Return_Value;"))
		{
			usernameStateObj.setString(1, getSignUserName());
			ResultSet usernameSet = usernameStateObj.executeQuery();
			usernameSet.next();
			
			return usernameSet.getBoolean("Return_Value")? true : false;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	private boolean isUserMailIDExist()
	{
		try(Connection userCheckConnectionObj = DBManager.getDBConnectorInst();
			PreparedStatement usermailStateObj = userCheckConnectionObj.prepareStatement("SELECT IF ( EXISTS(SELECT Mail_ID FROM Users WHERE Mail_ID=?), 1, 0) AS Return_Value;"))
		{
			usermailStateObj.setString(1, getSignUserMailID());
			ResultSet usernameSet = usermailStateObj.executeQuery();
			usernameSet.next();
			
			return usernameSet.getBoolean("Return_Value")? true : false;
			
		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return true;
		}
	}
	/*
	 * For adding new user to DB and returning last inserted id.
	 */
	private int addUserDetails()
	{
		int addedUserID=0;
		
		try(Connection userInsrtConnection = DBManager.getDBConnectorInst();
				PreparedStatement userInsrtprepObj = userInsrtConnection.prepareStatement("INSERT INTO Users(Username, Mail_ID, Password) VALUES(?, ?, AES_ENCRYPT(?, 'Hash7'));");
				PreparedStatement lastUserIDprepObj = userInsrtConnection.prepareStatement("SELECT LAST_INSERT_ID();"))
		
		{
			try
			{
				userInsrtprepObj.setString(1, getSignUserName());
				userInsrtprepObj.setString(2, getSignUserMailID());
				userInsrtprepObj.setString(3, getSignPassword());
				userInsrtprepObj.executeUpdate();
				
				ResultSet idSet = lastUserIDprepObj.executeQuery();
				idSet.next();
				addedUserID = idSet.getInt("LAST_INSERT_ID()");
				
				userInsrtConnection.commit();
			}
			catch(SQLException e)
			{
				e.printStackTrace();
				userInsrtConnection.rollback();
			}
		} 
		catch (SQLException conectionExcep) {
			// TODO Auto-generated catch block
			conectionExcep.printStackTrace();
		}
		
		return addedUserID;
	}
	
	public String renderSignup()
	{
		return SUCCESS;
	}
}
