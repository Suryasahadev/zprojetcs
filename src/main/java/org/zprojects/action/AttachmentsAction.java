package org.zprojects.action;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.zprojects.modal.AttachmentEntity;
import org.zprojects.modal.MilestoneEntity;
import org.zprojects.connector.DBManager;
import org.zprojects.container.GlobalNotations;
import org.zprojects.container.SessionContainer;
import org.zprojects.modal.AttachmentEntity;

import com.opensymphony.xwork2.ActionSupport;

public class AttachmentsAction extends ActionSupport implements SessionContainer, GlobalNotations
{
	//For setting user requested content
	private String requestedContent = "";
	private int requestedUnit = 0;
	
	//Global values
	private int selectedProjectID = 0;
	private int selectedMilestoneID = 0;
	private int selectedTaskID = 0;
	private int selectedUnitID = 0;
	private int selectedAttachmentID = 0;
	private InputStream downloadingFileStream = null;
	private String downloadingFileName = "";
	
	List<AttachmentEntity> attachmentList = new ArrayList<AttachmentEntity>();
	
	public String getRequestedContent() {
		return requestedContent;
	}
	public void setRequestedContent(String requestedContent) {
		this.requestedContent = requestedContent;
	}
	public int getRequestedUnit() {
		return requestedUnit;
	}
	public void setRequestedUnit(int requestedUnit) {
		this.requestedUnit = requestedUnit;
	}
	public int getSelectedProjectID() {
		return selectedProjectID;
	}
	public void setSelectedProjectID(int selectedProjectID) {
		this.selectedProjectID = selectedProjectID;
	}
	public int getSelectedMilestoneID() {
		return selectedMilestoneID;
	}
	public void setSelectedMilestoneID(int selectedMilestoneID) {
		this.selectedMilestoneID = selectedMilestoneID;
	}
	public int getSelectedTaskID() {
		return selectedTaskID;
	}
	public void setSelectedTaskID(int selectedTaskID) {
		this.selectedTaskID = selectedTaskID;
	}
	public int getSelectedUnitID() {
		return selectedUnitID;
	}
	public void setSelectedUnitID(int selectedUnitID) {
		this.selectedUnitID = selectedUnitID;
	}
	public int getSelectedAttachmentID() {
		return selectedAttachmentID;
	}
	public void setSelectedAttachmentID(int selectedAttachmentID) {
		this.selectedAttachmentID = selectedAttachmentID;
	}
	public InputStream getDownloadingFileStream() {
		return downloadingFileStream;
	}
	public void setDownloadingFileStream(InputStream downloadingFileStream) {
		this.downloadingFileStream = downloadingFileStream;
	}
	public String getDownloadingFileName() {
		return downloadingFileName;
	}
	public void setDownloadingFileName(String downloadingFileName) {
		this.downloadingFileName = downloadingFileName;
	}
	public List<AttachmentEntity> getAttachmentList() {
		return attachmentList;
	}
	public void setAttachmentList(List<AttachmentEntity> attachmentList) {
		this.attachmentList = attachmentList;
	}
	
	private void initAttachmentList(String query, int constraintID)
	{
		try(
			Connection connectionObj = DBManager.getDBConnectorInst();
			PreparedStatement attchmntLstPrepObj = connectionObj.prepareStatement(query);
			)
		{
			attchmntLstPrepObj.setInt(1, constraintID);
			ResultSet atchmntDetSet = attchmntLstPrepObj.executeQuery();
			while(atchmntDetSet.next())
		    {
				attachmentList.add(new AttachmentEntity(atchmntDetSet.getString("File_Name"), atchmntDetSet.getInt("Attachment_ID")));
		    }
		}
		catch (SQLException queryExcep) 
		{
			// TODO Auto-generated catch block
			queryExcep.printStackTrace();
		}
	}
	
	public String getUnitAttachments()
	{
		String atchmntPreQuery = "SELECT File_Name, Attachment_ID FROM Attachments WHERE "+RENDER_CONTENT_ID_MAPPING.get(requestedUnit)+"=?";
		initAttachmentList(atchmntPreQuery, selectedUnitID);
		
		//Setting re content based on unit value
		if(requestedUnit==1)
			requestedContent = PROJECT_ATTACHMENTS;
		else if(requestedUnit==2)
			requestedContent = MILESTONE_ATTACHMENTS;
		else
			requestedContent = TASK_ATTACHMENTS;
		
		return SUCCESS;
	}
	
	public String downloadAttachment()
	{
		try(
				Connection connectionObj = DBManager.getDBConnectorInst();
				PreparedStatement docsGetStateobj = connectionObj.prepareStatement("SELECT File, File_Name FROM Attachments WHERE Attachment_ID=?");
			)
		{
			
			//Getting File Contents From DB
			docsGetStateobj.setInt(1, selectedAttachmentID);
			ResultSet docsSet = docsGetStateobj.executeQuery();
			while(docsSet.next())
			{
				//Setting Download Stream
				setDownloadingFileStream(docsSet.getBinaryStream("File"));
				//Setting Download Filename
				setDownloadingFileName(docsSet.getString("File_Name"));
			}
		}
		catch(SQLException connectionExcep)
		{
			connectionExcep.printStackTrace();
		}
		
		return SUCCESS;
	}
}
