package org.zprojects.action;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.zprojects.modal.ProjectEntity;
import org.zprojects.connector.DBManager;
import org.zprojects.container.GlobalNotations;
import org.zprojects.container.SessionContainer;

import com.opensymphony.xwork2.ActionSupport;

public class ProjectAction extends ActionSupport implements SessionContainer, GlobalNotations
{
	//For setting user requested content
	private String requestedContent = "";
	
	//Global values
	private String crtNewProjectName = "";
	private String crtNewProjectDescription = "";
	private String crtNewProjectOwner = "";
	private String crtNewProjectMembers = "";
	private String crtNewProjectFrmDate;
	private String crtNewProjectToDate;
	private int crtNewProjectPriority = 0;
	private int crtNewProjectStatus = 0;
	private String crtNewProjectTags;
	private File[] crtNewProjectFiles;
	private String[] crtNewProjectFilesName;
	private ProjectEntity newProjectEntity = null;
	private int selectedProjectID = 0;
	private ProjectEntity detailingPrjctEntityObj = null;
	
	//Project DM Variables
	private int lastInsertID = 0;
	
	//List which will be Used in View
	List<ProjectEntity> projectList = new ArrayList<ProjectEntity>();
	
	public String getRequestedContent() {
		return requestedContent;
	}
	public void setRequestedContent(String requestedContent) {
		this.requestedContent = requestedContent;
	}
	
	public String getCrtNewProjectName() {
		return crtNewProjectName;
	}
	public void setCrtNewProjectName(String crtNewProjectName) {
		this.crtNewProjectName = crtNewProjectName;
	}
	public String getCrtNewProjectDescription() {
		return crtNewProjectDescription;
	}
	public void setCrtNewProjectDescription(String crtNewProjectDescription) {
		this.crtNewProjectDescription = crtNewProjectDescription;
	}
	public String getCrtNewProjectOwner() {
		return crtNewProjectOwner;
	}
	public void setCrtNewProjectOwner(String crtNewProjectOwner) {
		this.crtNewProjectOwner = crtNewProjectOwner;
	}
	public String getCrtNewProjectMembers() {
		return crtNewProjectMembers;
	}
	public void setCrtNewProjectMembers(String crtNewProjectMembers) {
		this.crtNewProjectMembers = crtNewProjectMembers;
	}
	public String getCrtNewProjectFrmDate() {
		return crtNewProjectFrmDate;
	}
	public void setCrtNewProjectFrmDate(String crtNewProjectFrmDate) {
		this.crtNewProjectFrmDate = crtNewProjectFrmDate;
	}
	public String getCrtNewProjectToDate() {
		return crtNewProjectToDate;
	}
	public void setCrtNewProjectToDate(String crtNewProjectToDate) {
		this.crtNewProjectToDate = crtNewProjectToDate;
	}
	public int getCrtNewProjectPriority() {
		return crtNewProjectPriority;
	}
	public void setCrtNewProjectPriority(int crtNewProjectPriority) {
		this.crtNewProjectPriority = crtNewProjectPriority;
	}
	public int getCrtNewProjectStatus() {
		return crtNewProjectStatus;
	}
	public void setCrtNewProjectStatus(int crtNewProjectStatus) {
		this.crtNewProjectStatus = crtNewProjectStatus;
	}
	public String getCrtNewProjectTags() {
		return crtNewProjectTags;
	}
	public void setCrtNewProjectTags(String crtNewProjectTags) {
		this.crtNewProjectTags = crtNewProjectTags;
	}
	public File[] getCrtNewProjectFiles() {
		return crtNewProjectFiles;
	}
	public void setCrtNewProjectFiles(File[] crtNewProjectFiles) {
		this.crtNewProjectFiles = crtNewProjectFiles;
	}
	public String[] getCrtNewProjectFilesName() {
		return crtNewProjectFilesName;
	}
	public void setCrtNewProjectFilesName(String[] crtNewProjectFilesName) {
		this.crtNewProjectFilesName = crtNewProjectFilesName;
	}
	public ProjectEntity getNewProjectEntity() {
		return newProjectEntity;
	}
	public void setNewProjectEntity(ProjectEntity newProjectEntity) {
		this.newProjectEntity = newProjectEntity;
	}
	public int getSelectedProjectID() {
		return selectedProjectID;
	}
	public void setSelectedProjectID(int selectedProjectID) {
		this.selectedProjectID = selectedProjectID;
	}
	public ProjectEntity getDetailingPrjctEntityObj() {
		return detailingPrjctEntityObj;
	}
	public void setDetailingPrjctEntityObj(ProjectEntity detailingPrjctEntityObj) {
		this.detailingPrjctEntityObj = detailingPrjctEntityObj;
	}
	
	public List<ProjectEntity> getProjectList() {
		return projectList;
	}
	public void setProjectList(List<ProjectEntity> projectList) {
		this.projectList = projectList;
	}
	
	//Project DM Getters and Setters
	public int getLastInsertID() {
		return lastInsertID;
	}
	public void setLastInsertID(int lastInsertID) {
		this.lastInsertID = lastInsertID;
	}
	
	private void initProjectList(String query, int memberID)
	{
		try(
			Connection connectionObj = DBManager.getDBConnectorInst();
			PreparedStatement prjctLstPrepObj = connectionObj.prepareStatement(query);
			)
		{
			prjctLstPrepObj.setInt(1, memberID);
			ResultSet prjctDetSet = prjctLstPrepObj.executeQuery();
			while(prjctDetSet.next())
		    {
				projectList.add(new ProjectEntity(prjctDetSet.getInt("Project_ID"), prjctDetSet.getString("Project_Name"), prjctDetSet.getString("Description"), prjctDetSet.getString("ProjectOwner"), null, GlobalNotations.PRIORITY_MAPPING.get(prjctDetSet.getInt("Priority")), GlobalNotations.STATUS_MAPPING.get(prjctDetSet.getInt("Status")), prjctDetSet.getTimestamp("Creation_Timestamp"), prjctDetSet.getTimestamp("Start_Timestamp"), prjctDetSet.getTimestamp("Due_Timestamp"), prjctDetSet.getTimestamp("Completion_Timestamp")));
		    }
		}
		catch (SQLException queryExcep) 
		{
			// TODO Auto-generated catch block
			queryExcep.printStackTrace();
		}
	}
	
	public String getCreatedProjects()
	{
		if(getSession().get(SK_AUTH_BREACH)!=null && (boolean)getSession().get(SK_AUTH_BREACH))
		{
			getSession().put(SK_AUTH_BREACH, null);
			addActionError(AUTH_BREACH_SESSION);
		}
		
		String projectPreQuery = "SELECT Projects.*, Users.Username AS ProjectOwner FROM Projects JOIN Users ON Projects.Owner = Users.User_ID WHERE Projects.Owner = ? ORDER BY Creation_Timestamp DESC;";
		initProjectList(projectPreQuery, (int)getSession().get(SK_LOGGED_USERID));
		
		requestedContent = CREATED_PROJECTS;
		return SUCCESS;
	}
	
	public String getAssignedProjects()
	{
		String projectPreQuery = "SELECT Projects.*,Users.Username AS ProjectOwner FROM Projects JOIN Users ON Projects.Owner = Users.User_ID WHERE Project_ID IN (SELECT Project_ID FROM Project_Map WHERE User_ID=?) ORDER BY Creation_Timestamp DESC;";
		initProjectList(projectPreQuery, (int)getSession().get(SK_LOGGED_USERID));
		
		requestedContent = ASSIGNED_PROJECTS;
		return SUCCESS;
	}
	
	public String getProjectDetails()
	{
		try(
			Connection connectionObj = DBManager.getDBConnectorInst();
			PreparedStatement prjctMembsPrepObj = connectionObj.prepareStatement("SELECT Users.Username FROM Project_Map JOIN Users ON Project_Map.User_ID=Users.User_ID WHERE Project_ID=?");
			PreparedStatement prjctDetPrepObj = connectionObj.prepareStatement("SELECT Projects.*, Users.Username FROM Projects JOIN Users ON Projects.Owner = Users.User_ID WHERE Project_ID=?");
			)
		{
			prjctMembsPrepObj.setInt(1, selectedProjectID);
			ResultSet prjctMembsSet = prjctMembsPrepObj.executeQuery();
			List<String> prctMembers = new ArrayList<String>();
			while(prjctMembsSet.next())
			{
				prctMembers.add(prjctMembsSet.getString("Username"));
			}
			
			prjctDetPrepObj.setInt(1, selectedProjectID);
			ResultSet prjctDetSet = prjctDetPrepObj.executeQuery();
			while(prjctDetSet.next())
		    {
				setDetailingPrjctEntityObj(new ProjectEntity(prjctDetSet.getInt("Project_ID"), prjctDetSet.getString("Project_Name"), prjctDetSet.getString("Description"), prjctDetSet.getString("Username"), prctMembers, GlobalNotations.PRIORITY_MAPPING.get(prjctDetSet.getInt("Priority")), GlobalNotations.STATUS_MAPPING.get(prjctDetSet.getInt("Status")), prjctDetSet.getTimestamp("Creation_Timestamp"), prjctDetSet.getTimestamp("Start_Timestamp"), prjctDetSet.getTimestamp("Due_Timestamp"), prjctDetSet.getTimestamp("Completion_Timestamp")));
		    }
		}
		catch (SQLException queryExcep) 
		{
			// TODO Auto-generated catch block
			queryExcep.printStackTrace();
		}
		
		requestedContent = PROJECT_DETAILS;
		return SUCCESS;
	}
	
	//Common Operations for Project DM's
	
	/*
	 * For getting User_ID as List with given UserNames in crtNewProjectMembers
	 */
	private List<Integer> getUserIds()
	{
		List<Integer> userIDList = new ArrayList<Integer>();
		try(Connection dbConnectionObj = DBManager.getDBConnectorInst();)
		{
			try(
					PreparedStatement userAccPrepObj = dbConnectionObj.prepareStatement("SELECT User_ID FROM USERS WHERE Username=?"); 
				)
			{
				if(!crtNewProjectMembers.isEmpty())
				{
					for(String assignee : crtNewProjectMembers.split(", "))
					{
						userAccPrepObj.setString(1, assignee);
						ResultSet assighneeSet = userAccPrepObj.executeQuery();
						
						if(assighneeSet.next())
							userIDList.add(assighneeSet.getInt("User_ID"));
					}
				}
			}
			catch(SQLException stateObjExcep)
			{
				stateObjExcep.printStackTrace();
			}
		}
		catch(SQLException connectExcep)
		{
			connectExcep.printStackTrace();
		}
			
		return userIDList;
	}
	
	/*
	 * For Updating the existing project
	 */
	private ProjectAction addProjectData(Connection dbConnectionObj, int project_ID) throws SQLException
	{
		PreparedStatement prjctUpdtPrepObj = dbConnectionObj.prepareStatement("UPDATE Projects SET Project_Name=?, Description=?, Priority=?, Status=?, Start_Timestamp=?, Due_Timestamp=? WHERE Project_ID=?;");
		
		prjctUpdtPrepObj.setString(1, this.crtNewProjectName);
		prjctUpdtPrepObj.setString(2, this.crtNewProjectDescription);
		prjctUpdtPrepObj.setInt(3,this.crtNewProjectPriority);
		prjctUpdtPrepObj.setInt(4,this.crtNewProjectStatus);
		prjctUpdtPrepObj.setTimestamp(5, Timestamp.valueOf(this.crtNewProjectFrmDate+GlobalNotations.TIMESTAMP_SUFFIX));
		prjctUpdtPrepObj.setTimestamp(6, Timestamp.valueOf(this.crtNewProjectToDate+GlobalNotations.TIMESTAMP_SUFFIX));
		prjctUpdtPrepObj.setInt(7, project_ID);
		
		prjctUpdtPrepObj.executeUpdate();
		
		return this;
	}
	
	/*
	 * For Adding New Project
	 */
	private ProjectAction addProjectData(Connection dbConnectionObj) throws SQLException
	{
		PreparedStatement prjctApndPrepObj = dbConnectionObj.prepareStatement("INSERT INTO Projects(Project_Name, Description, Owner, Priority, Status, Creation_Timestamp, Start_Timestamp, Due_Timestamp, Completion_Timestamp) VALUES(?, ?, ?, ?, ?, now(), ?, ?, NULL);");
		
		prjctApndPrepObj.setString(1, this.crtNewProjectName);
		prjctApndPrepObj.setString(2, this.crtNewProjectDescription);
		prjctApndPrepObj.setInt(3, (int)getSession().get(SK_LOGGED_USERID));
		prjctApndPrepObj.setInt(4,this.crtNewProjectPriority);
		prjctApndPrepObj.setInt(5,this.crtNewProjectStatus);
		prjctApndPrepObj.setTimestamp(6, Timestamp.valueOf(this.crtNewProjectFrmDate+GlobalNotations.TIMESTAMP_SUFFIX));
		prjctApndPrepObj.setTimestamp(7, Timestamp.valueOf(this.crtNewProjectToDate+GlobalNotations.TIMESTAMP_SUFFIX));
		
		prjctApndPrepObj.executeUpdate();
		
		setLastInsertID(getLastDMInsertID(dbConnectionObj));
		
		return this;
	}
	
	/*
	 * For getting last insert ID of added project
	 */
	private int getLastDMInsertID(Connection dbConnectionObj) throws SQLException
	{
		PreparedStatement appndPrjctIDPrepObj = dbConnectionObj.prepareStatement("SELECT LAST_INSERT_ID();");
		ResultSet prjctIDSet = appndPrjctIDPrepObj.executeQuery();
		prjctIDSet.next();
		int apndPrjctID = prjctIDSet.getInt("LAST_INSERT_ID()");
		
		return apndPrjctID;
	}
	
	/*
	 * For deleting project mapping so as to add edited project entries
	 */
	private ProjectAction deleteProjectMapping(Connection dbConnectionObj, int project_ID) throws SQLException
	{
		PreparedStatement dltPrctMapPrepObj = dbConnectionObj.prepareStatement("DELETE FROM Project_Map WHERE Project_ID=?;");
		
		dltPrctMapPrepObj.setInt(1, selectedProjectID);
		dltPrctMapPrepObj.executeUpdate();
		
		return this;
	}
	
	/*
	 * For Adding all Assignee's to Project_Map
	 */
	private ProjectAction appendProjectMapping(Connection dbConnectionObj, int project_ID, List<Integer> users_ID) throws SQLException
	{
		PreparedStatement prjctMapApndPrepObj = dbConnectionObj.prepareStatement("INSERT INTO Project_Map(Project_ID, User_ID) Values(?, ?);");
		
		prjctMapApndPrepObj.setInt(1, project_ID);
		for(int assignee_ID : users_ID)
		{	
			prjctMapApndPrepObj.setInt(2, assignee_ID);
			prjctMapApndPrepObj.executeUpdate();
		}
		
		return this;
	}
	
	/*
	 * For Appending files to Project
	 */
	private ProjectAction appendProjectAttachments(Connection dbConnectionObj, int project_ID, File[] fileArr, String[] fileNames) throws SQLException
	{
		PreparedStatement atchmntApndPrepObj = dbConnectionObj.prepareStatement("INSERT INTO Attachments(Project_ID, File, File_Name, Uploaded_Time) Values(?, ?, ?, now());");
		
		if(fileArr!=null)
		{
			for(int curFileIndex=0; curFileIndex<fileArr.length; curFileIndex++)
			{
				//Inserting File as BLOB
				atchmntApndPrepObj.setInt(1, project_ID);
				atchmntApndPrepObj.setString(3, fileNames[curFileIndex]);
				
				try(FileInputStream ipStream = new FileInputStream(fileArr[curFileIndex]))
				{
					atchmntApndPrepObj.setBinaryStream(2, ipStream, (int)fileArr[curFileIndex].length());
					atchmntApndPrepObj.executeUpdate();
				}
				catch(Exception excep)
				{
					excep.printStackTrace();
				}
			}
		}
		
		return this;
	}
	
	//For Adding New Project
	public void addNewProject()
	{
		//Design Pattern Approach to achieve Project Adding.
		try(Connection dbConnectionObj = DBManager.getDBConnectorInst();)
		{
			try
			{
				addProjectData(dbConnectionObj)
					.appendProjectMapping(dbConnectionObj, lastInsertID, getUserIds())
					.appendProjectAttachments(dbConnectionObj, lastInsertID, crtNewProjectFiles, crtNewProjectFilesName);
				
				dbConnectionObj.commit();
			}
			catch(SQLException dmException)
			{
				dbConnectionObj.rollback();
				dmException.printStackTrace();
			}
		}
		catch(SQLException connectExcep)
		{
			connectExcep.printStackTrace();
		}
	}
	
	//For Editing Project
	public void editProject()
	{
		try(Connection dbConnectionObj = DBManager.getDBConnectorInst();)
		{
			try
			{
				addProjectData(dbConnectionObj, selectedProjectID)
					.deleteProjectMapping(dbConnectionObj, selectedProjectID)
					.appendProjectMapping(dbConnectionObj, selectedProjectID, getUserIds())
					.appendProjectAttachments(dbConnectionObj, selectedProjectID, crtNewProjectFiles, crtNewProjectFilesName);
				
				dbConnectionObj.commit();
			}
			catch(SQLException dmException)
			{
				dbConnectionObj.rollback();
				dmException.printStackTrace();
			}
		}
		catch(SQLException connectExcep)
		{
			connectExcep.printStackTrace();
		}
	}
}
