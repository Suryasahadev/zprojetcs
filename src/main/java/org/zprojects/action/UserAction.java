package org.zprojects.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.zprojects.connector.DBManager;
import org.zprojects.container.GlobalNotations;
import org.zprojects.container.SessionContainer;

import com.opensymphony.xwork2.ActionSupport;

public class UserAction extends ActionSupport implements SessionContainer, GlobalNotations
{
	public void getUsernameList()
	{
		JSONObject userListJSNObj = new JSONObject();
		JSONArray userListJSNArray = new JSONArray();
		
		HttpServletResponse responseObj =  ServletActionContext.getResponse();
		responseObj.setContentType("application/json");
		
		try
			(
				PrintWriter writerObj = responseObj.getWriter();
				Connection connObj = DBManager.getDBConnectorInst();
				PreparedStatement userPrepObj = connObj.prepareStatement("SELECT Username FROM Users;");
			)
		{
			ResultSet userNameSet = userPrepObj.executeQuery();
			while(userNameSet.next())
			{
				userListJSNArray.add(userNameSet.getString(1));
			}
			userListJSNObj.put("userArr", userListJSNArray);
			writerObj.write(userListJSNObj.toJSONString());
		}
		catch(IOException |  SQLException excep)
		{
			excep.printStackTrace();
		}
	}
}
