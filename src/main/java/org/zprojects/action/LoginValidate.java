package org.zprojects.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.struts2.dispatcher.SessionMap;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import org.zprojects.connector.DBManager;
import org.zprojects.container.GlobalNotations;
import org.zprojects.container.SessionContainer;

public class LoginValidate extends ActionSupport implements SessionContainer, GlobalNotations
{
	private String username = "";
	private String password = "";
	private String errorCode = "";
	private String successCode = "";
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getSuccessCode() {
		return successCode;
	}
	public void setSuccessCode(String successCode) {
		this.successCode = successCode;
	}
	
	public String validateLoginUser()
	{
		try(Connection userLogConnection = DBManager.getDBConnectorInst();
				PreparedStatement userListPrepObj = userLogConnection.prepareStatement("SELECT IF (EXISTS(SELECT Username FROM Users WHERE Username=? OR Mail_ID=?), 1, 0);"))
		{
			userListPrepObj.setString(1, username);
			userListPrepObj.setString(2, username);
			ResultSet userSet = userListPrepObj.executeQuery();
			userSet.next();
			
			if(!userSet.getBoolean(1))
			{
				setErrorCode(EC_INVALID_USERNAME);
			}
			else
			{
				try(PreparedStatement userPassPreObj = userLogConnection.prepareStatement("SELECT User_ID, Username, CAST(AES_DECRYPT(Password, 'Hash7') AS CHAR) DecryptedPass FROM Users WHERE Username=? OR Mail_ID=?;"))
				{
					userPassPreObj.setString(1, username);
					userPassPreObj.setString(2, username);
					ResultSet passSetObj = userPassPreObj.executeQuery();
					passSetObj.next();
					String dcryptdPasswrd = passSetObj.getString("DecryptedPass");
					if(!dcryptdPasswrd.equals(password))
					{
						setErrorCode(EC_INVALID_USER_PASSWORD);
					}
					else
					{
						getSession().put(SK_USER_LOGGED_IN, true);
						getSession().put(SK_LOGGED_USERNAME, passSetObj.getString("Username"));
						getSession().put(SK_LOGGED_USERID, passSetObj.getInt("User_ID"));
						setSuccessCode(SC_LOGIN_UP_SUCCESS);
					}
				}
			}
		}
		catch(SQLException exception)
		{
			exception.printStackTrace();
		}
		
		return SUCCESS;
	}
	public String renderLogin()
	{
		if(getSession().get(SK_HOME_BREACH)!=null && (boolean)getSession().get(SK_HOME_BREACH))
		{
			getSession().put(SK_HOME_BREACH, null);
			addActionError(UNAUTHORIZED_SESSION);
			return DIRECT_HIT;
		}
		else
		{
			return SUCCESS;
		}
	}
	public String logout()
	{
		SessionMap sessionMapObj = (SessionMap)getSession();
		sessionMapObj.invalidate();
		
		return SUCCESS;
	}
}
