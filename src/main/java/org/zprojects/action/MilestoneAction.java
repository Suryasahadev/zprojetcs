package org.zprojects.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.zprojects.connector.DBManager;
import org.zprojects.container.GlobalNotations;
import org.zprojects.container.SessionContainer;
import org.zprojects.modal.MilestoneEntity;
import org.zprojects.modal.TaskEntity;

import com.opensymphony.xwork2.ActionSupport;

public class MilestoneAction extends ActionSupport implements SessionContainer, GlobalNotations
{
	//For setting user requested content
	private String requestedContent = "";
	
	//Global values
	private String crtNewMlstnName = "";
	private String crtNewMlstnProjectName = "";
	private int crtNewMlstnProjectID = 0;
	private String crtNewMlstnDescription = "";
	private String crtNewMlstnOwner = "";
	private String crtNewMlstnTasksID;
	private String crtNewMlstnFrmDate = "";
	private String crtNewMlstnToDate = "";
	private int crtNewMlstnPriority = 0;
	private int crtNewMlstnStatus = 0;
	private String crtNewMlstnTags = "";
	private File[] crtNewMlstnFiles;
	private String[] crtNewMlstnFilesName;
	private MilestoneEntity detailingMlstnEntityObj = null;

	private int selectedProjectID = 0;
	private int selectedMilestoneID = 0;
	
	//Project DM Variables
	private int lastInsertID = 0;
		
	//List which will be Used in View
	List<MilestoneEntity> milestoneList = new ArrayList<MilestoneEntity>();
	Map<TaskEntity, Boolean> mlstnTaskMap = new HashMap<TaskEntity, Boolean>();	
	
	public String getRequestedContent() {
		return requestedContent;
	}
	public void setRequestedContent(String requestedContent) {
		this.requestedContent = requestedContent;
	}
	public String getCrtNewMlstnName() {
		return crtNewMlstnName;
	}
	public void setCrtNewMlstnName(String crtNewMlstnName) {
		this.crtNewMlstnName = crtNewMlstnName;
	}
	public String getCrtNewMlstnProjectName() {
		return crtNewMlstnProjectName;
	}
	public void setCrtNewMlstnProjectName(String crtNewMlstnProjectName) {
		this.crtNewMlstnProjectName = crtNewMlstnProjectName;
	}
	public int getCrtNewMlstnProjectID() {
		return crtNewMlstnProjectID;
	}
	public void setCrtNewMlstnProjectID(int crtNewMlstnProjectID) {
		this.crtNewMlstnProjectID = crtNewMlstnProjectID;
	}
	public String getCrtNewMlstnDescription() {
		return crtNewMlstnDescription;
	}
	public void setCrtNewMlstnDescription(String crtNewMlstnDescription) {
		this.crtNewMlstnDescription = crtNewMlstnDescription;
	}
	public String getCrtNewMlstnOwner() {
		return crtNewMlstnOwner;
	}
	public void setCrtNewMlstnOwner(String crtNewMlstnOwner) {
		this.crtNewMlstnOwner = crtNewMlstnOwner;
	}
	public String getCrtNewMlstnTasksID() {
		return crtNewMlstnTasksID;
	}
	public void setCrtNewMlstnTasksID(String crtNewMlstnTasksID) {
		this.crtNewMlstnTasksID = crtNewMlstnTasksID;
	}
	public String getCrtNewMlstnFrmDate() {
		return crtNewMlstnFrmDate;
	}
	public void setCrtNewMlstnFrmDate(String crtNewMlstnFrmDate) {
		this.crtNewMlstnFrmDate = crtNewMlstnFrmDate;
	}
	public String getCrtNewMlstnToDate() {
		return crtNewMlstnToDate;
	}
	public void setCrtNewMlstnToDate(String crtNewMlstnToDate) {
		this.crtNewMlstnToDate = crtNewMlstnToDate;
	}
	public int getCrtNewMlstnPriority() {
		return crtNewMlstnPriority;
	}
	public void setCrtNewMlstnPriority(int crtNewMlstnPriority) {
		this.crtNewMlstnPriority = crtNewMlstnPriority;
	}
	public int getCrtNewMlstnStatus() {
		return crtNewMlstnStatus;
	}
	public void setCrtNewMlstnStatus(int crtNewMlstnStatus) {
		this.crtNewMlstnStatus = crtNewMlstnStatus;
	}
	public String getCrtNewMlstnTags() {
		return crtNewMlstnTags;
	}
	public void setCrtNewMlstnTags(String crtNewMlstnTags) {
		this.crtNewMlstnTags = crtNewMlstnTags;
	}
	public File[] getCrtNewMlstnFiles() {
		return crtNewMlstnFiles;
	}
	public void setCrtNewMlstnFiles(File[] crtNewMlstnFiles) {
		this.crtNewMlstnFiles = crtNewMlstnFiles;
	}
	public String[] getCrtNewMlstnFilesName() {
		return crtNewMlstnFilesName;
	}
	public void setCrtNewMlstnFilesName(String[] crtNewMlstnFilesName) {
		this.crtNewMlstnFilesName = crtNewMlstnFilesName;
	}
	public MilestoneEntity getDetailingMlstnEntityObj() {
		return detailingMlstnEntityObj;
	}
	public void setDetailingMlstnEntityObj(MilestoneEntity detailingMlstnEntityObj) {
		this.detailingMlstnEntityObj = detailingMlstnEntityObj;
	}
	public int getSelectedProjectID() {
		return selectedProjectID;
	}
	public void setSelectedProjectID(int selectedProjectID) {
		this.selectedProjectID = selectedProjectID;
	}
	public int getSelectedMilestoneID() {
		return selectedMilestoneID;
	}
	public void setSelectedMilestoneID(int selectedMilestoneID) {
		this.selectedMilestoneID = selectedMilestoneID;
	}
	public int getLastInsertID() {
		return lastInsertID;
	}
	public void setLastInsertID(int lastInsertID) {
		this.lastInsertID = lastInsertID;
	}
	public List<MilestoneEntity> getMilestoneList() {
		return milestoneList;
	}
	public void setMilestoneList(List<MilestoneEntity> milestoneList) {
		this.milestoneList = milestoneList;
	}
	public Map<TaskEntity, Boolean> getMlstnTaskMap() {
		return mlstnTaskMap;
	}
	public void setMlstnTaskMap(Map<TaskEntity, Boolean> mlstnTaskMap) {
		this.mlstnTaskMap = mlstnTaskMap;
	}
	
	private void initMilestoneList(String query, int constraintID)
	{
		try(
			Connection connectionObj = DBManager.getDBConnectorInst();
			PreparedStatement mlstnLstPrepObj = connectionObj.prepareStatement(query);
			)
		{
			mlstnLstPrepObj.setInt(1, constraintID);
			ResultSet mlstnDetSet = mlstnLstPrepObj.executeQuery();
			while(mlstnDetSet.next())
		    {
				milestoneList.add(new MilestoneEntity(mlstnDetSet.getInt("Milestone_ID"), mlstnDetSet.getString("Milestone_Name"), mlstnDetSet.getString("Milestone_Description"), mlstnDetSet.getString("MilestoneOwner"), mlstnDetSet.getTimestamp("Start_Timestamp"), mlstnDetSet.getTimestamp("Due_Timestamp"), GlobalNotations.PRIORITY_MAPPING.get(mlstnDetSet.getInt("Priority")), GlobalNotations.STATUS_MAPPING.get(mlstnDetSet.getInt("Status"))));
		    }
		}
		catch (SQLException queryExcep) 
		{
			// TODO Auto-generated catch block
			queryExcep.printStackTrace();
		}
	}
	
	public String getCreatedMilestones()
	{
		String mlstnPreQuery = "SELECT milestone.*, owner.Username AS MilestoneOwner FROM Milestones milestone JOIN Users owner ON milestone.Owner=owner.User_ID where milestone.owner=?;";
		initMilestoneList(mlstnPreQuery, (int)getSession().get(SK_LOGGED_USERID));
		
		requestedContent = CREATED_MILESTONES;
		return SUCCESS;
	}
	
	public String getProjectMilestones()
	{
		String mlstnPreQuery = "SELECT milestone.*, owner.Username AS MilestoneOwner FROM Milestones milestone JOIN Users owner ON milestone.Owner=owner.User_ID where milestone.Project_ID=?;";
		initMilestoneList(mlstnPreQuery, selectedProjectID);
		
		requestedContent = PROJECT_MILESTONES;
		return SUCCESS;
	}
	
	public String getMilestoneDetails()
	{
		try
			(
				Connection connectionObj = DBManager.getDBConnectorInst();
				PreparedStatement mlstnDetPrepObj = connectionObj.prepareStatement("SELECT milestone.*, owner.Username AS MilestoneOwner FROM Milestones milestone JOIN Users owner ON milestone.Owner=owner.User_ID where Milestone_ID=?;");
				PreparedStatement mergedTaskListPrepObj = connectionObj.prepareStatement("SELECT *, IF(Milestone_ID=?, 1, 0) AS IsAssociated FROM Task WHERE Project_ID=?");
			)
		{
			int pinnedProjectID = 0;
			mlstnDetPrepObj.setInt(1, selectedMilestoneID);
			
			ResultSet mlstnDetailSet = mlstnDetPrepObj.executeQuery();
			if(mlstnDetailSet.next())
			{
				pinnedProjectID = mlstnDetailSet.getInt("Project_ID");
				setDetailingMlstnEntityObj(new MilestoneEntity(mlstnDetailSet.getInt("Milestone_ID"), mlstnDetailSet.getString("Milestone_Name"), mlstnDetailSet.getString("Milestone_Description"), mlstnDetailSet.getString("MilestoneOwner"), mlstnDetailSet.getTimestamp("Start_Timestamp"), mlstnDetailSet.getTimestamp("Due_Timestamp"), GlobalNotations.PRIORITY_MAPPING.get(mlstnDetailSet.getInt("Priority")), GlobalNotations.STATUS_MAPPING.get(mlstnDetailSet.getInt("Status"))));
			}
				
			mergedTaskListPrepObj.setInt(1, selectedMilestoneID);
			mergedTaskListPrepObj.setInt(2, pinnedProjectID);
			
			ResultSet mergedTaskSet = mergedTaskListPrepObj.executeQuery();
			while(mergedTaskSet.next())
			{
				if(mergedTaskSet.getBoolean("IsAssociated"))
					mlstnTaskMap.put(new TaskEntity(mergedTaskSet.getString("Task_Name"), mergedTaskSet.getString("Description"), mergedTaskSet.getInt("Task_ID"), mergedTaskSet.getString("Owner"), mergedTaskSet.getString("Assignee"), mergedTaskSet.getTimestamp("Start_Timestamp"), mergedTaskSet.getTimestamp("Due_Timestamp"),  GlobalNotations.PRIORITY_MAPPING.get(mergedTaskSet.getInt("Priority")), GlobalNotations.STATUS_MAPPING.get(mergedTaskSet.getInt("Status"))), true);
				else
					mlstnTaskMap.put(new TaskEntity(mergedTaskSet.getString("Task_Name"), mergedTaskSet.getString("Description"), mergedTaskSet.getInt("Task_ID"), mergedTaskSet.getString("Owner"), mergedTaskSet.getString("Assignee"), mergedTaskSet.getTimestamp("Start_Timestamp"), mergedTaskSet.getTimestamp("Due_Timestamp"),  GlobalNotations.PRIORITY_MAPPING.get(mergedTaskSet.getInt("Priority")), GlobalNotations.STATUS_MAPPING.get(mergedTaskSet.getInt("Status"))), false);
			}
		}
		catch(SQLException connectExcep)
		{
			connectExcep.printStackTrace();
		}
		
		requestedContent = MILESTONE_DETAILS;
		return SUCCESS;
	}
	
	public String getMilestoneTasks()
	{
		try(Connection dbConnectionObj = DBManager.getDBConnectorInst();)
		{
			try(
					PreparedStatement taskListPrepObj = dbConnectionObj.prepareStatement("SELECT task.*, owner.Username AS TaskOwner, assignee.Username AS TaskAssignee FROM Task task JOIN Users owner ON task.Owner=owner.User_ID JOIN Users assignee ON task.Assignee=assignee.User_ID WHERE Milestone_ID=?;"); 
				)
			{
				JSONArray taskObjJSNArray = new JSONArray();
				JSONObject taskListJSNObj = new JSONObject();
				taskListPrepObj.setInt(1, getSelectedMilestoneID());
				ResultSet taskListSet = taskListPrepObj.executeQuery();
				while(taskListSet.next())
				{
					JSONObject taskJSNObj = new JSONObject();
					taskJSNObj.put("TaskName", taskListSet.getString("Task_Name"));
					taskJSNObj.put("Owner", taskListSet.getString("TaskOwner"));
					taskJSNObj.put("StartDate", formatToCustomDate(taskListSet.getTimestamp("Start_Timestamp")));
					taskJSNObj.put("DueDate", formatToCustomDate(taskListSet.getTimestamp("Due_Timestamp")));
					taskJSNObj.put("Assignee", taskListSet.getString("TaskAssignee"));
					taskJSNObj.put("Priority", PRIORITY_MAPPING.get(taskListSet.getInt("Priority")));
					taskJSNObj.put("Status", STATUS_MAPPING.get(taskListSet.getInt("Status")));
					taskObjJSNArray.add(taskJSNObj);
				}
				taskListJSNObj.put("TaskArray", taskObjJSNArray);
				
				
				//Writing it in response
				HttpServletResponse responseObj =  ServletActionContext.getResponse();
				responseObj.setContentType("application/json");
				try
				(PrintWriter writerObj = responseObj.getWriter();)
				{
					writerObj.write(taskListJSNObj.toJSONString());
				}
				catch(IOException writerExcep) 
				{
					writerExcep.printStackTrace();
				}
			}
			catch(SQLException stateObj)
			{
				stateObj.printStackTrace();
			}
		}
		catch(SQLException connExcep)
		{
			connExcep.printStackTrace();
		}
		return SUCCESS;
	}
	
	/*
	 * For deleting task mapping from milestones
	 */
	private MilestoneAction deleteTaskMapping(Connection dbConnectionObj, int milestone_ID) throws SQLException
	{
		PreparedStatement dscnctTaskPrepObj = dbConnectionObj.prepareStatement("UPDATE Task SET Milestone_ID=NULL WHERE Milestone_ID=?");
		
		//Disconnecting all Tasks related to this Milestone
		dscnctTaskPrepObj.setInt(1, milestone_ID);
		dscnctTaskPrepObj.executeUpdate();
		
		return this;
	}
	
	private MilestoneAction appendTaskMapping(Connection dbConnectionObj, int milestoneID, String[] taskStrIDArray) throws SQLException
	{
		
		PreparedStatement updateMlstnIDfrTaskPrepObj = dbConnectionObj.prepareStatement("UPDATE Task SET Milestone_ID=? WHERE Task_ID=?");
	
		//Updating Task Milestone_ID
		if(!crtNewMlstnTasksID.isEmpty())
		{
			for(String taskID: taskStrIDArray)
			{
				updateMlstnIDfrTaskPrepObj.setInt(1, milestoneID);
				updateMlstnIDfrTaskPrepObj.setInt(2, Integer.parseInt(taskID));
				updateMlstnIDfrTaskPrepObj.executeUpdate();
			}
		}
		
		return this;
	}
	
	private MilestoneAction appendMilestoneAttachments(Connection dbConnectionObj, int milestone_ID, File[] fileArr, String[] fileNames) throws SQLException
	{
		PreparedStatement atchmntApndPrepObj = dbConnectionObj.prepareStatement("INSERT INTO Attachments(Milestone_ID, File, File_Name, Uploaded_Time) Values(?, ?, ?, now());");
		
		if(fileArr!=null)
		{
			for(int curFileIndex=0; curFileIndex<fileArr.length; curFileIndex++)
			{
				//Inserting File as BLOB
				atchmntApndPrepObj.setInt(1, milestone_ID);
				atchmntApndPrepObj.setString(3, fileNames[curFileIndex]);
				
				try(FileInputStream ipStream = new FileInputStream(fileArr[curFileIndex]))
				{
					atchmntApndPrepObj.setBinaryStream(2, ipStream, (int)fileArr[curFileIndex].length());
					atchmntApndPrepObj.executeUpdate();
				}
				catch(Exception excep)
				{
					excep.printStackTrace();
				}
			}
		}
		
		return this;
	}
	
	/*
	 * For Editing Existing Milestone
	 */
	private MilestoneAction addMilestoneData(Connection dbConnectionObj, int milestone_ID) throws SQLException
	{
		PreparedStatement mlstnApndPrepObj = dbConnectionObj.prepareStatement("UPDATE Milestones SET Milestone_Name=?, Milestone_Description=?, Priority=?, Status=?, Start_Timestamp=?, Due_Timestamp=? WHERE Milestone_ID=?;");
		
		mlstnApndPrepObj.setString(1, crtNewMlstnName);
		mlstnApndPrepObj.setString(2, crtNewMlstnDescription);
		mlstnApndPrepObj.setInt(3, crtNewMlstnPriority);
		mlstnApndPrepObj.setInt(4, crtNewMlstnStatus);
		mlstnApndPrepObj.setTimestamp(5, Timestamp.valueOf(crtNewMlstnFrmDate+GlobalNotations.TIMESTAMP_SUFFIX));
		mlstnApndPrepObj.setTimestamp(6, Timestamp.valueOf(crtNewMlstnToDate+GlobalNotations.TIMESTAMP_SUFFIX));	
		mlstnApndPrepObj.setInt(7, selectedMilestoneID);
		
		mlstnApndPrepObj.executeUpdate();
		
		return this;
	}
	
	/*
	 * For Adding New Project
	 */
	private MilestoneAction addMilestoneData(Connection dbConnectionObj) throws SQLException
	{
		PreparedStatement mlstnApndPrepObj = dbConnectionObj.prepareStatement("INSERT INTO Milestones(Milestone_Name, Milestone_Description, Owner, Project_ID, Creation_Timestamp, Start_Timestamp, Due_Timestamp, Priority, Status) VALUES(?, ?, ?, ?, now(), ?, ?, ?, ?);");
		
		mlstnApndPrepObj.setString(1, crtNewMlstnName);
		mlstnApndPrepObj.setString(2, crtNewMlstnDescription);
		mlstnApndPrepObj.setInt(3, (int)getSession().get(SK_LOGGED_USERID));
		mlstnApndPrepObj.setInt(4, selectedProjectID);
		mlstnApndPrepObj.setTimestamp(5, Timestamp.valueOf(crtNewMlstnFrmDate+GlobalNotations.TIMESTAMP_SUFFIX));
		mlstnApndPrepObj.setTimestamp(6, Timestamp.valueOf(crtNewMlstnToDate+GlobalNotations.TIMESTAMP_SUFFIX));	
		mlstnApndPrepObj.setInt(7, crtNewMlstnPriority);
		mlstnApndPrepObj.setInt(8, crtNewMlstnStatus);
		
		mlstnApndPrepObj.executeUpdate();
		
		setLastInsertID(getLastDMInsertID(dbConnectionObj));
		
		return this;
	}
	
	/*
	 * For getting last insert ID of added project
	 */
	private int getLastDMInsertID(Connection dbConnectionObj) throws SQLException
	{
		PreparedStatement appndPrjctIDPrepObj = dbConnectionObj.prepareStatement("SELECT LAST_INSERT_ID();");
		ResultSet prjctIDSet = appndPrjctIDPrepObj.executeQuery();
		prjctIDSet.next();
		int apndPrjctID = prjctIDSet.getInt("LAST_INSERT_ID()");
		
		return apndPrjctID;
	}
	
	public void addNewMilestone()
	{
		try(Connection dbConnectionObj = DBManager.getDBConnectorInst();)
		{
			try
			{
				addMilestoneData(dbConnectionObj)
					.appendTaskMapping(dbConnectionObj, lastInsertID, crtNewMlstnTasksID.split(","))
					.appendMilestoneAttachments(dbConnectionObj, lastInsertID, crtNewMlstnFiles, crtNewMlstnFilesName);
				
				dbConnectionObj.commit();
			}
			catch(SQLException dmException)
			{
				dbConnectionObj.rollback();
				dmException.printStackTrace();
			}
		}
		catch(SQLException connectExcep)
		{
			connectExcep.printStackTrace();
		}
	}
	
	public void editMilestone()
	{
		try(Connection dbConnectionObj = DBManager.getDBConnectorInst();)
		{
			try
			{
				addMilestoneData(dbConnectionObj, selectedMilestoneID)
					.deleteTaskMapping(dbConnectionObj, selectedMilestoneID)
					.appendTaskMapping(dbConnectionObj, selectedMilestoneID, crtNewMlstnTasksID.split(","))
					.appendMilestoneAttachments(dbConnectionObj, selectedMilestoneID, crtNewMlstnFiles, crtNewMlstnFilesName);
				
				dbConnectionObj.commit();
			}
			catch(SQLException dmException)
			{
				dbConnectionObj.rollback();
				dmException.printStackTrace();
			}
			
			//Getting Associated Project ID to send it to front end 
			int assctdProjectID = 0;
			try(PreparedStatement taskDetPrepObj = dbConnectionObj.prepareStatement("SELECT Project_ID FROM Milestones WHERE Milestone_ID=?;"))
			{
				taskDetPrepObj.setInt(1, selectedMilestoneID);
				ResultSet taskDetSet = taskDetPrepObj.executeQuery();
				taskDetSet.next();
				
				assctdProjectID = taskDetSet.getInt("Project_ID");
			}
			
			//Writing JSON Response for ProjectID
			JSONObject retJSONObj = new JSONObject();
			retJSONObj.put("AssociatedProjectID", assctdProjectID);
			
			HttpServletResponse reponseObj = ServletActionContext.getResponse();
			reponseObj.setContentType("application/json");
			try(PrintWriter writerObj = reponseObj.getWriter())
			{
				writerObj.write(retJSONObj.toJSONString());
			}
			catch(IOException ioExcep)
			{
				ioExcep.printStackTrace();
			}
		}
		catch(SQLException connectExcep)
		{
			connectExcep.printStackTrace();
		}
	}
}
