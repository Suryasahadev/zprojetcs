package org.zprojects.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.zprojects.modal.ProjectEntity;
import org.zprojects.modal.TaskEntity;
import org.zprojects.modal.MilestoneEntity;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONObject;
import org.zprojects.connector.DBManager;
import org.zprojects.container.GlobalNotations;
import org.zprojects.container.SessionContainer;

import com.opensymphony.xwork2.ActionSupport;

public class TaskAction extends ActionSupport implements SessionContainer, GlobalNotations
{
	//For setting user requested content
	private String requestedContent = "";
		
	//Global values
	private String crtNewTaskName = "";
	private String crtNewTaskProjectName = "";
	private int crtNewTaskProjectID = 0;
	private int crtNewTaskMlstnID = 0;
	private String crtNewTaskDescription = "";
	private String crtNewTaskOwner = "";
	private String crtNewTaskAssignee = "";
	private String crtNewTaskFrmDate = "";
	private String crtNewTaskToDate = "";
	private int crtNewTaskPriority = 0;
	private int crtNewTaskStatus = 0;
	private String crtNewTaskTags = "";
	private File[] crtNewTaskFiles;
	private String[] crtNewTaskFilesName;
	private TaskEntity newTaskEntity = null;
	private TaskEntity detailingTaskEntityObj = null;
	Map<MilestoneEntity, Boolean> taskMlstnMap = new HashMap<MilestoneEntity, Boolean>();	
	
	private int selectedProjectID = 0;
	private int selectedTaskID = 0;
	
	//Project DM Variables
	private int lastInsertID = 0;
	
	//List which will be Used in View
	List<TaskEntity> taskList = new ArrayList<TaskEntity>();
		
	public String getRequestedContent() {
		return requestedContent;
	}
	public void setRequestedContent(String requestedContent) {
		this.requestedContent = requestedContent;
	}
	public String getCrtNewTaskName() {
		return crtNewTaskName;
	}
	public void setCrtNewTaskName(String crtNewTaskName) {
		this.crtNewTaskName = crtNewTaskName;
	}
	public String getCrtNewTaskProjectName() {
		return crtNewTaskProjectName;
	}
	public void setCrtNewTaskProjectName(String crtNewTaskProjectName) {
		this.crtNewTaskProjectName = crtNewTaskProjectName;
	}
	public int getCrtNewTaskProjectID() {
		return crtNewTaskProjectID;
	}
	public void setCrtNewTaskProjectID(int crtNewTaskProjectID) {
		this.crtNewTaskProjectID = crtNewTaskProjectID;
	}
	public int getCrtNewTaskMlstnID() {
		return crtNewTaskMlstnID;
	}
	public void setCrtNewTaskMlstnID(int crtNewTaskMlstnID) {
		this.crtNewTaskMlstnID = crtNewTaskMlstnID;
	}
	public String getCrtNewTaskDescription() {
		return crtNewTaskDescription;
	}
	public void setCrtNewTaskDescription(String crtNewTaskDescription) {
		this.crtNewTaskDescription = crtNewTaskDescription;
	}
	public String getCrtNewTaskOwner() {
		return crtNewTaskOwner;
	}
	public void setCrtNewTaskOwner(String crtNewTaskOwner) {
		this.crtNewTaskOwner = crtNewTaskOwner;
	}
	public String getCrtNewTaskAssignee() {
		return crtNewTaskAssignee;
	}
	public void setCrtNewTaskAssignee(String crtNewTaskAssignee) {
		this.crtNewTaskAssignee = crtNewTaskAssignee;
	}
	public String getCrtNewTaskFrmDate() {
		return crtNewTaskFrmDate;
	}
	public void setCrtNewTaskFrmDate(String crtNewTaskFrmDate) {
		this.crtNewTaskFrmDate = crtNewTaskFrmDate;
	}
	public String getCrtNewTaskToDate() {
		return crtNewTaskToDate;
	}
	public void setCrtNewTaskToDate(String crtNewTaskToDate) {
		this.crtNewTaskToDate = crtNewTaskToDate;
	}
	public int getCrtNewTaskPriority() {
		return crtNewTaskPriority;
	}
	public void setCrtNewTaskPriority(int crtNewTaskPriority) {
		this.crtNewTaskPriority = crtNewTaskPriority;
	}
	public int getCrtNewTaskStatus() {
		return crtNewTaskStatus;
	}
	public void setCrtNewTaskStatus(int crtNewTaskStatus) {
		this.crtNewTaskStatus = crtNewTaskStatus;
	}
	public String getCrtNewTaskTags() {
		return crtNewTaskTags;
	}
	public void setCrtNewTaskTags(String crtNewTaskTags) {
		this.crtNewTaskTags = crtNewTaskTags;
	}
	public File[] getCrtNewTaskFiles() {
		return crtNewTaskFiles;
	}
	public void setCrtNewTaskFiles(File[] crtNewTaskFiles) {
		this.crtNewTaskFiles = crtNewTaskFiles;
	}
	public String[] getCrtNewTaskFilesName() {
		return crtNewTaskFilesName;
	}
	public void setCrtNewTaskFilesName(String[] crtNewTaskFilesName) {
		this.crtNewTaskFilesName = crtNewTaskFilesName;
	}
	public TaskEntity getNewTaskEntity() {
		return newTaskEntity;
	}
	public void setNewTaskEntity(TaskEntity newTaskEntity) {
		this.newTaskEntity = newTaskEntity;
	}	
	public TaskEntity getDetailingTaskEntityObj() {
		return detailingTaskEntityObj;
	}
	public void setDetailingTaskEntityObj(TaskEntity detailingTaskEntityObj) {
		this.detailingTaskEntityObj = detailingTaskEntityObj;
	}
	public Map<MilestoneEntity, Boolean> getTaskMlstnMap() {
		return taskMlstnMap;
	}
	public void setTaskMlstnMap(Map<MilestoneEntity, Boolean> taskMlstnMap) {
		this.taskMlstnMap = taskMlstnMap;
	}
	public int getSelectedProjectID() {
		return selectedProjectID;
	}
	public void setSelectedProjectID(int selectedProjectID) {
		this.selectedProjectID = selectedProjectID;
	}
	public int getSelectedTaskID() {
		return selectedTaskID;
	}
	public void setSelectedTaskID(int selectedTaskID) {
		this.selectedTaskID = selectedTaskID;
	}
	
	//Project DM Getters and Setters
	public int getLastInsertID() {
		return lastInsertID;
	}
	public void setLastInsertID(int lastInsertID) {
		this.lastInsertID = lastInsertID;
	}
	
	public List<TaskEntity> getTaskList() {
		return taskList;
	}
	public void setTaskList(List<TaskEntity> taskList) {
		this.taskList = taskList;
	}
	
	private void initTaskList(String query, int constraintID)
	{
		try(
			Connection connectionObj = DBManager.getDBConnectorInst();
			PreparedStatement taskLstPrepObj = connectionObj.prepareStatement(query);
			)
		{
			taskLstPrepObj.setInt(1, constraintID);
			ResultSet taskDetSet = taskLstPrepObj.executeQuery();
			while(taskDetSet.next())
		    {
				taskList.add(new TaskEntity(taskDetSet.getString("Task_Name"), taskDetSet.getString("Description"), taskDetSet.getInt("Task_ID"), taskDetSet.getString("TaskOwner"), taskDetSet.getString("TaskAssignee"), taskDetSet.getTimestamp("Start_Timestamp"), taskDetSet.getTimestamp("Due_Timestamp"),  GlobalNotations.PRIORITY_MAPPING.get(taskDetSet.getInt("Priority")), GlobalNotations.STATUS_MAPPING.get(taskDetSet.getInt("Status"))));
		    }
		}
		catch (SQLException queryExcep) 
		{
			// TODO Auto-generated catch block
			queryExcep.printStackTrace();
		}
	}
	
	public String getCreatedTasks()
	{
		String taskPreQuery = "SELECT task.*, owner.Username AS TaskOwner, assignee.Username AS TaskAssignee FROM Task task LEFT JOIN Users owner ON task.Owner=owner.User_ID LEFT JOIN Users assignee ON task.Assignee=assignee.User_ID where task.Owner=? ORDER BY Creation_Timestamp DESC;";
		initTaskList(taskPreQuery, (int)getSession().get(SK_LOGGED_USERID));
		
		requestedContent = CREATED_TASKS;
		return SUCCESS;
	}
	
	public String getAssignedTasks()
	{
		String taskPreQuery = "SELECT task.*, owner.Username AS TaskOwner, assignee.Username AS TaskAssignee FROM Task task LEFT JOIN Users owner ON task.Owner=owner.User_ID LEFT JOIN Users assignee ON task.Assignee=assignee.User_ID where task.Assignee=? ORDER BY Creation_Timestamp DESC;";
		initTaskList(taskPreQuery, (int)getSession().get(SK_LOGGED_USERID));
				
		requestedContent = ASSIGNED_TASKS;
		return SUCCESS;
	}
	
	public String getProjectTasks()
	{
		String taskPreQuery = "SELECT task.*, owner.Username AS TaskOwner, assignee.Username AS TaskAssignee FROM Task task LEFT JOIN Users owner ON task.Owner=owner.User_ID LEFT JOIN Users assignee ON task.Assignee=assignee.User_ID where task.Project_ID=? ORDER BY Creation_Timestamp DESC;";
		initTaskList(taskPreQuery, selectedProjectID);
				
		requestedContent = PROJECT_TASKS;
		return SUCCESS;
	}
	
	public String getTaskDetails()
	{
		try
		(
			Connection connectionObj = DBManager.getDBConnectorInst();
			PreparedStatement taskDetPrepObj = connectionObj.prepareStatement("SELECT task.*, owner.Username AS TaskOwner, assignee.Username AS TaskAssignee FROM Task task LEFT JOIN Users owner ON task.Owner=owner.User_ID LEFT JOIN Users assignee ON task.Assignee=assignee.User_ID where Task_ID=?;");
			PreparedStatement mergedMlstnListPrepObj = connectionObj.prepareStatement("SELECT milestones.*, IF(milestones.Milestone_ID=task.Milestone_ID, 1, 0) AS IsAssociated From Milestones milestones JOIN Task task WHERE milestones.Project_ID=? AND task.Task_ID=?;");
		)
		{
			int pinnedProjectID = 0;
			taskDetPrepObj.setInt(1, selectedTaskID);
			
			ResultSet taskDetailSet = taskDetPrepObj.executeQuery();
			if(taskDetailSet.next())
			{
				pinnedProjectID = taskDetailSet.getInt("Project_ID");
				setDetailingTaskEntityObj(new TaskEntity(taskDetailSet.getString("Task_Name"), taskDetailSet.getString("Description"), taskDetailSet.getInt("Task_ID"), taskDetailSet.getString("TaskOwner"), taskDetailSet.getString("TaskAssignee"), taskDetailSet.getTimestamp("Start_Timestamp"), taskDetailSet.getTimestamp("Due_Timestamp"),  GlobalNotations.PRIORITY_MAPPING.get(taskDetailSet.getInt("Priority")), GlobalNotations.STATUS_MAPPING.get(taskDetailSet.getInt("Status"))));
			}
			
			mergedMlstnListPrepObj.setInt(1, pinnedProjectID);
			mergedMlstnListPrepObj.setInt(2, selectedTaskID);
			
			ResultSet mergedMlstnSet = mergedMlstnListPrepObj.executeQuery();
			while(mergedMlstnSet.next())
			{
				if(mergedMlstnSet.getBoolean("IsAssociated"))
					taskMlstnMap.put(new MilestoneEntity(mergedMlstnSet.getInt("Milestone_ID"), mergedMlstnSet.getString("Milestone_Name"), mergedMlstnSet.getString("Milestone_Description"), mergedMlstnSet.getString("Owner"), mergedMlstnSet.getTimestamp("Start_Timestamp"), mergedMlstnSet.getTimestamp("Due_Timestamp"), GlobalNotations.PRIORITY_MAPPING.get(mergedMlstnSet.getInt("Priority")), GlobalNotations.STATUS_MAPPING.get(mergedMlstnSet.getInt("Status"))), true);
				else
					taskMlstnMap.put(new MilestoneEntity(mergedMlstnSet.getInt("Milestone_ID"), mergedMlstnSet.getString("Milestone_Name"), mergedMlstnSet.getString("Milestone_Description"), mergedMlstnSet.getString("Owner"), mergedMlstnSet.getTimestamp("Start_Timestamp"), mergedMlstnSet.getTimestamp("Due_Timestamp"), GlobalNotations.PRIORITY_MAPPING.get(mergedMlstnSet.getInt("Priority")), GlobalNotations.STATUS_MAPPING.get(mergedMlstnSet.getInt("Status"))), false);
			}
		}
		catch(SQLException connectExcep)
		{
			connectExcep.printStackTrace();
		}
		requestedContent = TASK_DETAILS;
		return SUCCESS;
	}
	
	//Common Operations for Task DM's
	
	/*
	 * For getting User_ID as List with given UserNames in crtNewProjectMembers
	 */
	private int getAssigneeId()
	{
		int assigneeID = 0;
		try(Connection dbConnectionObj = DBManager.getDBConnectorInst();)
		{
			try(
					PreparedStatement userAccPrepObj = dbConnectionObj.prepareStatement("SELECT User_ID FROM USERS WHERE Username=?"); 
				)
			{
				if(!crtNewTaskAssignee.isEmpty())
				{
					userAccPrepObj.setString(1, crtNewTaskAssignee);
					ResultSet assighneeSet = userAccPrepObj.executeQuery();
					
					if(assighneeSet.next())
						assigneeID = assighneeSet.getInt("User_ID");
				}
			}
			catch(SQLException stateObjExcep)
			{
				stateObjExcep.printStackTrace();
			}
		}
		catch(SQLException connectExcep)
		{
			connectExcep.printStackTrace();
		}
			
		return assigneeID;
	}
	
	/*
	 * For Editing Existing Task
	 */
	private TaskAction addTaskData(Connection dbConnectionObj, int task_ID) throws SQLException
	{
		PreparedStatement taskUpdtPrepObj = dbConnectionObj.prepareStatement("UPDATE Task SET Task_Name=?, Description=?, Milestone_ID=?, Assignee=?, Priority=?, Status=?, Start_Timestamp=?, Due_Timestamp=? WHERE Task_ID=?;");
		
		taskUpdtPrepObj.setString(1, crtNewTaskName);
		taskUpdtPrepObj.setString(2, crtNewTaskDescription);
		
		//Setting Milsetone ID or Null if No Milestone have been Selected
		if(crtNewTaskMlstnID!=0)
			taskUpdtPrepObj.setInt(3, crtNewTaskMlstnID);
		else
			taskUpdtPrepObj.setNull(3, java.sql.Types.NULL);
		
		//Getting User_ID from assignee name if 0 entering NULL
		int assignee_ID = getAssigneeId();
		
		if(assignee_ID!=0)
			taskUpdtPrepObj.setInt(4, assignee_ID);
		else
			taskUpdtPrepObj.setNull(4, java.sql.Types.NULL);
		
		taskUpdtPrepObj.setInt(5,crtNewTaskPriority);
		taskUpdtPrepObj.setInt(6,crtNewTaskStatus);
		taskUpdtPrepObj.setTimestamp(7, Timestamp.valueOf(crtNewTaskFrmDate+GlobalNotations.TIMESTAMP_SUFFIX));
		taskUpdtPrepObj.setTimestamp(8, Timestamp.valueOf(crtNewTaskToDate+GlobalNotations.TIMESTAMP_SUFFIX));
		taskUpdtPrepObj.setInt(9, selectedTaskID);
		
		taskUpdtPrepObj.executeUpdate();
		
		return this;
	}
	
	/*
	 * For Adding New Project
	 */
	private TaskAction addTaskData(Connection dbConnectionObj) throws SQLException
	{
		PreparedStatement taskApndPrepObj = dbConnectionObj.prepareStatement("INSERT INTO Task(Task_Name, Description, Project_ID, Milestone_ID, Owner, Assignee, Priority, Status, Creation_Timestamp, Start_Timestamp, Due_Timestamp) VALUES(?, ?, ?, ?, ?, ?, ?, ?, now(), ?, ?);");
		
		taskApndPrepObj.setString(1, crtNewTaskName);
		taskApndPrepObj.setString(2, crtNewTaskDescription);
		taskApndPrepObj.setInt(3, selectedProjectID);
		
		//Setting Milsetone ID or Null if No Milestone have been Selected
		if(crtNewTaskMlstnID!=0)
			taskApndPrepObj.setInt(4, crtNewTaskMlstnID);
		else
			taskApndPrepObj.setNull(4, java.sql.Types.NULL);
		
		taskApndPrepObj.setInt(5, (int)getSession().get(SK_LOGGED_USERID));
		
		//Getting User_ID from assignee name if 0 entering NULL
		int assignee_ID = getAssigneeId();
		
		if(assignee_ID!=0)
			taskApndPrepObj.setInt(6, assignee_ID);
		else
			taskApndPrepObj.setNull(6, java.sql.Types.NULL);
		
		taskApndPrepObj.setInt(7,crtNewTaskPriority);
		taskApndPrepObj.setInt(8,crtNewTaskStatus);
		taskApndPrepObj.setTimestamp(9, Timestamp.valueOf(crtNewTaskFrmDate+GlobalNotations.TIMESTAMP_SUFFIX));
		taskApndPrepObj.setTimestamp(10, Timestamp.valueOf(crtNewTaskToDate+GlobalNotations.TIMESTAMP_SUFFIX));
		
		taskApndPrepObj.executeUpdate();
		
		setLastInsertID(getLastDMInsertID(dbConnectionObj));
		
		return this;
	}
	
	/*
	 * For getting last insert ID of added project
	 */
	private int getLastDMInsertID(Connection dbConnectionObj) throws SQLException
	{
		PreparedStatement appndPrjctIDPrepObj = dbConnectionObj.prepareStatement("SELECT LAST_INSERT_ID();");
		ResultSet prjctIDSet = appndPrjctIDPrepObj.executeQuery();
		prjctIDSet.next();
		int apndPrjctID = prjctIDSet.getInt("LAST_INSERT_ID()");
		
		return apndPrjctID;
	}
	
	/*
	 * For Appending files to Project
	 */
	private TaskAction appendTaskAttachments(Connection dbConnectionObj, int task_ID, File[] fileArr, String[] fileNames) throws SQLException
	{
		PreparedStatement atchmntApndPrepObj = dbConnectionObj.prepareStatement("INSERT INTO Attachments(Task_ID, File, File_Name, Uploaded_Time) Values(?, ?, ?, now());");
		
		if(fileArr!=null)
		{
			for(int curFileIndex=0; curFileIndex<fileArr.length; curFileIndex++)
			{
				//Inserting File as BLOB
				atchmntApndPrepObj.setInt(1, task_ID);
				atchmntApndPrepObj.setString(3, fileNames[curFileIndex]);
				
				try(FileInputStream ipStream = new FileInputStream(fileArr[curFileIndex]))
				{
					atchmntApndPrepObj.setBinaryStream(2, ipStream, (int)fileArr[curFileIndex].length());
					atchmntApndPrepObj.executeUpdate();
				}
				catch(Exception excep)
				{
					excep.printStackTrace();
				}
			}
		}
		
		return this;
	}
	
	//For Adding New Project
	public void addNewTask()
	{
		//Design Pattern Approach to achieve Project Adding.
		try(Connection dbConnectionObj = DBManager.getDBConnectorInst();)
		{
			try
			{
				addTaskData(dbConnectionObj)
					.appendTaskAttachments(dbConnectionObj, lastInsertID, crtNewTaskFiles, crtNewTaskFilesName);
				
				dbConnectionObj.commit();
			}
			catch(SQLException dmException)
			{
				dbConnectionObj.rollback();
				dmException.printStackTrace();
			}
		}
		catch(SQLException connectExcep)
		{
			connectExcep.printStackTrace();
		}
	}
		
	//For Editing Project
	public void editTask()
	{
		try(Connection dbConnectionObj = DBManager.getDBConnectorInst();)
		{
			try
			{
				addTaskData(dbConnectionObj, selectedTaskID)
					.appendTaskAttachments(dbConnectionObj, selectedTaskID, crtNewTaskFiles, crtNewTaskFilesName);
			
				dbConnectionObj.commit();
			}
			catch(SQLException dmException)
			{
				dbConnectionObj.rollback();
				dmException.printStackTrace();
			}
			
			//Getting Associated Project ID to send it to front end 
			int assctdProjectID = 0;
			try(PreparedStatement taskDetPrepObj = dbConnectionObj.prepareStatement("SELECT Project_ID FROM Task WHERE Task_ID=?;"))
			{
				taskDetPrepObj.setInt(1, selectedTaskID);
				ResultSet taskDetSet = taskDetPrepObj.executeQuery();
				taskDetSet.next();
				
				assctdProjectID = taskDetSet.getInt("Project_ID");
			}
			
			//Writing JSON Response for ProjectID
			JSONObject retJSONObj = new JSONObject();
			retJSONObj.put("AssociatedProjectID", assctdProjectID);
			
			HttpServletResponse reponseObj = ServletActionContext.getResponse();
			reponseObj.setContentType("application/json");
			try(PrintWriter writerObj = reponseObj.getWriter())
			{
				writerObj.write(retJSONObj.toJSONString());
			}
			catch(IOException ioExcep)
			{
				ioExcep.printStackTrace();
			}
		}
		catch(SQLException connectExcep)
		{
			connectExcep.printStackTrace();
		}
	}
}
