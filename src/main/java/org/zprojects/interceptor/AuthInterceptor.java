package org.zprojects.interceptor;

import org.zprojects.container.GlobalNotations;
import org.zprojects.container.SessionContainer;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class AuthInterceptor implements Interceptor, SessionContainer, GlobalNotations
{
	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String intercept(ActionInvocation actionInvocation) throws Exception 
	{
		if(getSession().get(SK_USER_LOGGED_IN)!=null && (Boolean)getSession().get(SK_USER_LOGGED_IN))
		{
			//Redirecting to Home Breach
			getSession().put(SK_AUTH_BREACH, true);
			return DIRECT_AUTH_HIT;
		}
		else
		{
			return actionInvocation.invoke();
		}
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
}