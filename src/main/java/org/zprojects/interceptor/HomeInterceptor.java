package org.zprojects.interceptor;
 
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import org.zprojects.container.GlobalNotations;
import org.zprojects.container.SessionContainer;

import com.opensymphony.xwork2.Action;

public class HomeInterceptor implements Interceptor, SessionContainer, GlobalNotations
{
	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String intercept(ActionInvocation actionInvocation) throws Exception 
	{
		if(getSession().get(SK_USER_LOGGED_IN)!=null && (Boolean)getSession().get(SK_USER_LOGGED_IN))
		{
			return actionInvocation.invoke();
		}
		else
		{
			getSession().put(SK_HOME_BREACH, true);
			return DIRECT_HIT;
		}
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
}

	
