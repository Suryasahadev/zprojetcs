package org.zprojects.connector;

import java.util.Properties;
import java.util.Random;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.zprojects.container.GlobalNotations;
import org.zprojects.container.SessionContainer;

import com.opensymphony.xwork2.ActionContext;

public class MailGenerator implements GlobalNotations, SessionContainer
{
	//Singleton Object for MailGenerator class
	private static MailGenerator mailGeneratorInst = null;
	
	private static Session mailSessionObj = null;
	private static Properties systemPropertiesObj = System.getProperties(); 
	
	//Creating only one instance if NULL
	public static void initMailGenratorInst()
	{
		if(mailGeneratorInst==null)
			new MailGenerator();
	}
	//Constructor initiating Mail Session Object
	private MailGenerator()
	{
		mailSessionObj = Session.getInstance(systemPropertiesObj, new Authenticator() 
		{
            @Override
            protected PasswordAuthentication getPasswordAuthentication()
            {
            	return new PasswordAuthentication(systemPropertiesObj.getProperty(SENDER_MAILID), systemPropertiesObj.getProperty(SENDER_MAIL_APP_PASSWORD));
            }
        });
	}
	private static int generateOTP()
	{
		//Generating a prefix value in thousands.
		int thsndValue = new Random().nextInt(9)+1;
        thsndValue = thsndValue*1000;
        
        //Appending Thousand'th value with generated new number.
        int otpValue = (new Random().nextInt(999)+1)+thsndValue;
        
		return otpValue;
	}
	public static void sendOTPMail(String userName, String mailID)
	{
		//Vanishing the last OTP
		ActionContext.getContext().put(CURRENTLY_GENERATED_OTP, 0);
		
		int generatedOTP = generateOTP();
		
		String otpTemplateString = 
				"<div style=\"font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2\">\n"
				+ "  <div style=\"margin:50px auto;width:70%;padding:20px 0\">\n"
				+ "    <div style=\"border-bottom:1px solid #eee\">\n"
				+ "      <a href=\"\" style=\"font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600\">ZProjects Authentication</a>\n"
				+ "    </div>\n"
				+ "    <p style=\"font-size:1.1em\">Hi "+userName+",</p>\n"
				+ "    <p>Use the following OTP to complete your Sign Up process for ZProjects</p>\n"
				+ "    <h2 style=\"background: #00466a;margin: 0 auto;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;\">"+generatedOTP+"</h2>\n"
				+ "    <p style=\"font-size:0.9em;\">Regards,<br />ZProjects</p>\n"
				+ "    <hr style=\"border:none;border-top:1px solid #eee\" />\n"
				+ "    <div style=\"float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300\">\n"
				+ "      <p>ZProjects Inc</p>\n"
				+ "      <p>Zoho Corporation</p>\n"
				+ "      <p>Chennai</p>\n"
				+ "    </div>\n"
				+ "  </div>\n"
				+ "</div>";
		
		try
        {
            Message messageObj = new MimeMessage(mailSessionObj);
            messageObj.addRecipient(Message.RecipientType.TO, new InternetAddress(mailID));
            messageObj.setSubject("ZProjects - Signup Authentication");
            messageObj.setContent(otpTemplateString, "text/html");
            Transport.send(messageObj);
            
            //Adding the currently generated OTP
            ActionContext.getContext().getSession().put(CURRENTLY_GENERATED_OTP, generatedOTP);
        }
        catch (Exception excep)
        {
            excep.printStackTrace();
        }
	}
}
