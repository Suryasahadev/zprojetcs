package org.zprojects.connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBManager 
{
	private static final String CONNECTION_URL="jdbc:mysql://localhost:3306/TMS_Web_DB";
	private String userName = "";
	private String password = ""; 
	
	public DBManager()
	{
		this.userName = System.getProperty("DB_Username");
		this.password = System.getProperty("DB_Password");
	}
	public static Connection getDBConnectorInst() throws SQLException
	{
		Connection actConnection = new DBManager().instantiateConnection();
		actConnection.setAutoCommit(false);
		return actConnection;
	}
	private Connection instantiateConnection() throws SQLException
	{
		return DriverManager.getConnection(CONNECTION_URL, userName, password);
	}
}
