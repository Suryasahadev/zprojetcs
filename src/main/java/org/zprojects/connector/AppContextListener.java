package org.zprojects.connector;

import java.util.Map;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.struts2.interceptor.SessionAware;
import org.zprojects.container.GlobalNotations;

public class AppContextListener implements ServletContextListener, GlobalNotations
{
	@Override
	public void contextInitialized(ServletContextEvent cntxtEvent)
	{
		//Setting DB Related Values
		System.setProperty("DB_Username", cntxtEvent.getServletContext().getInitParameter("dbUsername"));
		System.setProperty("DB_Password", cntxtEvent.getServletContext().getInitParameter("dbPassword"));
		
		//Setting Mail Related values
		System.setProperty(SENDER_MAILID, cntxtEvent.getServletContext().getInitParameter("senderMailID"));
		System.setProperty(SENDER_MAIL_APP_PASSWORD, cntxtEvent.getServletContext().getInitParameter("senderAppPassword"));
		System.setProperty("mail.smtp.host", "smtp.gmail.com");
		System.setProperty("mail.smtp.port", "587");
		System.setProperty("mail.smtp.auth", "true");
		System.setProperty("mail.smtp.socketFactory.port", "465");
		System.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		
		//Initiating Mail instance
		MailGenerator.initMailGenratorInst();
	}
}
