package org.zprojects.modal;

public class UserEntity 
{
	private int userID=0;
	private String userName= "";
	private String userMail="";
	
	public UserEntity(int userID, String userName, String userMail) {
		super();
		this.userID = userID;
		this.userName = userName;
		this.userMail = userMail;
	}
	
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserMail() {
		return userMail;
	}
	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}
}
