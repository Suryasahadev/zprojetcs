package org.zprojects.modal;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.zprojects.container.GlobalNotations;

public class TaskEntity implements GlobalNotations
{
	private int taskID=0;
	private String taskName="";
	private String taskDescription="";
	private String owner = "";
	private String assignee = "";
	private int projectID = 0;
	private int milestoneID = 0;
	private Timestamp startTimestamp = null;
	private Timestamp creationTimestamp = null;
	private Timestamp dueTimeStamp = null;
	private Timestamp completionTimeStamp = null;
	private String taskPriority = "";
	private String taskStatus = "";
	private boolean hasBug = false;
	
	public TaskEntity(String taskName, String taskDescription, int taskID, String owner, String assignee, Timestamp startTimestamp, 
			Timestamp dueTimeStamp, String taskPriority, String taskStatus)
	{
		this.taskName = taskName;
		this.taskDescription = taskDescription;
		this.taskID = taskID;
		this.owner = owner;
		this.assignee = assignee;
		this.startTimestamp = startTimestamp;
		this.dueTimeStamp = dueTimeStamp;
		this.taskPriority = taskPriority;
		this.taskStatus = taskStatus;
	}
	public TaskEntity(int taskID, String taskName, String owner, String assignee, int projectID,
			int milestoneID, Timestamp creationTimestamp, Timestamp startTimestamp, Timestamp dueTimeStamp, Timestamp completionTimeStamp,
			String taskPriority, String taskStatus, boolean hasBug) 
	{
		super();
		this.taskID = taskID;
		this.taskName = taskName;
		this.owner = owner;
		this.assignee = assignee;
		this.projectID = projectID;
		this.milestoneID = milestoneID;
		this.creationTimestamp = creationTimestamp;
		this.startTimestamp = startTimestamp;
		this.dueTimeStamp = dueTimeStamp;
		this.completionTimeStamp = completionTimeStamp;
		this.taskPriority = taskPriority;
		this.taskStatus = taskStatus;
		this.hasBug = hasBug;
	}
	
	public TaskEntity(int taskID, String taskName, int projectID)
	{
		this.taskID = taskID;
		this.taskName = taskName;
		this.projectID = projectID;
	}
			
	public int getTaskID() {
		return taskID;
	}
	public void setTaskID(int taskID) {
		this.taskID = taskID;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getTaskDescription() {
		return taskDescription;
	}
	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public int getProjectID() {
		return projectID;
	}
	public void setProjectID(int projectID) {
		this.projectID = projectID;
	}
	public int getMilestoneID() {
		return milestoneID;
	}
	public void setMilestoneID(int milestoneID) {
		this.milestoneID = milestoneID;
	}
	public String getCreationTimestamp() {
		return formatToCustomDate(creationTimestamp);
	}
	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public String getStartTimestamp() {
		return formatToCustomDate(startTimestamp);
	}
	public String getDPFormattedStartTimestamp() {
		return formatToDPFormat(startTimestamp);
	}
	public void setStartTimestamp(Timestamp startTimestamp) {
		this.startTimestamp = startTimestamp;
	}
	public String getDueTimeStamp() {
		return formatToCustomDate(dueTimeStamp);
	}
	public String getDPFormattedDueTimestamp() {
		return formatToDPFormat(dueTimeStamp);
	}
	public void setDueTimeStamp(Timestamp dueTimeStamp) {
		this.dueTimeStamp = dueTimeStamp;
	}
	public String getCompletionTimeStamp() {
		return formatToCustomDate(completionTimeStamp);
	}
	public void setCompletionTimeStamp(Timestamp completionTimeStamp) {
		this.completionTimeStamp = completionTimeStamp;
	}
	public String getTaskPriority() {
		return taskPriority;
	}
	public void setTaskPriority(String taskPriority) {
		this.taskPriority = taskPriority;
	}
	public String getTaskStatus() {
		return taskStatus;
	}
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}
	public boolean isHasBug() {
		return hasBug;
	}
	public void setHasBug(boolean hasBug) {
		this.hasBug = hasBug;
	}
}
