package org.zprojects.modal;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import org.zprojects.container.GlobalNotations;

public class MilestoneEntity implements GlobalNotations
{
	private int milestoneID = 0;
	private String milestoneName = "";
	private String milestoneDescription = "";
	private String owner = "";
	private Timestamp creationTimestamp = null;
	private Timestamp startTimestamp = null;
	private Timestamp dueTimeStamp = null;
	private Timestamp completionTimeStamp = null;
	private String priority = "";
	private String status = "";
	
	private int projectID=0;
	
	public MilestoneEntity(int milestoneID, String milestoneName, String milestoneDescription, String owner, Timestamp startTimestamp, Timestamp dueTimestamp,
			String priority, String status) 
	{
		super();
		this.milestoneID = milestoneID;
		this.milestoneName = milestoneName;
		this.milestoneDescription = milestoneDescription;
		this.owner = owner;
		this.startTimestamp = startTimestamp;
		this.dueTimeStamp = dueTimestamp;
		this.priority = priority;
		this.status = status;
	}
	public MilestoneEntity(int milestoneID, String milestoneName, String milestoneDescription, String owner, Timestamp creationTimestamp, Timestamp startTimestamp, Timestamp dueTimestamp,
			Timestamp completionTimestamp, String priority, String status) 
	{
		super();
		this.milestoneID = milestoneID;
		this.milestoneName = milestoneName;
		this.milestoneDescription = milestoneDescription;
		this.owner = owner;
		this.creationTimestamp = creationTimestamp;
		this.startTimestamp = startTimestamp;
		this.dueTimeStamp = dueTimestamp;
		this.completionTimeStamp = completionTimestamp;
		this.priority = priority;
		this.status = status;
	}
	
	public int getMilestoneID() {
		return milestoneID;
	}
	public void setMilestoneID(int milestoneID) {
		this.milestoneID = milestoneID;
	}
	public String getMilestoneName() {
		return milestoneName;
	}
	public void setMilestoneName(String milestoneName) {
		this.milestoneName = milestoneName;
	}
	public String getCreationTimestamp() {
		return formatToCustomDate(creationTimestamp);
	}
	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public String getDueTimeStamp() {
		return formatToCustomDate(dueTimeStamp);
	}
	public String getDPFormattedDueTimestamp() {
		return formatToDPFormat(dueTimeStamp);
	}
	public void setDueTimestamp(Timestamp dueTimestamp) {
		this.dueTimeStamp = dueTimestamp;
	}
	public int getProjectID() {
		return projectID;
	}
	public void setProjectID(int projectID) {
		this.projectID = projectID;
	}
	public String getMilestoneDescription() {
		return milestoneDescription;
	}

	public void setMilestoneDescription(String milestoneDescription) {
		this.milestoneDescription = milestoneDescription;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getStartTimestamp() {
		return formatToCustomDate(startTimestamp);
	}
	
	public String getDPFormattedStartTimestamp() {
		return formatToDPFormat(startTimestamp);
	}
	
	public void setStartTimestamp(Timestamp startTimestamp) {
		this.startTimestamp = startTimestamp;
	}

	public String getCompletionTimeStamp() {
		return formatToCustomDate(completionTimeStamp);
	}

	public void setCompletionTimestamp(Timestamp completionTimestamp) {
		this.completionTimeStamp = completionTimestamp;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
