package org.zprojects.modal;

public class AttachmentEntity 
{
	private String fileName = "";
	private String documentSpec = "";
	private int documentID = 0;
	
	public AttachmentEntity(String fileName, String documentSpec, int documentID) {
		super();
		this.fileName = fileName;
		this.documentSpec = documentSpec;
		this.documentID = documentID;
	}
	
	public AttachmentEntity(String fileName, int documentID) {
		super();
		this.fileName = fileName;
		this.documentID = documentID;
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getDocumentSpec() {
		return documentSpec;
	}
	public void setDocumentSpec(String documentSpec) {
		this.documentSpec = documentSpec;
	}
	public int getDocumentID() {
		return documentID;
	}
	public void setDocumentID(int documentID) {
		this.documentID = documentID;
	}
}
