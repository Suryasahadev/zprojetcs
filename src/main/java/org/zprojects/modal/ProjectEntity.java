package org.zprojects.modal;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import org.zprojects.container.GlobalNotations;

public class ProjectEntity implements GlobalNotations
{
	private int projectID = 0;
	private String projectName = "";
	private String projectDescription = "";
	private String owner = "";
	private List<String> members;
	private String priority;
	private String status;
	private Timestamp creationTimestamp;
	private Timestamp startTimestamp;
	private Timestamp dueTimestamp;
	private Timestamp completionTimestamp;
	
	public ProjectEntity(int projectID, String projectName, String owner, List<String> members, Timestamp startTimestamp, Timestamp dueTimestamp, 
			String priority, String status) 
	{
		this.projectID = projectID;
		this.projectName = projectName;
		this.owner = owner;
		this.members = members;
		this.priority = priority;
		this.status = status;
		this.startTimestamp = startTimestamp;
		this.dueTimestamp = dueTimestamp;
	}
	public ProjectEntity(int projectID, String projectName, String projectDescription, String owner, List<String> members, String priority,
			String status, Timestamp creationTimestamp, Timestamp startTimestamp, Timestamp dueTimestamp,
			Timestamp completionTimestamp) 
	{
		super();
		this.projectID = projectID;
		this.projectName = projectName;
		this.projectDescription = projectDescription;
		this.owner = owner;
		this.members = members;
		this.priority = priority;
		this.status = status;
		this.creationTimestamp = creationTimestamp;
		this.startTimestamp = startTimestamp;
		this.dueTimestamp = dueTimestamp;
		this.completionTimestamp = completionTimestamp;
	}
	
	public int getProjectID() {
		return projectID;
	}
	public void setProjectID(int projectID) {
		this.projectID = projectID;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectDescription() {
		return projectDescription;
	}
	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}
	public String getOwner() 
	{
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	//Returning ["Surya", "Prakash"] as "Surya", "Prakash"
	public String getStringifiedMembers() {
		return members.toString().substring(1, members.toString().length()-1);
	}
	public List<String> getMembers() {
		return members;
	}
	public void setMembers(List<String> members) {
		this.members = members;
	}
	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	public String getCompletionTimestamp() {
		return formatToCustomDate(completionTimestamp);
	}
	public void setCompletionTimestamp(Timestamp completionTimestamp) {
		this.completionTimestamp = completionTimestamp;
	}
	public String getCreationTimestamp() {
		return formatToCustomDate(creationTimestamp);
	}
	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public String getStartTimestamp() {
		return formatToCustomDate(startTimestamp);
	}
	public String getDPFormattedStartTimestamp() {
		return formatToDPFormat(startTimestamp);
	}
	public void setStartTimestamp(Timestamp startTimestamp) {
		this.startTimestamp = startTimestamp;
	}
	public String getDueTimestamp() {
		return formatToCustomDate(dueTimestamp);
	}
	public String getDPFormattedDueTimestamp() {
		return formatToDPFormat(dueTimestamp);
	}
	public void setDueTimestamp(Timestamp dueTimestamp) {
		this.dueTimestamp = dueTimestamp;
	}
}
