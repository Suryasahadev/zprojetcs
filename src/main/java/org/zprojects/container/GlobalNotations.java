package org.zprojects.container;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

public interface GlobalNotations 
{
	Map<Integer, String> PRIORITY_MAPPING = new HashMap<Integer, String>()
	{
		{put(1, "Low");};
		{put(2, "Medium");};
		{put(3, "High");};
	};
	Map<Integer, String> STATUS_MAPPING = new HashMap<Integer, String>()
	{
		{put(1, "Open");};
		{put(2, "Development");};
		{put(3, "Testing");};
		{put(4, "Closed");};
	};
	Map<Integer, String> STATUS_COLOR_CODDING = new HashMap<Integer, String>()
	{
		{put(1, "#5866C2");};
		{put(2, "#FDAA29");};
		{put(3, "#EF4F85");};
		{put(4, "#7DCF41");};
	};
	Map<Integer, String> RENDER_CONTENT_ID_MAPPING = new HashMap<Integer, String>()
	{
		{put(1, "Project_ID");};
		{put(2, "Milestone_ID");};
		{put(3, "Task_ID");};
	};
	
	//Mail Credentials 
	String SENDER_MAILID = "SenderMailID";
	String SENDER_MAIL_APP_PASSWORD = "SenderAppPassword";
	String CURRENTLY_GENERATED_OTP = "CurrentOTP";
	
	//Request content defs
	String CREATED_PROJECTS = "CreatedProjects";
	String ASSIGNED_PROJECTS = "AssignedProjects";
	String CREATED_TASKS = "CreatedTasks";
	String ASSIGNED_TASKS = "AssignedTasks";
	String CREATED_MILESTONES = "CreatedMilestones";
	String PROJECT_TASKS = "ProjectTasks";
	String PROJECT_MILESTONES = "ProjectMilestones";
	String PROJECT_ATTACHMENTS = "ProjectAttachments";
	String PROJECT_CHARTS = "ProjectCharts";
	String PROJECT_DETAILS = "ProjectDetails";
	String TASK_ATTACHMENTS = "TaskAttachments";
	String TASK_DETAILS = "TaskDetails";
	String MILESTONE_ATTACHMENTS = "MilestoneAttachments";
	String MILESTONE_CHARTS = "MilestoneCharts";
	String MILESTONE_DETAILS = "MilestoneDetails";
	String USER_DASHBOARD = "Dashboard";
	
	//TIMESTAMP Notations
	String TIMESTAMP_SUFFIX = " 00:00:00.0000";
	String NULL_TIMESTAMP = "Not Mentioned";
	
	//Sign up Error Codes
	String SC_SIGN_UP_SUCCESS = "S200";
	String SC_SIGN_USER_VALIDATION_SUCCESS = "S201";
	String EC_USER_NAME_EXISTS = "S205";
	String EC_USER_MAIL_EXISTS = "S206";
	String SC_OTP_VALIDATION_SUCCESS = "S300";
	String EC_INCORRECT_OTP = "S350";
	
	//Login Error Codes
	String SC_LOGIN_UP_SUCCESS = "L200";
	String EC_INVALID_USERNAME = "L205";
	String EC_INVALID_USER_PASSWORD = "L206";
	
	//Session Attribute Keys
	String SK_LOGGED_USERNAME = "Username";
	String SK_LOGGED_USERID = "UserID";
	String SK_USER_LOGGED_IN = "LOGGED_USER";
	String SK_HOME_BREACH = "BREACH_HOME";
	String SK_AUTH_BREACH = "BREACH_AUTH";
	
	//Interceptor Results
	String PROCEED_HOME = "Home";
	String DIRECT_HIT = "Direct_Hit";
	String DIRECT_AUTH_HIT = "Direct_Auth_Hit";
	
	//Error Messages
	String UNAUTHORIZED_SESSION = "Invalid Session";
	String AUTH_BREACH_SESSION = "Logout to access Authentication pages";
	
	default String formatToCustomDate(Timestamp timestamp)
	{
		if(timestamp!=null)
			return new SimpleDateFormat("dd-MMM-yyyy").format(timestamp);
		else
			return NULL_TIMESTAMP;
	}
	
	default String formatToDPFormat(Timestamp timestamp)
	{
		if(timestamp!=null)
			return new SimpleDateFormat("MM/dd/yyyy").format(timestamp);
		else
			return NULL_TIMESTAMP;
	}
}
