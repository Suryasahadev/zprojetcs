package org.zprojects.container;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;

public interface SessionContainer 
{
	default Map<String, Object> getSession()
	{
		return ActionContext.getContext().getSession();
	}
}
